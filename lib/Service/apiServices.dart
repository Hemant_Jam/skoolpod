class ApiServices {
  static const String SOCKET_SERVER = "http://15.206.197.138:8111";
  static const String BASE_URL = "http://15.206.197.138/api/v1/";
  static const String BASE_IMAGE_URL =
      "http://15.206.197.138/storage/app/public/";
  static const String API_KEY = "f1b3c70aac237edfa6aeef52924cd6e3";

  static const String LOGIN = "login";
  static const String GET_TEACHER_CLASS = "get-teacher-classes";
  static const String GET_PRINCIPAL_DASHBOARD = "get-principal-dashboard";
  static const String SEND_PUSH_NOTIFICATION = "send-push-notification";
  static const String GET_CLASS_STUDENT_ATTENDANCE = "get-student-attendance";
  static const String GET_TEACHER_SCHOOL_ATTENDANCE =
      "get-teacher-school-attendance";
  static const String GET_STUDENT_LIST = "get-division-student-list";
  static const String GET_ALL_ATTENDANCE = "get-student-all-attendance";
  static const String FILL_STUDENT_ATTENDANCE = "fill-student-attendance";
  static const String GET_STANDARD_DIVISION = "get-standard-division";
  static const String GET_CLASS_ATTENDANCE = "get-class-attendance";
  static const String GET_STUDENT_PROFILE = "get-student-profile";
  static const String UPDATE_STUDENT_PROFILE = "student-profile-update";
  static const String UPLOAD_STUDENT_PROFILE_IMAGE =
      "student-profile-image-update";
  static const String GET_CHILD_LIST = "get-children";
  static const String GET_TEACHER_LEAVE = "get-teacher-leave";
  static const String STUDENT_LEAVE_MANAGE = "student-leave-manage";
  static const String GET_PRINCIPAL = "get-principal";
  static const String ADD_TEACHER_LEAVE = "add-teacher-leave";
  static const String TEACHER_LEAVE_MANAGE = "teacher-leave-manage";
  static const String REMOVE_TEACHER_LEAVE = "remove-teacher-leave";
  static const String CHILDREN_LEAVE = "children-leave";
  static const String GET_STUDENT_LEAVE_TEACHER = "get-student-leave-teacher";
  static const String GET_TEACHER_LECTURE = "get-teacher-lecture";
  static const String dummy_image_url =
      "https://i.pinimg.com/originals/83/64/66/83646654668bf9ae412f45bb2e417ddf.jpg";

  // Chat/Message Module
  static const String GET_CHAT = "get-chat";
  static const String UNREAD_CONVERSATION_COUNT = "get-unread-message-count";
  static const String GET_SINGLE_CONVERSATION = "get-single-conversation";
  static const String SEND_MESSAGE = "send-message";
  static const String READ_MESSAGE = "read-message";

  // Socket Events
  static const EVENT_SEND_MESSAGE = "send_message";
  static const EVENT_START_TYPING = "start_typing";
  static const EVENT_STOP_TYPING = "stop_typing";
  static const EVENT_READ_MESSAGE = "read_message";
  static const EVENT_GET_MESSAGE = "send_response_";
  static const EVENT_GET_NEW_MESSAGE = "get_message_";
  static const EVENT_GET_START_TYPING = "start_receiving_";
  static const EVENT_GET_STOP_TYPING = "stop_receiving_";
}
