import 'dart:io';
import 'package:downloads_path_provider_28/downloads_path_provider_28.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:skoolpod/Helper/permissionHelper.dart';
import 'package:skoolpod/Helper/sharedPreference.dart';
import 'package:skoolpod/Model/model_common_res.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class WebCall {
  static BaseOptions options = new BaseOptions(
    baseUrl: ApiServices.BASE_URL,
    contentType: "application/json",
  );

  static Future<dynamic>? requestPostApiCall(BuildContext context,
      {String? url,
      Map<String, String>? headers,
      Map<String, dynamic>? body,
      bool isLoading = true,
      bool removeBaseUrl = false}) async {
    var connectivityResult = await Connectivity().checkConnectivity();

    var responseBody;

    if (connectivityResult != ConnectivityResult.mobile &&
        connectivityResult != ConnectivityResult.wifi) {
      Ui.showMessageDialog(context, "No internet connection");
      return null;
    } else {
      if (isLoading) Ui.showLoadingDialog(context);

      print("Url: " + ApiServices.BASE_URL + url!);
      print("body: " + body.toString());
      Map<String, String> map = new Map();
      String? token = await SharedPref.getItem(Strings.APP_TOKEN, String);
      map.putIfAbsent("token", () => token ?? "");
      map.putIfAbsent("apikey", () => ApiServices.API_KEY);
      map.putIfAbsent("Content-Type", () => "application/json");

      if (headers != null && headers.isNotEmpty) map.addAll(headers);

      try {
        options.headers = map;
        if (removeBaseUrl) options.baseUrl = "";
        Dio dio = Dio(options);
        Response response = await dio.post(url, data: body);

        print("Response: " + response.data.toString());
        if (response.statusCode == 200) {
          if (isLoading) Navigator.pop(context);
          if (response.data["status"] == 1) {
            responseBody = new Map<String, dynamic>.from(response.data);
          } else {
            ModelCommonRes modelCommonRes =
                ModelCommonRes.fromJson(response.data);
            Ui.showToastMessage(modelCommonRes.msg!);
          }
        }
      } on DioError catch (e) {
        print(e.response.toString());
        Ui.showToastMessage("Something went wrong");

        if (isLoading) Navigator.pop(context);
      }
    }
    return responseBody;
  }

  static Future<dynamic>? uploadImage(
    BuildContext context,
    Map<String, String> param, {
    String? url,
    bool isLoading = true,
    MapEntry<String, MultipartFile>? file,
  }) async {
    var connectivityResult = await Connectivity().checkConnectivity();

    if (connectivityResult != ConnectivityResult.mobile &&
        connectivityResult != ConnectivityResult.wifi) {
      Ui.showMessageDialog(context, "No internet connection");
      return null;
    } else {
      if (isLoading) Ui.showLoadingDialog(context);

      try {
        FormData formData = FormData();

        formData.fields.addAll(param.entries);
        if (file != null) formData.files.add(file);

        Map<String, String> map = new Map();
        String? token = await SharedPref.getItem(Strings.APP_TOKEN, String);
        map.putIfAbsent("token", () => token ?? "");
        map.putIfAbsent("apikey", () => "f1b3c70aac237edfa6aeef52924cd6e3");
        map.putIfAbsent("Content-Type", () => "multipart/form-data");
        options.headers = map;
        Dio dio = Dio(options);
        print("Url: " + ApiServices.BASE_URL + url!);
        print("body: " + formData.toString());
        Response response = await dio.post(url, data: formData);

        print("Response: " + response.data.toString());
        if (response.statusCode == 200) {
          if (isLoading) Navigator.pop(context);
          return response.data;
        }
      } catch (e) {
        print(e.toString());
        if (isLoading) Navigator.pop(context);
      }
    }
    return null;
  }

  static Future<void> downloadFile(
      BuildContext context, String stringOfUrls) async {
    if (stringOfUrls.isEmpty) {
      Ui.showToastMessage("No media available to download");
      return;
    }

    Dio dio = Dio();

    if (!await Permission.storage.isGranted)
      PermissionHelper.takePermission();
    else {
      Ui.showLoadingDialog(context);
      try {
        var dir = await DownloadsPathProvider.downloadsDirectory;
        print(dir!.path);
        Response response = await dio.download(stringOfUrls,
            dir.path + "/" + File(stringOfUrls).path.split("/").last,
            onReceiveProgress: (rec, total) {
          print("Rec: $rec , Total: $total");
        });
        if (response.statusCode == 200)
          Ui.showSuccessToastMessage(
              "Download completed, please check Download folder");
      } catch (e) {
        Ui.showToastMessage("Something went wrong");
        print(e);
      }

      Navigator.of(context).pop();
    }
  }
}
