import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:sizer/sizer.dart';

class AppbarActionItem extends StatelessWidget {
  const AppbarActionItem({
    Key? key,
    this.imageIcon,
    this.routeName,
    this.totalItemsCount=0
  }) : super(key: key);

  final ImageProvider? imageIcon;
  final String? routeName;
  final int totalItemsCount;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4),
      child: Padding(
        padding: const EdgeInsets.only(right: 4,left: 4,top: 5),
        child:  Container(
            height: 100.0,
            width: 35.0,
            child:  GestureDetector(
              onTap: () {

              },
              child:  Stack(
                children: <Widget>[
                  Center(child:  Image(image: imageIcon!,height: 2.5.h,color: Colors.amber,),),
                  totalItemsCount == 0
                      ?  Container()
                      :  Positioned(left: 16,
                      child:  Stack(
                        children: <Widget>[
                           Icon(Icons.brightness_1,
                              size: 18.5, color: Colors.red),
                          Positioned.fill(
                            child:  Align(alignment: Alignment.center,
                                child:  Text(
                                  totalItemsCount.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 10.5,
                                      fontWeight: FontWeight.w500),
                                )),
                          ),
                        ],
                      )),
                ],
              ),
            )),
      ),
    );
  }

  String? getCurrentScreenName(BuildContext context){
    var route = ModalRoute.of(context);

    if(route?.settings!=null){
      return route?.settings.name;
    }

  }
}