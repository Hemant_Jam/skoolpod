import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:skoolpod/Resources/colorProperties.dart';

class UnderLineEditTextFormField extends StatefulWidget {
  UnderLineEditTextFormField({
    Key? key,
    @required TextEditingController? textController,
    this.hintText = "",
    this.validation,
    this.labelText,
    this.labelStyle,
    this.style,
    this.inputFormatters,
    this.maxLines,
    this.errorMaxLines,
    this.iconData,
    this.obscureText,
    this.keyboardType,
    this.onChanged
  }) : _textController = textController, super(key: key);

  final TextEditingController? _textController;
  final String hintText;
  final String? labelText;
  final TextStyle? labelStyle;
  final TextStyle? style;
  final String? Function(String? value)? validation;
  final List<TextInputFormatter>? inputFormatters;
  final ImageProvider? iconData;
  final int? maxLines;
  final int? errorMaxLines;
  final bool? obscureText;
  final TextInputType? keyboardType;
  final void Function(String)? onChanged;

  @override
  _UnderLineEditTextFormFieldState createState() => _UnderLineEditTextFormFieldState();
}

class _UnderLineEditTextFormFieldState extends State<UnderLineEditTextFormField> {
  final String? Function(String?) nullValidation = (String? value) => null;
  var prefixIcon;

  bool _obscureText =false;
  var suffixIcon;
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
  @override
  void initState() {
    _obscureText = widget.obscureText??false;
    print(_obscureText.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    prefixIcon =  widget.iconData!=null? ImageIcon(widget.iconData,color: PrimaryColor,size:32 ,): null;
    return TextFormField(controller: widget._textController,
      validator: widget.validation ?? nullValidation,obscureText: _obscureText,
      inputFormatters: widget.inputFormatters??[],
      keyboardType: widget.keyboardType,
      maxLines: widget.maxLines,
      style: widget.style,
      onChanged: widget.onChanged?? (String abc){},
      decoration: InputDecoration( prefixIcon: prefixIcon,hintText: widget.hintText,
        errorMaxLines: widget.errorMaxLines,
        prefixIconConstraints:BoxConstraints(minWidth: 30, maxHeight: 20),
        suffixIconConstraints:BoxConstraints(minWidth: 30, maxHeight: 20) ,
        suffixIcon: widget.obscureText!=null?GestureDetector(onTap:_toggle,child: ImageIcon(Image.asset(_obscureText?'assets/images/ic_visible.png' :'assets/images/ic_invisible.png').image,color: Colors.grey,)):null,
        contentPadding: EdgeInsets.only(top: 2),
        labelText: widget.labelText,labelStyle: widget.labelStyle,
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: PrimaryColor),
        ),),);
  }
}