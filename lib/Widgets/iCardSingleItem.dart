import 'package:flutter/cupertino.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';

class ICardSingleItem extends StatelessWidget {
   ICardSingleItem({Key? key,this.title,this.iconImage,this.backgroundImage,this.navigationPage}) : super(key: key);

   final String? backgroundImage;
   final String? iconImage;
   final String? title;
   final String? navigationPage;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if(navigationPage!=null)
          Navigator.of(context).pushNamed(navigationPage!);
      },
      child: Stack(
        children: [
          Image.asset(backgroundImage??"assets/images/dash_bg_message.png"),
          Align(alignment: Alignment.topRight,
            child: Container(height: 12.0.h,
                padding: EdgeInsets.only(top: 20,right: 20),
                child: Image.asset(iconImage??"assets/images/ic_dash_messages.png")),
          ),
          Positioned.fill(left: 12,
            child: Align(alignment: Alignment.bottomLeft,
                child: Text(title!,style: TextStyle(color: White,fontFamily: "Baloo",fontSize: 18.0.sp),)),
          )
        ],
      ),
    );
  }
}
