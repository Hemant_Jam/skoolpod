import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:downloads_path_provider_28/downloads_path_provider_28.dart';
import 'package:flutter/material.dart';
import 'package:flutter_link_previewer/flutter_link_previewer.dart';
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Helper/utils.dart';
import 'package:skoolpod/Model/model_chat.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' show PreviewData;

class ChatItemWidget extends StatefulWidget{
  var index;
  ChatData conversationData;
  int currentUserId;

  ChatItemWidget(this.conversationData,this.index,this.currentUserId);
  static const double radiusOfCorner =20;

  @override
  _ChatItemWidgetState createState() => _ChatItemWidgetState();
}

class _ChatItemWidgetState extends State<ChatItemWidget> {
  PreviewData? previewData ;
  ImageProvider? _imageToShow;

  @override
  void initState() {
    if(widget.conversationData.message!.type == Strings.MESSAGE_DOCUMENT )
      _imageToShow = new AssetImage("assets/images/ic_msg_file.png");
    else
      _imageToShow = new AssetImage("assets/images/ic_msg_image.png");

    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    bool isCurrentUserMessage = widget.conversationData.message!.sender!.id == widget.currentUserId;
    // bool textIsLink = widget.conversationData.message!.type == Strings.MESSAGE_TEXT  &&  Uri.parse(widget.conversationData.message!.body!).isAbsolute;
    bool textIsLink = widget.conversationData.message!.type == Strings.MESSAGE_TEXT  &&  RegExp(Strings.urlPattern, caseSensitive: false).hasMatch(widget.conversationData.message!.body!) ;


     Decoration chatItemDecoration =  BoxDecoration(
        color: isCurrentUserMessage? Dash6 :PrimaryColor,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(ChatItemWidget.radiusOfCorner),topRight: Radius.circular(ChatItemWidget.radiusOfCorner),
            bottomRight: Radius.circular(isCurrentUserMessage?0:ChatItemWidget.radiusOfCorner),bottomLeft: Radius.circular(isCurrentUserMessage?ChatItemWidget.radiusOfCorner:0)));


    return Row(mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: isCurrentUserMessage? MainAxisAlignment.end :MainAxisAlignment.start,

      children: [

        Flexible(
          child: Container(
            margin: EdgeInsets.only(right: isCurrentUserMessage? 0: 15.0.w,top: 8,bottom: 8,left: isCurrentUserMessage? 15.0.w : 0),
            decoration: chatItemDecoration,
            child: Row(mainAxisSize: MainAxisSize.min,
              children: [
                if(widget.conversationData.message!.type != Strings.MESSAGE_TEXT  && isCurrentUserMessage)
                  FutureBuilder<Widget>(
                      future: setDocImage(widget.conversationData.message!.body!,widget.conversationData.message!.type!),
                      builder: (context, AsyncSnapshot<Widget> snapshot) {
                        if (snapshot.hasData) {
                          return snapshot.data!;
                        } else {
                          return Container();
                        }
                      }
                  ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: isCurrentUserMessage? CrossAxisAlignment.end : CrossAxisAlignment.start,
                    children: <Widget>[
                      ///* User name */
                      if(!isCurrentUserMessage)
                      Container(
                        child: Text(widget.conversationData.message!.sender!.firstname??'',style: TextStyle(color: ColorUsername,fontSize: 11.0.sp)),
                        margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 0.0),
                      ),

                      ///* Message or doc */
                      if(widget.conversationData.message!.type == Strings.MESSAGE_TEXT  && !textIsLink)
                      Container(
                        child:
                        Text(widget.conversationData.message?.body??'This is a received message',style: TextStyle(color: White,fontSize: 13.0.sp )),
                        margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
                      ),
                      if(widget.conversationData.message!.type != Strings.MESSAGE_TEXT  && !textIsLink)
                        Container(
                          child:
                          Text(widget.conversationData.message!.title??'title',style: TextStyle(color: White,fontSize: 13.0.sp )),
                          margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
                        ),

                      if(textIsLink)
                        Container(margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
                          child: LinkPreview(
                            enableAnimation: true,
                            onPreviewDataFetched: (data) {
                              setState(() {
                                previewData=data;
                              });
                            },
                            previewData: previewData, // Pass the preview data from the state
                            text: widget.conversationData.message!.body!,
                            width: double.maxFinite,
                          ),
                        ),

                      ///* DateTime */
                      Container(
                        padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 5.0),
                        child: Text(
                            getFormatedDate(widget.conversationData.message!.updatedAt!),style: TextStyle(color: White,fontSize: 8.0.sp)),
                      )
                    ],

                  ),
                ),

                if(widget.conversationData.message!.type != Strings.MESSAGE_TEXT  && !isCurrentUserMessage)
                  FutureBuilder<Widget>(
                      future: setDocImage(widget.conversationData.message!.body!,widget.conversationData.message!.type!),
                      builder: (context, AsyncSnapshot<Widget> snapshot) {
                        if (snapshot.hasData) {
                          return snapshot.data!;
                        } else {
                          return Container();
                        }
                      }
                  ),
              ],
            ),
          ),
        ),
      ],
    );
    /*indexIf (index % 2 == 0) {
      //This is the sent message. We'll later use data from firebase instead of index to determine the message is sent or received.
      return Container(
          child: Column(children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  child: Text(
                    'This is a sent message',
                    style: TextStyle(color: Colors.black),
                  ),
                  padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                  width: 200.0,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(8.0)),
                  margin: EdgeInsets.only(right: 10.0),
                )
              ],
              mainAxisAlignment:
              MainAxisAlignment.end, // aligns the chatItem to right end
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: Text(
                      DateFormat('dd MMM kk:mm')
                          .format(DateTime.fromMillisecondsSinceEpoch(1565888474278)),
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12.0,
                          fontStyle: FontStyle.normal),
                    ),
                    margin: EdgeInsets.only(left: 5.0, top: 5.0, bottom: 5.0),
                  )])
          ]));
    } else {
      // This is a received message
      return Container(
        margin: EdgeInsets.only(right: 15.0.w,top: 10,bottom: 10),
        decoration: chatItemDecoration,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Text('Hmt someone',style: TextStyle(color: ColorUsername,fontSize: 11.0.sp)),
              margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 0.0),
            ),
            Container(
              child: Text('This is a received message',style: TextStyle(color: White,fontSize: 12.0.sp)),
              margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 5.0),
              child: Text(
                getFormatedDate(""),style: TextStyle(color: White,fontSize: 9.0.sp)),
            )
          ],

        ),
      );
    } */ }

    String getFormatedDate(String dateTime){
    //if(Utils.formateDate(dateTime, Strings.FORMAT_SERVER, Strings.FORMAT_SIMPLE))
      if(DateTime.now().day.compareTo(DateFormat(Strings.FORMAT_SERVER).parse(dateTime).day)==0)
    return Utils.formateDate(dateTime, Strings.FORMAT_SERVER, Strings.FORMAT_TODAY_TIME);
      else
        return Utils.formateDate(dateTime, Strings.FORMAT_SERVER, Strings.FORMAT_DATE_TIME);
    }

    Future<Widget> setDocImage(String filePath, String type) async {
      var dir = await DownloadsPathProvider.downloadsDirectory;
      File file = File(dir!.path+"/"+File(filePath).path.split("/").last);

      ///  set image preview if there is doc type is image /*
      if(type == Strings.MESSAGE_IMAGE && !await file.exists()){
        return GestureDetector(
          onTap: ()=> onDocumentClick(filePath),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.all(10),
                child: CachedNetworkImage(
                  width: 60.0,
                  height: 62.0,fit: BoxFit.cover,
                  memCacheHeight: 20,memCacheWidth: 20,
                  imageUrl: ApiServices.BASE_IMAGE_URL+filePath,
                  errorWidget: (context, url, error) => Container(height: 68,
                      child: Image.asset("assets/images/ic_msg_image.png",)),
                ),
              ),
              Positioned.fill(
                child: Align(alignment: Alignment.bottomRight,
                    child: Image.asset("assets/images/ic_download.png",height: 25,)),
              )
            ],
          ),
        );
      }
      else{
        if(await file.exists()  && type == Strings.MESSAGE_IMAGE)
        _imageToShow = Image.file(file).image;

        return GestureDetector(
          onTap: ()=> onDocumentClick(filePath),
          child: Container(
              margin: EdgeInsets.fromLTRB(8,10,10.0,10),
              height: 62,width: 60,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: _imageToShow!,
                  fit: BoxFit.cover,
                ),
              )),
        );
      }
    }

    Future<void> onDocumentClick(String fileUrl) async {
      var dir = await DownloadsPathProvider.downloadsDirectory;
      File file = File(dir!.path+"/"+File(fileUrl).path.split("/").last);
      if(await file.exists()){
        final _result = await OpenFile.open(file.path);
        print(_result.message);
        print(_result.type);
      }
      else{
        WebCall.downloadFile(context, ApiServices.BASE_IMAGE_URL+fileUrl).then((value){setState(() { });});
      }
    }
}