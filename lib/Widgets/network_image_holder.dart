import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Service/apiServices.dart';

class NetworkImageHolder extends StatelessWidget {
  const NetworkImageHolder({Key? key,this.imageUrl,this.placeHolder}) : super(key: key);
  final String? imageUrl;
  final String? placeHolder;

  @override
  Widget build(BuildContext context) {
    return imageUrl!=null && imageUrl!.isNotEmpty?
        CachedNetworkImage(
          imageUrl: ApiServices.BASE_IMAGE_URL+imageUrl!,
          imageBuilder: (context, imageProvider) => Container(
            width: 68.0,
            height: 68.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
            ),
          ),
          //placeholder: (context, url) => Image.asset("assets/images/ic_student_placeholder.png"),
          errorWidget: (context, url, error) => Container(height: 68,
              child: Image.asset(placeHolder??"assets/images/ic_student_placeholder.png",)),
        )
            : Image.asset(placeHolder??"assets/images/ic_student_placeholder.png",);
  }
}
