import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';

class TextFormFieldWithTitle extends StatefulWidget {
  TextFormFieldWithTitle(
      {Key? key,
      @required TextEditingController? textController,
      this.focusedBorder,
      this.validation,
      this.labelText,
      this.labelStyle,
      this.style,
      this.inputFormatters,
      this.maxLines,
      this.errorMaxLines,
      this.enabled,
      this.keyboardType,
      this.onChanged,
      this.title,
      this.contentPadding})
      : _textController = textController,
        super(key: key);

  final TextEditingController? _textController;
  final InputBorder? focusedBorder;
  final String? labelText;
  final TextStyle? labelStyle;
  final TextStyle? style;
  final String? Function(String? value)? validation;
  final List<TextInputFormatter>? inputFormatters;
  final bool? enabled;
  final int? maxLines;
  final int? errorMaxLines;
  final TextInputType? keyboardType;
  final void Function(String)? onChanged;
  final String? title;
  final EdgeInsetsGeometry? contentPadding;

  @override
  _TextFormFieldWithTitleState createState() => _TextFormFieldWithTitleState();
}

class _TextFormFieldWithTitleState extends State<TextFormFieldWithTitle> {
  final String? Function(String?) nullValidation = (String? value) => null;
  var suffixIcon;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.title != null)
          Text(
            widget.title!,
            style: TextStyle(fontSize: 11.0.sp),
          ),
        TextFormField(
          controller: widget._textController,
          validator: widget.validation ?? nullValidation,
          inputFormatters: widget.inputFormatters ?? [],
          keyboardType: widget.keyboardType,
          maxLines: widget.maxLines,
          enabled: widget.enabled ?? false,
          style: widget.style ?? TextStyle(fontSize: 12.0.sp),
          onChanged: widget.onChanged ?? (String abc) {},
          decoration: InputDecoration(
            errorMaxLines: widget.errorMaxLines,
            isDense: true,
            prefixIconConstraints: const BoxConstraints(minWidth: 30),
            suffixIconConstraints:
                const BoxConstraints(minWidth: 30, maxHeight: 20),
            //prefix: widget.prefixWidget,
            contentPadding: widget.contentPadding ??
                const EdgeInsets.symmetric(vertical: 12),
            //labelText: widget.labelText,labelStyle: widget.labelStyle,
            border: widget.focusedBorder ?? InputBorder.none,
            enabledBorder: widget.focusedBorder ?? InputBorder.none,
            focusedBorder: widget.focusedBorder ?? InputBorder.none,
          ),
        ),
      ],
    );
  }
}
