import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_login.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Widgets/appbar_action_item.dart';

class HomeScreenAppBar extends StatelessWidget implements PreferredSizeWidget{
  const HomeScreenAppBar({
    Key? key,
    required this.userData,
  }) : super(key: key);

  final Data userData;

  @override
  Widget build(BuildContext context) {
    String title = userData.role == Strings.PRINCIPAL ? Strings.dashboard : userData.firstname!=null ? userData.firstname!+" "+userData.lastname! : Strings. dashboard ;
    return AppBar(backgroundColor: White,
      titleSpacing: 12.0,
      title: Row(
        children: [
          userData.role != Strings.PRINCIPAL?
          Align(
            child: Container(height: 38,
              child:userData.image!=null?
              CircleAvatar(
                backgroundImage:
                NetworkImage(ApiServices.BASE_IMAGE_URL+userData.image!),
                backgroundColor: Colors.transparent,
              ):
              Image.asset("assets/images/ic_student_placeholder.png",),
            ),
          ):
          Container(height: 38,
            padding: const EdgeInsets.all(2.0),
            child: Image.asset("assets/images/ic_dashboard.png"),
          ),
          SizedBox(width: 13,),
          Text(title,style: TextStyle(color: PrimaryColor,fontSize: 14.0.sp),),
        ],
      ),
      actions: [
        AppbarActionItem(
          imageIcon: Image.asset("assets/images/bell.png").image,
          totalItemsCount: 0,
          //routeName: ScreenFavourite.routeName,
        ),
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize =>  Size.fromHeight(55/*6.0.h*/);
}