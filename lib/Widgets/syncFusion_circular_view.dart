import 'package:flutter/cupertino.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class SyncfusionCircularView extends StatelessWidget {
  const SyncfusionCircularView({
    Key? key,required this.value
  }) : super(key: key);

  final double value;

  @override
  Widget build(BuildContext context) {
    return Container(height: 60,
      child: SfRadialGauge(axes: <RadialAxis>[
        RadialAxis(
          minimum: 0,
          maximum: 100,
          showLabels: false,
          showTicks: false,
          startAngle: 270,endAngle: 270,
          annotations: <GaugeAnnotation>[
            GaugeAnnotation(
                positionFactor: 0.1,
                angle: 90,
                widget: Text("${value.round()}%" ,
                  style: TextStyle(fontSize: 11,fontWeight: FontWeight.bold),
                ))
          ],
          axisLineStyle: AxisLineStyle(
            thickness: 4.0,
            color: Color.fromARGB(30, 0, 169, 181),
          ),
          pointers: <GaugePointer>[
            RangePointer(
              value: value,
              enableAnimation: true,
              width: 0.1,animationDuration: 500,animationType: AnimationType.ease,
              sizeUnit: GaugeSizeUnit.factor,

            ),
          ],
        ),
      ]),
    );
  }
}