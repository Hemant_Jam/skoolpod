import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:skoolpod/Resources/colorProperties.dart';

class SpeedDialFloatingButton{
  Widget getFloatingButton(ValueNotifier<bool> isDialOpen, void Function(String navigateTo) onTap){
    bool customDialRoot = false;
    TextStyle  speedChildLabelStyle = TextStyle(color: PrimaryColor,fontSize: 12.0.sp);
    return SpeedDial(
      // animatedIcon: AnimatedIcons.menu_close,
      // animatedIconTheme: IconThemeData(size: 22.0),
      // / This is ignored if animatedIcon is non null
      // child: Text("open"),
      // activeChild: Text("close"),
      icon: Icons.add,
      activeIcon: Icons.close,
      spacing: 2,
      openCloseDial: isDialOpen,
      childPadding: EdgeInsets.all(5),
      spaceBetweenChildren: 4,
      dialRoot: customDialRoot
          ? (ctx, open, toggleChildren) {
        return ElevatedButton(
          onPressed: toggleChildren,
          style: ElevatedButton.styleFrom(
            primary: Colors.blue[900],
            padding:
            EdgeInsets.symmetric(horizontal: 22, vertical: 18),
          ),
          child: Text(
            "Custom Dial Root",
            style: TextStyle(fontSize: 17),
          ),
        );
      }
          : null,
      buttonSize: 56, // it's the SpeedDial size which defaults to 56 itself
      // iconTheme: IconThemeData(size: 22),
      //label: false ? Text("Open") : null, // The label of the main button. ///
      /// The active label of the main button, Defaults to label if not specified.
      //activeLabel: false ? Text("Close") : null,  ///

      /// Transition Builder between label and activeLabel, defaults to FadeTransition.
      // labelTransitionBuilder: (widget, animation) => ScaleTransition(scale: animation,child: widget),
      /// The below button size defaults to 56 itself, its the SpeedDial childrens size
      childrenButtonSize: 54.0,
      visible: true, ///
      direction: SpeedDialDirection.Up,
      switchLabelPosition: false, ///

      /// If true user is forced to close dial manually
      closeManually: false,

      /// If false, backgroundOverlay will not be rendered.
      renderOverlay: true,
      // overlayColor: Colors.black,
      // overlayOpacity: 0.5,
      onOpen: () => print('OPENING DIAL'),
      onClose: () => print('DIAL CLOSED'),
      useRotationAnimation: true, ///
      //tooltip: 'Open Speed Dial',
      heroTag: 'speed-dial-hero-tag',
      // foregroundColor: Colors.black,
      backgroundColor: PrimaryColor,
      // activeForegroundColor: Colors.red,
      // activeBackgroundColor: Colors.blue,
      elevation: 8.0,
      isOpenOnStart: false,
      animationSpeed: 200,
      shape: customDialRoot ? RoundedRectangleBorder() : StadiumBorder(),
      // childMargin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      children: [
        SpeedDialChild(
          child:  ImageIcon(AssetImage("assets/images/ic_send_notification.png")) ,
          backgroundColor: ColorYellow,
          foregroundColor: Colors.white,
          label: 'Send Notification',
          labelStyle: speedChildLabelStyle,
          labelBackgroundColor: Dash1,
          onTap: () => onTap("NOTIFICATION"),
        ),
        SpeedDialChild(
          child: Icon(Icons.add) ,
          backgroundColor: ColorYellow,
          foregroundColor: Colors.white,
          labelStyle: speedChildLabelStyle,
          label: 'Add Assignment',
          labelBackgroundColor: Dash1,
          onTap: () => onTap("ASSIGNMENT"),
        ),
      ],
    );
  }
}