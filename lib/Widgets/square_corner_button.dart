import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';

class SquareButton extends StatelessWidget {
  const SquareButton({
    Key? key,
    this.backgroundColour= PrimaryColor,
    required this.onPressed,
    this.child,this.borderColour,
    this.width,
    this.cornerRadius,
    this.text
  }) : super(key: key);

  final Color backgroundColour;
  final Color? borderColour;
  final VoidCallback onPressed;
  final double? width;
  final Widget? child;
  final double? cornerRadius;
  final String? text;


  @override
  Widget build(BuildContext context) {
    return Container(
      width: width??double.maxFinite,
      height: 55,//6.0.h,
      child: ElevatedButton(
          child: child??Text(text??"", style: TextStyle(fontSize: 13.0.sp,color: Colors.white)),
          onPressed: onPressed,
          style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(backgroundColour),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(cornerRadius??0),
                      side: BorderSide(color: borderColour??backgroundColour)
                  )
              )
          )
      ),
    );
  }
}