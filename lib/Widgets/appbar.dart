import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Widgets/appbar_action_item.dart';

class GlobalAppBar extends StatelessWidget  implements PreferredSizeWidget{
  const GlobalAppBar({
    Key? key,@required this.title,this.backgroundColor,this.showAction=true
  }) : super(key: key);

  final String? title;
  final Color? backgroundColor;
  final bool showAction;
  @override
  Widget build(BuildContext context) {
        return AppBar(
          backgroundColor: backgroundColor,
          iconTheme: IconThemeData(
            color: White, //change your color here
          ),
          leading: IconButton(
            onPressed: ()=>Navigator.of(context).pop(),iconSize: 13.0.sp,
            icon: Icon(Icons.arrow_back_ios_new_sharp),color: White,),
          title: Text(title??"",style: TextStyle(color: White),),
          centerTitle: false,
          actions:showAction?
          [
            AppbarActionItem(
              imageIcon: Image.asset("assets/images/bell.png").image,
              totalItemsCount: 0,
              //routeName: ScreenFavourite.routeName,
            ),
          ] :[]
        );

  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize =>  Size.fromHeight(55/*6.5.h*/);
}
