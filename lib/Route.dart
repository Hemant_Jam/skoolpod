import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:skoolpod/Model/model_classes.dart' as ModelClass;
import 'package:skoolpod/Model/model_messages.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Admin/screen_admin_home.dart';
import 'package:skoolpod/Screen/Admin/screen_class_detail.dart';
import 'package:skoolpod/Screen/Admin/screen_class_list.dart';
import 'package:skoolpod/Screen/Admin/screen_push_message.dart';
import 'package:skoolpod/Screen/Parent/screen_child_leave_list.dart';
import 'package:skoolpod/Screen/Parent/screen_child_list.dart';
import 'package:skoolpod/Screen/Parent/screen_parent_home.dart';
import 'package:skoolpod/Screen/Teacher/screen_add_assignment.dart';
import 'package:skoolpod/Screen/Teacher/screen_classes.dart';
import 'package:skoolpod/Screen/Teacher/screen_class_detail.dart';
import 'package:skoolpod/Screen/Teacher/screen_more_option.dart';
import 'package:skoolpod/Screen/Teacher/screen_teacher_add_leave.dart';
import 'package:skoolpod/Screen/screen_splash.dart';
import 'package:skoolpod/Screen/screen_student_profile.dart';
import 'Screen/AttendanceScreen/screen_attendance.dart';
import 'Screen/Parent/screen_child_leave_add.dart';
import 'Screen/Principal/screen_teacher_leave_list.dart';
import 'Screen/Teacher/screen_activity_list.dart';
import 'Screen/Teacher/screen_leave_manage.dart';
import 'Screen/Teacher/screen_student_activity.dart';
import 'Screen/Teacher/screen_student_list_activity.dart';
import 'Screen/Teacher/screen_take_attendance.dart';
import 'Screen/Teacher/screen_teacher_home.dart';
import 'Screen/screen_chat.dart';
import 'Screen/screen_login.dart';
import 'Screen/screen_messages.dart';
import 'Screen/screen_testing.dart';

class RouteGenerator {
  static const Duration duration = Duration(milliseconds: 800);
  static const transitionType = PageTransitionType.rightToLeftWithFade;
  static Route? generateRoute(RouteSettings settings) {
    // Getting arguments passed while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      /*case MyAppTesting.routeName:
        return MaterialPageRoute(
            builder: (_) => MyAppTesting()
        );*/
      case MyTesting.routeName:
        return MaterialPageRoute(
            settings: settings, builder: (_) => MyTesting());
      case ScreenSplash.routeName:
        return MaterialPageRoute(
            settings: settings, builder: (_) => ScreenSplash());
      case ScreenLogin.routeName:
        return MaterialPageRoute(
            settings: settings, builder: (_) => ScreenLogin());
      case ScreenTeacherHome.routeName:
        return PageTransition(
          child: ScreenTeacherHome(),
          type: transitionType,
          reverseDuration: duration,
          settings: settings,
          duration: duration,
        );
      case ScreenParentHome.routeName:
        return PageTransition(
          child: ScreenParentHome(),
          type: transitionType,
          reverseDuration: duration,
          settings: settings,
          duration: duration,
        );
      case ScreenAdminHome.routeName:
        return PageTransition(
          child: ScreenAdminHome(),
          type: transitionType,
          reverseDuration: duration,
          settings: settings,
          duration: duration,
        );
      case ScreenMessages.routeName:
        return PageTransition(
          child: ScreenMessages(),
          type: transitionType,
          reverseDuration: duration,
          settings: settings,
          duration: duration,
        );
      /*return MaterialPageRoute(
            settings: settings,
            builder: (_) =>ScreenMessages()
        );*/
      case ScreenAddAssignment.routeName:
        return PageTransition(
            child: ScreenAddAssignment(
              result: args as Results,
            ),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenPushMessage.routeName:
        return PageTransition(
            child: ScreenPushMessage(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenChat.routeName:
        return PageTransition(
            child: ScreenChat(
              conversationItem: args as Data,
            ),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenClasses.routeName:
        return PageTransition(
            child: ScreenClasses(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenClassDetail.routeName:
        return PageTransition(
            child: ScreenClassDetail(classData: args as ModelClass.Data),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenClassListForAdmin.routeName:
        return PageTransition(
            child: ScreenClassListForAdmin(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenAdminClassDetail.routeName:
        return PageTransition(
            child: ScreenAdminClassDetail(
              arguments: args as Arguments,
            ),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenMoreOption.routeName:
        return PageTransition(
            child: ScreenMoreOption(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenTakeAttendance.routeName:
        return PageTransition(
            child: ScreenTakeAttendance(classData: args as ModelClass.Data),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenStudentProfile.routeName:
        return PageTransition(
            child: ScreenStudentProfile(
              argument: args as Map<String, dynamic>,
            ),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);
      case ScreenChildList.routeName:
        return PageTransition(
            child: ScreenChildList(type: args as String),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      ///attendanceScreen
      case AttendancePage.routeName:
        return PageTransition(
            child: AttendancePage(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      ///teacherLeaveScreen
      case ScreenLeaveManage.routeName:
        return PageTransition(
            child: ScreenLeaveManage(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      ///teacher apply for leave screen
      case ScreenTeacherAddLeaves.routeName:
        return PageTransition(
            child: ScreenTeacherAddLeaves(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      ///teacher leave list / principal side
      case TeacherLeaveList.routeName:
        return PageTransition(
            child: TeacherLeaveList(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      /// child leave list screen
      case ScreenStudentLeaveList.routeName:
        return PageTransition(
            child: ScreenStudentLeaveList(
              argument: args as Map<String, dynamic>,
            ),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      ///child leave add screen
      case ScreenChildLeaveAdd.routeName:
        return PageTransition(
            child: ScreenChildLeaveAdd(argument: args as Map<String, dynamic>),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      /// student activity screen teacher side
      case ScreenStudentActivity.routeName:
        return PageTransition(
            child: ScreenStudentActivity(),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      /// student list of activity side
      case ScreenStudentListActivity.routeName:
        return PageTransition(
            child: ScreenStudentListActivity(
                arguments: args as Map<String, dynamic>),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      /// activity list of student teacher side
      ///
      case ScreenActivityList.routeName:
        return PageTransition(
            child: ScreenActivityList(argument: args as Map<String, dynamic>),
            type: transitionType,
            reverseDuration: duration,
            settings: settings,
            duration: duration,
            alignment: Alignment.center);

      /*default:
        return MaterialPageRoute(
            builder: (context) => DevelopmentScreen("This page is under development"),
          );*/
    }
  }
}
