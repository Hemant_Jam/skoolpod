import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Route.dart';
import 'package:skoolpod/Screen/screen_splash.dart';

import 'Resources/colorProperties.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Sizer(
          builder: (context, orientation, deviceType)  {
            return  MultiProvider(
              providers: [
                ChangeNotifierProvider<SessionNotifier>(create: (_) => SessionNotifier()),
              ],
              child: MaterialApp(
                  debugShowCheckedModeBanner: false,
                  title: 'SkoolPod app',
                  theme: ThemeData(
                      primarySwatch:MaterialColor(0xff00baaf, colorCodes),
                      focusColor:MaterialColor(0xff00baaf, colorCodes),
                      //hintColor: MaterialColor(0xff00baaf, colorCodes),
                      textTheme: TextTheme(
                        //bodyText2: TextStyle(color: PrimaryColor,fontSize: 12.0.sp),
                        //subtitle2: TextStyle(color: Colors.white,fontSize: 10.0.sp),
                        //headline2: TextStyle(fontWeight: FontWeight.bold,fontSize: 14.0.sp,color: PrimaryColor,fontFamily: "Philosopher"),
                        //headline1: TextStyle(fontWeight: FontWeight.w400,color: Color(0xff333333),fontSize: 12.0.sp,fontFamily: "Avenir-LT-Std"),
                      )
                  ),
                  onGenerateRoute: RouteGenerator.generateRoute,
                  initialRoute: ScreenSplash.routeName,
                  // home: ScreenLogin(),
                ),
            );
          },
        );
      },
    );
  }
}

