class ModelClassDetail {
  int? status;
  String? msg;
  List<Data>? data;

  ModelClassDetail({this.status, this.msg, this.data});

  ModelClassDetail.fromJson(dynamic json) {
    status = json["status"];
    msg = json["msg"];
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["msg"] = msg;
    if (data != null) {
      map["data"] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int? id;
  String? firstname;
  dynamic? middleName;
  String? lastname;
  String? dob;
  String image = "";
  dynamic? thumb;
  String? gender;
  int? schoolId;
  int? standardId;
  int? divisionId;
  dynamic? parentId;
  String? status;
  Attendance? attendance;

  Data(
      {this.id,
      this.firstname,
      this.middleName,
      this.lastname,
      this.dob,
      this.image = "",
      this.thumb,
      this.gender,
      this.schoolId,
      this.standardId,
      this.divisionId,
      this.parentId,
      this.status,
      this.attendance});

  Data.fromJson(dynamic json) {
    id = json["id"];
    firstname = json["firstname"];
    middleName = json["middlename"];
    lastname = json["lastname"];
    dob = json["dob"];
    image = json["image"];
    thumb = json["thumb"];
    gender = json["gender"];
    schoolId = json["school_id"];
    standardId = json["standard_id"];
    divisionId = json["division_id"];
    parentId = json["parent_id"];
    status = json["status"];
    attendance = json["attendance"] != null
        ? Attendance.fromJson(json["attendance"])
        : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firstname"] = firstname;
    map["middlename"] = middleName;
    map["lastname"] = lastname;
    map["dob"] = dob;
    map["image"] = image;
    map["thumb"] = thumb;
    map["gender"] = gender;
    map["school_id"] = schoolId;
    map["standard_id"] = standardId;
    map["division_id"] = divisionId;
    map["parent_id"] = parentId;
    map["status"] = status;
    if (attendance != null) {
      map["attendance"] = attendance?.toJson();
    }
    return map;
  }
}

class Attendance {
  int? present;
  int? absent;
  String? datePresent;

  Attendance({this.present, this.absent, this.datePresent});

  Attendance.fromJson(dynamic json) {
    present = json["present"];
    absent = json["absent"];
    datePresent = json["date_present"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["present"] = present;
    map["absent"] = absent;
    map["date_present"] = datePresent;
    return map;
  }
}
