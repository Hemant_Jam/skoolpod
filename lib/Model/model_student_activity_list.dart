class ModelStudentActivityList {
  int? status;
  String? msg;
  List<Data>? data;

  ModelStudentActivityList({this.status, this.msg, this.data});

  ModelStudentActivityList.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int? id;
  int? teacherId;
  int? studentId;
  int? lectureId;
  int? activityId;
  int? badgeId;
  String? image;
  String? description;
  dynamic? time;
  String? createdAt;
  Activity? activity;
  Badge? badge;
  Lecture? lecture;

  Data(
      {this.id,
      this.teacherId,
      this.studentId,
      this.lectureId,
      this.activityId,
      this.badgeId,
      this.image,
      this.description,
      this.time,
      this.createdAt,
      this.activity,
      this.badge,
      this.lecture});

  Data.fromJson(dynamic json) {
    id = json['id'];
    teacherId = json['teacher_id'];
    studentId = json['student_id'];
    lectureId = json['lecture_id'];
    activityId = json['activity_id'];
    badgeId = json['badge_id'];
    image = json['image'];
    description = json['description'];
    time = json['time'];
    createdAt = json['created_at'];
    activity =
        json['activity'] != null ? Activity.fromJson(json['activity']) : null;
    badge = json['badge'] != null ? Badge.fromJson(json['badge']) : null;
    lecture =
        json['lecture'] != null ? Lecture.fromJson(json['lecture']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['teacher_id'] = teacherId;
    map['student_id'] = studentId;
    map['lecture_id'] = lectureId;
    map['activity_id'] = activityId;
    map['badge_id'] = badgeId;
    map['image'] = image;
    map['description'] = description;
    map['time'] = time;
    map['created_at'] = createdAt;
    if (activity != null) {
      map['activity'] = activity?.toJson();
    }
    if (badge != null) {
      map['badge'] = badge?.toJson();
    }
    if (lecture != null) {
      map['lecture'] = lecture?.toJson();
    }
    return map;
  }
}

class Lecture {
  int? id;
  String? name;
  int? schoolId;
  int? standardId;
  String? status;

  Lecture({this.id, this.name, this.schoolId, this.standardId, this.status});

  Lecture.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    schoolId = json['school_id'];
    standardId = json['standard_id'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['school_id'] = schoolId;
    map['standard_id'] = standardId;
    map['status'] = status;
    return map;
  }
}

class Badge {
  int? id;
  int? schoolId;
  String? name;
  String? image;
  String? status;

  Badge({this.id, this.schoolId, this.name, this.image, this.status});

  Badge.fromJson(dynamic json) {
    id = json['id'];
    schoolId = json['school_id'];
    name = json['name'];
    image = json['image'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['school_id'] = schoolId;
    map['name'] = name;
    map['image'] = image;
    map['status'] = status;
    return map;
  }
}

class Activity {
  int? id;
  int? teacherId;
  int? schoolId;
  int? tdid;
  dynamic? scheduleId;
  String? name;
  String? status;

  Activity(
      {this.id,
      this.teacherId,
      this.schoolId,
      this.tdid,
      this.scheduleId,
      this.name,
      this.status});

  Activity.fromJson(dynamic json) {
    id = json['id'];
    teacherId = json['teacher_id'];
    schoolId = json['school_id'];
    tdid = json['tdid'];
    scheduleId = json['schedule_id'];
    name = json['name'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['teacher_id'] = teacherId;
    map['school_id'] = schoolId;
    map['tdid'] = tdid;
    map['schedule_id'] = scheduleId;
    map['name'] = name;
    map['status'] = status;
    return map;
  }
}
