class ModelClasses {
  int? _status;
  String? _msg;
  List<Data>? _data;

  int? get status => _status;
  String? get msg => _msg;
  List<Data>? get data => _data;

  ModelClasses({
      int? status, 
      String? msg, 
      List<Data>? data}){
    _status = status;
    _msg = msg;
    _data = data;
}

  ModelClasses.fromJson(dynamic json) {
    _status = json["status"];
    _msg = json["msg"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["msg"] = _msg;
    if (_data != null) {
      map["data"] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Data {
  int? _id;
  int? _schoolId;
  int? _teacherId;
  int? _standardId;
  int? _divisionId;
  int? _dayId;
  int? _lectureId;
  int? _tdid;
  int? _order;
  String type = "";
  String? role;
  String? _in;
  String? _out;
  String? _active;
  Lecture? _lecture;
  LectureAttendance? _lectureAttendance;
  Division? _division;
  Standard? _standard;
  int? _total;

  int? get id => _id;
  int? get schoolId => _schoolId;
  int? get teacherId => _teacherId;
  int? get standardId => _standardId;
  int? get divisionId => _divisionId;
  int? get dayId => _dayId;
  int? get lectureId => _lectureId;
  int? get tdid => _tdid;
  int? get order => _order;
  String? get ins => _in;
  String? get out => _out;
  String? get active => _active;
  Lecture? get lecture => _lecture;
  LectureAttendance? get lectureAttendance => _lectureAttendance;
  Division? get division => _division;
  Standard? get standard => _standard;
  int? get total => _total;

  Data({
      int? id, 
      int? schoolId, 
      int? teacherId, 
      int? standardId, 
      int? divisionId, 
      int? dayId, 
      int? lectureId, 
      int? tdid, 
      int? order, 
      String? ins,
      String? out, 
      String? active, 
      Lecture? lecture, 
      LectureAttendance? lectureAttendance,
      Division? division,
      Standard? standard, 
      int? total}){
    _id = id;
    _schoolId = schoolId;
    _teacherId = teacherId;
    _standardId = standardId;
    _divisionId = divisionId;
    _dayId = dayId;
    _lectureId = lectureId;
    _tdid = tdid;
    _order = order;
    _in = ins;
    _out = out;
    _active = active;
    _lecture = lecture;
    _lectureAttendance = lectureAttendance;
    _division = division;
    _standard = standard;
    _total = total;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _schoolId = json["school_id"];
    _teacherId = json["teacher_id"];
    _standardId = json["standard_id"];
    _divisionId = json["division_id"];
    _dayId = json["day_id"];
    _lectureId = json["lecture_id"];
    _tdid = json["tdid"];
    _order = json["order"];
    _in = json["in"];
    _out = json["out"];
    _active = json["active"];
    _lecture = json["lecture"] != null ? Lecture.fromJson(json["lecture"]) : null;
    _lectureAttendance = json["lecture_attendance"] != null ? LectureAttendance.fromJson(json["lecture_attendance"]) : null;
    _division = json["division"] != null ? Division.fromJson(json["division"]) : null;
    _standard = json["standard"] != null ? Standard.fromJson(json["standard"]) : null;
    _total = json["total"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["school_id"] = _schoolId;
    map["teacher_id"] = _teacherId;
    map["standard_id"] = _standardId;
    map["division_id"] = _divisionId;
    map["day_id"] = _dayId;
    map["lecture_id"] = _lectureId;
    map["tdid"] = _tdid;
    map["order"] = _order;
    map["in"] = _in;
    map["out"] = _out;
    map["active"] = _active;
    if (_lecture != null) {
      map["lecture"] = _lecture?.toJson();
    }
    if (_division != null) {
      map["division"] = _division?.toJson();
    }
    if (_standard != null) {
      map["standard"] = _standard?.toJson();
    }
    map["total"] = _total;
    return map;
  }

}

class LectureAttendance {
  int? _id;
  int? _absent;
  int? _present;
  String? _date;
  int? _divisionId;
  int? _scheduleId;
  int? _standardId;
  int? _teacherId;

  int? get id => _id;
  String? get date => _date;
  int? get present => _present;
  int? get absent => _absent;
  int? get divisionId => _divisionId;
  int? get scheduleId => _scheduleId;
  int? get standardId => _standardId;
  int? get teacherId => _teacherId;

  LectureAttendance({
    int? id,
    int? absent,
    int? present,
    String? date,
    int? divisionId,
    int? scheduleId,
    int? standardId,
    int? teacherId}){
    _id = id;
    _absent = absent;
    _present = present;
    _date = date;
    _divisionId = divisionId;
    _scheduleId = scheduleId;
    _standardId = standardId;
    _teacherId = teacherId;
  }

  LectureAttendance.fromJson(dynamic json) {
    _id = json["id"];
    _date = json["date"];
    _absent = json["absent"];
    _present = json["present"];
    _divisionId = json["division_id"];
    _standardId = json["standard_id"];
    _scheduleId = json["schedule_id"];
    _teacherId = json["teacher_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["date"] = _date;
    map["absent"] = _absent;
    map["present"] = _present;
    map["division_id"] = _divisionId;
    return map;
  }

}

class Standard {
  int? _id;
  String? _name;
  int? _schoolId;
  int? _msid;
  String? _status;

  int? get id => _id;
  String? get name => _name;
  int? get schoolId => _schoolId;
  int? get msid => _msid;
  String? get status => _status;

  Standard({
      int? id, 
      String? name, 
      int? schoolId, 
      int? msid, 
      String? status}){
    _id = id;
    _name = name;
    _schoolId = schoolId;
    _msid = msid;
    _status = status;
}

  Standard.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _schoolId = json["school_id"];
    _msid = json["msid"];
    _status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["school_id"] = _schoolId;
    map["msid"] = _msid;
    map["status"] = _status;
    return map;
  }

}

class Division {
  int? _id;
  String? _name;
  int? _schoolId;
  int? _mdid;
  String? _status;

  int? get id => _id;
  String? get name => _name;
  int? get schoolId => _schoolId;
  int? get mdid => _mdid;
  String? get status => _status;

  Division({
      int? id, 
      String? name, 
      int? schoolId, 
      int? mdid, 
      String? status}){
    _id = id;
    _name = name;
    _schoolId = schoolId;
    _mdid = mdid;
    _status = status;
}

  Division.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _schoolId = json["school_id"];
    _mdid = json["mdid"];
    _status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["school_id"] = _schoolId;
    map["mdid"] = _mdid;
    map["status"] = _status;
    return map;
  }

}

class Lecture {
  int? _id;
  String? _name;
  int? _schoolId;
  int? _standardId;
  String? _status;

  int? get id => _id;
  String? get name => _name;
  int? get schoolId => _schoolId;
  int? get standardId => _standardId;
  String? get status => _status;

  Lecture({
      int? id, 
      String? name, 
      int? schoolId, 
      int? standardId, 
      String? status}){
    _id = id;
    _name = name;
    _schoolId = schoolId;
    _standardId = standardId;
    _status = status;
}

  Lecture.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _schoolId = json["school_id"];
    _standardId = json["standard_id"];
    _status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["school_id"] = _schoolId;
    map["standard_id"] = _standardId;
    map["status"] = _status;
    return map;
  }

}