  class ModelAdminDashboard {
  int? _status;
  String? _msg;
  Data? _data;

  int? get status => _status;
  String? get msg => _msg;
  Data? get data => _data;

  ModelAdminDashboard({
      int? status, 
      String? msg, 
      Data? data}){
    _status = status;
    _msg = msg;
    _data = data;
}

  ModelAdminDashboard.fromJson(dynamic json) {
    _status = json["status"];
    _msg = json["msg"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["msg"] = _msg;
    if (_data != null) {
      map["data"] = _data?.toJson();
    }
    return map;
  }

}

class Data {
  Teacher? _teacher;
  Student? _student;
  List<dynamic>? _event;
  List<Lecture>? _lecture;

  Teacher? get teacher => _teacher;
  Student? get student => _student;
  List<dynamic>? get event => _event;
  List<Lecture>? get lecture => _lecture;

  Data({
      Teacher? teacher, 
      Student? student, 
      List<dynamic>? event, 
      List<Lecture>? lecture}){
    _teacher = teacher;
    _student = student;
    _event = event;
    _lecture = lecture;
}

  Data.fromJson(dynamic json) {
    _teacher = json["teacher"] != null ? Teacher.fromJson(json["teacher"]) : null;
    _student = json["student"] != null ? Student.fromJson(json["student"]) : null;
    if (json["event"] != null) {
      _event = [];
      /*json["event"].forEach((v) {
        _event?.add(dynamic.fromJson(v));
      });*/
    }
    if (json["lecture"] != null) {
      _lecture = [];
      json["lecture"].forEach((v) {
        _lecture?.add(Lecture.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_teacher != null) {
      map["teacher"] = _teacher?.toJson();
    }
    if (_student != null) {
      map["student"] = _student?.toJson();
    }
    if (_event != null) {
      map["event"] = _event?.map((v) => v.toJson()).toList();
    }
    if (_lecture != null) {
      map["lecture"] = _lecture?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Lecture {
  int? _id;
  String? _firstname;
  dynamic? _middlename;
  String? _lastname;
  String? _email;
  dynamic? _username;
  String? _dob;
  String? _image;
  String? _thumb;
  String? _phone;
  String? _address;
  String? _gender;
  String? _status;
  String? _role;
  int? _schoolId;
  int? _cityId;
  String? _appToken;
  List<InnerLecture>? _lecture;

  int? get id => _id;
  String? get firstname => _firstname;
  dynamic? get middlename => _middlename;
  String? get lastname => _lastname;
  String? get email => _email;
  dynamic? get username => _username;
  String? get dob => _dob;
  String? get image => _image;
  String? get thumb => _thumb;
  String? get phone => _phone;
  String? get address => _address;
  String? get gender => _gender;
  String? get status => _status;
  String? get role => _role;
  int? get schoolId => _schoolId;
  int? get cityId => _cityId;
  String? get appToken => _appToken;
  List<InnerLecture>? get lecture => _lecture;

  Lecture({
      int? id, 
      String? firstname, 
      dynamic? middlename, 
      String? lastname, 
      String? email, 
      dynamic? username, 
      String? dob, 
      String? image, 
      String? thumb, 
      String? phone, 
      String? address, 
      String? gender, 
      String? status, 
      String? role, 
      int? schoolId, 
      int? cityId, 
      String? appToken, 
      List<InnerLecture>? lecture}){
    _id = id;
    _firstname = firstname;
    _middlename = middlename;
    _lastname = lastname;
    _email = email;
    _username = username;
    _dob = dob;
    _image = image;
    _thumb = thumb;
    _phone = phone;
    _address = address;
    _gender = gender;
    _status = status;
    _role = role;
    _schoolId = schoolId;
    _cityId = cityId;
    _appToken = appToken;
    _lecture = lecture;
}

  Lecture.fromJson(dynamic json) {
    _id = json["id"];
    _firstname = json["firstname"];
    _middlename = json["middlename"];
    _lastname = json["lastname"];
    _email = json["email"];
    _username = json["username"];
    _dob = json["dob"];
    _image = json["image"];
    _thumb = json["thumb"];
    _phone = json["phone"];
    _address = json["address"];
    _gender = json["gender"];
    _status = json["status"];
    _role = json["role"];
    _schoolId = json["school_id"];
    _cityId = json["city_id"];
    _appToken = json["app_token"];
    if (json["lecture"] != null) {
      _lecture = [];
      json["lecture"].forEach((v) {
        _lecture?.add(InnerLecture.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["firstname"] = _firstname;
    map["middlename"] = _middlename;
    map["lastname"] = _lastname;
    map["email"] = _email;
    map["username"] = _username;
    map["dob"] = _dob;
    map["image"] = _image;
    map["thumb"] = _thumb;
    map["phone"] = _phone;
    map["address"] = _address;
    map["gender"] = _gender;
    map["status"] = _status;
    map["role"] = _role;
    map["school_id"] = _schoolId;
    map["city_id"] = _cityId;
    map["app_token"] = _appToken;
    if (_lecture != null) {
      map["lecture"] = _lecture?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class InnerLecture {
  Name? _name;
  List<DataTwo>? _data;

  Name? get name => _name;
  List<DataTwo>? get data => _data;

  InnerLecture({
      Name? name, 
      List<DataTwo>? data}){
    _name = name;
    _data = data;
}

  InnerLecture.fromJson(dynamic json) {
    _name = json["name"] != null ? Name.fromJson(json["name"]) : null;
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data?.add(DataTwo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_name != null) {
      map["name"] = _name?.toJson();
    }
    if (_data != null) {
      map["data"] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class DataTwo {
  Name? _name;
  List<DataThree>? _data;

  Name? get name => _name;
  List<DataThree>? get data => _data;

  DataTwo({
      Name? name, 
      List<DataThree>? data}){
    _name = name;
    _data = data;
}

  DataTwo.fromJson(dynamic json) {
    _name = json["name"] != null ? Name.fromJson(json["name"]) : null;
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data?.add(DataThree.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_name != null) {
      map["name"] = _name?.toJson();
    }
    if (_data != null) {
      map["data"] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class DataThree {
  int? _id;
  String? _name;
  int? _schoolId;
  int? _standardId;
  String? _status;

  int? get id => _id;
  String? get name => _name;
  int? get schoolId => _schoolId;
  int? get standardId => _standardId;
  String? get status => _status;

  DataThree({
      int? id, 
      String? name, 
      int? schoolId, 
      int? standardId, 
      String? status}){
    _id = id;
    _name = name;
    _schoolId = schoolId;
    _standardId = standardId;
    _status = status;
}

  DataThree.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _schoolId = json["school_id"];
    _standardId = json["standard_id"];
    _status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["school_id"] = _schoolId;
    map["standard_id"] = _standardId;
    map["status"] = _status;
    return map;
  }

}

/*class Name {
  int? _id;
  String? _name;
  int? _schoolId;
  int? _mdid;
  String? _status;

  int? get id => _id;
  String? get name => _name;
  int? get schoolId => _schoolId;
  int? get mdid => _mdid;
  String? get status => _status;

  Name({
      int? id, 
      String? name, 
      int? schoolId, 
      int? mdid, 
      String? status}){
    _id = id;
    _name = name;
    _schoolId = schoolId;
    _mdid = mdid;
    _status = status;
}

  Name.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _schoolId = json["school_id"];
    _mdid = json["mdid"];
    _status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["school_id"] = _schoolId;
    map["mdid"] = _mdid;
    map["status"] = _status;
    return map;
  }

}*/

class Name {
  int? _id;
  String? _name;
  int? _schoolId;
  int? _msid;
  String? _status;
  int? _studentsCount;

  int? get id => _id;
  String? get name => _name;
  int? get schoolId => _schoolId;
  int? get msid => _msid;
  String? get status => _status;
  int? get studentsCount => _studentsCount;

  Name({
      int? id, 
      String? name, 
      int? schoolId, 
      int? msid, 
      String? status, 
      int? studentsCount}){
    _id = id;
    _name = name;
    _schoolId = schoolId;
    _msid = msid;
    _status = status;
    _studentsCount = studentsCount;
}

  Name.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _schoolId = json["school_id"];
    _msid = json["msid"];
    _status = json["status"];
    _studentsCount = json["students_count"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["school_id"] = _schoolId;
    map["msid"] = _msid;
    map["status"] = _status;
    map["students_count"] = _studentsCount;
    return map;
  }

}

class Student {
  int? _total;
  int? _present;

  int? get total => _total;
  int? get present => _present;

  Student({
      int? total, 
      int? present}){
    _total = total;
    _present = present;
}

  Student.fromJson(dynamic json) {
    _total = json["total"];
    _present = json["present"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["total"] = _total;
    map["present"] = _present;
    return map;
  }

}

class Teacher {
  int? _total;
  int? _present;

  int? get total => _total;
  int? get present => _present;

  Teacher({
      int? total, 
      int? present}){
    _total = total;
    _present = present;
}

  Teacher.fromJson(dynamic json) {
    _total = json["total"];
    _present = json["present"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["total"] = _total;
    map["present"] = _present;
    return map;
  }

}