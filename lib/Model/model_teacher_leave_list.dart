class ModelTeacherLeaveList {
  int? status;
  String? msg;
  List<Data>? data;

  ModelTeacherLeaveList({this.status, this.msg, this.data});

  ModelTeacherLeaveList.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int? id;
  int? userId;
  int? principalId;
  String? start;
  String? end;
  int? days;
  String? reason;
  String? status;
  String? createdAt;
  Teacher? teacher;

  Data(
      {this.id,
      this.userId,
      this.principalId,
      this.start,
      this.end,
      this.days,
      this.reason,
      this.status,
      this.createdAt,
      this.teacher});

  Data.fromJson(dynamic json) {
    id = json['id'];
    userId = json['user_id'];
    principalId = json['principal_id'];
    start = json['start'];
    end = json['end'];
    days = json['days'];
    reason = json['reason'];
    status = json['status'];
    createdAt = json['created_at'];
    teacher =
        json['teacher'] != null ? Teacher.fromJson(json['teacher']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['user_id'] = userId;
    map['principal_id'] = principalId;
    map['start'] = start;
    map['end'] = end;
    map['days'] = days;
    map['reason'] = reason;
    map['status'] = status;
    map['created_at'] = createdAt;
    if (teacher != null) {
      map['teacher'] = teacher?.toJson();
    }
    return map;
  }
}

class Teacher {
  int? id;
  String? firstname;
  String? lastname;
  String? thumb;

  Teacher({this.id, this.firstname, this.lastname, this.thumb});

  Teacher.fromJson(dynamic json) {
    id = json['id'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    thumb = json['thumb'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['firstname'] = firstname;
    map['lastname'] = lastname;
    map['thumb'] = thumb;
    return map;
  }
}
