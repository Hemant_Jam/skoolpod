class ModelAddChildLeave {
  int? status;
  String? msg;
  List<Data>? data;

  ModelAddChildLeave({this.status, this.msg, this.data});

  ModelAddChildLeave.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int? id;
  String? firstname;
  String? middlename;
  String? lastname;
  String? email;
  String? username;
  String? dob;
  String? image;
  String? thumb;
  String? phone;
  String? address;
  String? gender;
  String? status;
  String? role;
  int? schoolId;
  int? cityId;
  String? appToken;

  Data(
      {this.id,
      this.firstname,
      this.middlename,
      this.lastname,
      this.email,
      this.username,
      this.dob,
      this.image,
      this.thumb,
      this.phone,
      this.address,
      this.gender,
      this.status,
      this.role,
      this.schoolId,
      this.cityId,
      this.appToken});

  Data.fromJson(dynamic json) {
    id = json['id'];
    firstname = json['firstname'];
    middlename = json['middlename'];
    lastname = json['lastname'];
    email = json['email'];
    username = json['username'];
    dob = json['dob'];
    image = json['image'];
    thumb = json['thumb'];
    phone = json['phone'];
    address = json['address'];
    gender = json['gender'];
    status = json['status'];
    role = json['role'];
    schoolId = json['school_id'];
    cityId = json['city_id'];
    appToken = json['app_token'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['firstname'] = firstname;
    map['middlename'] = middlename;
    map['lastname'] = lastname;
    map['email'] = email;
    map['username'] = username;
    map['dob'] = dob;
    map['image'] = image;
    map['thumb'] = thumb;
    map['phone'] = phone;
    map['address'] = address;
    map['gender'] = gender;
    map['status'] = status;
    map['role'] = role;
    map['school_id'] = schoolId;
    map['city_id'] = cityId;
    map['app_token'] = appToken;
    return map;
  }
}
