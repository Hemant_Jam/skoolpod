class ModelChat {
  int? _status;
  String? _msg;
  Data? _data;

  int? get status => _status;
  String? get msg => _msg;
  Data? get data => _data;

  ModelChat({
      int? status, 
      String? msg, 
      Data? data}){
    _status = status;
    _msg = msg;
    _data = data;
}

  ModelChat.fromJson(dynamic json) {
    _status = json["status"];
    _msg = json["msg"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["msg"] = _msg;
    if (_data != null) {
      map["data"] = _data?.toJson();
    }
    return map;
  }

}

class Data {
  int? _currentPage;
  List<ChatData>? _data;
  String? _firstPageUrl;
  int? _from;
  int? _lastPage;
  String? _lastPageUrl;
  dynamic? _nextPageUrl;
  String? _path;
  int? _perPage;
  dynamic? _prevPageUrl;
  int? _to;
  int? _total;

  int? get currentPage => _currentPage;
  List<ChatData>? get data => _data;
  String? get firstPageUrl => _firstPageUrl;
  int? get from => _from;
  int? get lastPage => _lastPage;
  String? get lastPageUrl => _lastPageUrl;
  dynamic? get nextPageUrl => _nextPageUrl;
  String? get path => _path;
  int? get perPage => _perPage;
  dynamic? get prevPageUrl => _prevPageUrl;
  int? get to => _to;
  int? get total => _total;

  Data({
      int? currentPage, 
      List<ChatData>? data,
      String? firstPageUrl, 
      int? from, 
      int? lastPage, 
      String? lastPageUrl, 
      dynamic? nextPageUrl, 
      String? path, 
      int? perPage, 
      dynamic? prevPageUrl, 
      int? to, 
      int? total}){
    _currentPage = currentPage;
    _data = data;
    _firstPageUrl = firstPageUrl;
    _from = from;
    _lastPage = lastPage;
    _lastPageUrl = lastPageUrl;
    _nextPageUrl = nextPageUrl;
    _path = path;
    _perPage = perPage;
    _prevPageUrl = prevPageUrl;
    _to = to;
    _total = total;
}

  Data.fromJson(dynamic json) {
    _currentPage = json["current_page"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data?.add(ChatData.fromJson(v));
      });
    }
    _firstPageUrl = json["first_page_url"];
    _from = json["from"];
    _lastPage = json["last_page"];
    _lastPageUrl = json["last_page_url"];
    _nextPageUrl = json["next_page_url"];
    _path = json["path"];
    _perPage = json["per_page"];
    _prevPageUrl = json["prev_page_url"];
    _to = json["to"];
    _total = json["total"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["current_page"] = _currentPage;
    if (_data != null) {
      map["data"] = _data?.map((v) => v.toJson()).toList();
    }
    map["first_page_url"] = _firstPageUrl;
    map["from"] = _from;
    map["last_page"] = _lastPage;
    map["last_page_url"] = _lastPageUrl;
    map["next_page_url"] = _nextPageUrl;
    map["path"] = _path;
    map["per_page"] = _perPage;
    map["prev_page_url"] = _prevPageUrl;
    map["to"] = _to;
    map["total"] = _total;
    return map;
  }

}

class ChatData {
  int? _id;
  int? _messageId;
  int? _conversationId;
  int? _userId;
  String? _isSeen;
  String? _isSender;
  String? _createdAt;
  String? _updatedAt;
  Message? _message;

  int? get id => _id;
  int? get messageId => _messageId;
  int? get conversationId => _conversationId;
  int? get userId => _userId;
  String? get isSeen => _isSeen;
  String? get isSender => _isSender;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  Message? get message => _message;

  ChatData({
      int? id, 
      int? messageId, 
      int? conversationId, 
      int? userId, 
      String? isSeen, 
      String? isSender, 
      String? createdAt, 
      String? updatedAt, 
      Message? message}){
    _id = id;
    _messageId = messageId;
    _conversationId = conversationId;
    _userId = userId;
    _isSeen = isSeen;
    _isSender = isSender;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _message = message;
}

  ChatData.fromJson(dynamic json) {
    _id = json["id"];
    _messageId = json["message_id"];
    _conversationId = json["conversation_id"];
    _userId = json["user_id"];
    _isSeen = json["is_seen"];
    _isSender = json["is_sender"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _message = json["message"] != null ? Message.fromJson(json["message"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["message_id"] = _messageId;
    map["conversation_id"] = _conversationId;
    map["user_id"] = _userId;
    map["is_seen"] = _isSeen;
    map["is_sender"] = _isSender;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    if (_message != null) {
      map["message"] = _message?.toJson();
    }
    return map;
  }

}

class Message {
  int? _id;
  int? _schoolId;
  String? _title;
  String? _body;
  int? _conversationId;
  int? _userId;
  String? _type;
  String? _extra;
  String? _createdAt;
  String? _updatedAt;
  Sender? _sender;

  int? get id => _id;
  int? get schoolId => _schoolId;
  String? get title => _title;
  String? get body => _body;
  int? get conversationId => _conversationId;
  int? get userId => _userId;
  String? get type => _type;
  String? get extra => _extra;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  Sender? get sender => _sender;

  Message({
      int? id, 
      int? schoolId, 
      String? title, 
      String? body, 
      int? conversationId, 
      int? userId, 
      String? type, 
      String? extra, 
      String? createdAt, 
      String? updatedAt, 
      Sender? sender}){
    _id = id;
    _schoolId = schoolId;
    _title = title;
    _body = body;
    _conversationId = conversationId;
    _userId = userId;
    _type = type;
    _extra = extra;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _sender = sender;
}

  Message.fromJson(dynamic json) {
    _id = json["id"];
    _schoolId = json["school_id"];
    _title = json["title"];
    _body = json["body"];
    _conversationId = json["conversation_id"];
    _userId = json["user_id"];
    _type = json["type"];
    _extra = json["extra"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _sender = json["sender"] != null ? Sender.fromJson(json["sender"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["school_id"] = _schoolId;
    map["title"] = _title;
    map["body"] = _body;
    map["conversation_id"] = _conversationId;
    map["user_id"] = _userId;
    map["type"] = _type;
    map["extra"] = _extra;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    if (_sender != null) {
      map["sender"] = _sender?.toJson();
    }
    return map;
  }

}

class Sender {
  int? _id;
  String? _firstname;
  String? _lastname;
  String? _thumb;

  int? get id => _id;
  String? get firstname => _firstname;
  String? get lastname => _lastname;
  String? get thumb => _thumb;

  Sender({
      int? id, 
      String? firstname, 
      String? lastname, 
      String? thumb}){
    _id = id;
    _firstname = firstname;
    _lastname = lastname;
    _thumb = thumb;
}

  Sender.fromJson(dynamic json) {
    _id = json["id"];
    _firstname = json["firstname"];
    _lastname = json["lastname"];
    _thumb = json["thumb"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["firstname"] = _firstname;
    map["lastname"] = _lastname;
    map["thumb"] = _thumb;
    return map;
  }

}