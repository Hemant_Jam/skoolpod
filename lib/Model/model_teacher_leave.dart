class ModelTeacherLeave {
  int? status;
  String? msg;
  List<Data>? data;

  ModelTeacherLeave({this.status, this.msg, this.data});

  ModelTeacherLeave.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int? id;
  int? userId;
  int? principalId;
  String? start;
  String? end;
  num? days;
  String? reason;
  String? status;
  String? createdAt;

  Data(
      {this.id,
      this.userId,
      this.principalId,
      this.start,
      this.end,
      this.days,
      this.reason,
      this.status,
      this.createdAt});

  Data.fromJson(dynamic json) {
    id = json['id'];
    userId = json['user_id'];
    principalId = json['principal_id'];
    start = json['start'];
    end = json['end'];
    days = json['days'];
    reason = json['reason'];
    status = json['status'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['user_id'] = userId;
    map['principal_id'] = principalId;
    map['start'] = start;
    map['end'] = end;
    map['days'] = days;
    map['reason'] = reason;
    map['status'] = status;
    map['created_at'] = createdAt;
    return map;
  }
}
