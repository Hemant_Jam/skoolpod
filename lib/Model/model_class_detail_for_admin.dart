class ModelClassDetailForAdmin {
  int? status;
  String? msg;
  Data? data;

  ModelClassDetailForAdmin({this.status, this.msg, this.data});

  ModelClassDetailForAdmin.fromJson(dynamic json) {
    status = json["status"];
    msg = json["msg"];
    data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["msg"] = msg;
    if (data != null) {
      map["data"] = data?.toJson();
    }
    return map;
  }
}

class Data {
  List<Teacher>? teacher;
  List<Student>? student;

  Data({this.teacher, this.student});

  Data.fromJson(dynamic json) {
    if (json["teacher"] != null) {
      teacher = [];
      json["teacher"].forEach((v) {
        teacher?.add(Teacher.fromJson(v));
      });
    }
    if (json["student"] != null) {
      student = [];
      json["student"].forEach((v) {
        student?.add(Student.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (teacher != null) {
      map["teacher"] = teacher?.map((v) => v.toJson()).toList();
    }
    if (student != null) {
      map["student"] = student?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Student {
  int? id;
  String? firstname;
  String? lastname;
  String? thumb;
  Attendance? attendance;

  Student(
      {this.id, this.firstname, this.lastname, this.thumb, this.attendance});

  Student.fromJson(dynamic json) {
    id = json["id"];
    firstname = json["firstname"];
    lastname = json["lastname"];
    thumb = json["thumb"];
    attendance = json["attendance"] != null
        ? Attendance.fromJson(json["attendance"])
        : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firstname"] = firstname;
    map["lastname"] = lastname;
    map["thumb"] = thumb;
    if (attendance != null) {
      map["attendance"] = attendance?.toJson();
    }
    return map;
  }
}

class Attendance {
  int? present;
  int? absent;
  String? datePresent;

  Attendance({this.present, this.absent, this.datePresent});

  Attendance.fromJson(dynamic json) {
    present = json["present"];
    absent = json["absent"];
    datePresent = json["date_present"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["present"] = present;
    map["absent"] = absent;
    map["date_present"] = datePresent;
    return map;
  }
}

class Teacher {
  int? id;
  String? firstname;
  String? lastname;
  String? thumb;
  List<AttendanceTeacher>? attendance;

  Teacher(
      {this.id, this.firstname, this.lastname, this.thumb, this.attendance});

  Teacher.fromJson(dynamic json) {
    id = json["id"];
    firstname = json["firstname"];
    lastname = json["lastname"];
    thumb = json["thumb"];
    if (json["attendance"] != null) {
      attendance = [];
      json["attendance"].forEach((v) {
        attendance?.add(AttendanceTeacher.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firstname"] = firstname;
    map["lastname"] = lastname;
    map["thumb"] = thumb;
    if (attendance != null) {
      map["attendance"] = attendance?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class AttendanceTeacher {
  int? id;
  String? date;
  String? ins;
  String? out;

  AttendanceTeacher({this.id, this.date, this.ins, this.out});

  AttendanceTeacher.fromJson(dynamic json) {
    id = json["id"];
    date = json["date"];
    ins = json["in"];
    out = json["out"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["date"] = date;
    map["in"] = ins;
    map["out"] = out;
    return map;
  }
}
