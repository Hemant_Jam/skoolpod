class ModelTeacherAttendance {
  int? status;
  String? msg;
  Data? data;

  ModelTeacherAttendance({
      this.status, 
      this.msg, 
      this.data});

  ModelTeacherAttendance.fromJson(dynamic json) {
    status = json["status"];
    msg = json["msg"];
    data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["msg"] = msg;
    if (data != null) {
      map["data"] = data?.toJson();
    }
    return map;
  }

}

class Data {
  int? id;
  int? teacherId;
  String? date;
  String? ins;
  String? out;

  Data({
      this.id, 
      this.teacherId, 
      this.date, 
      this.ins,
      this.out});

  Data.fromJson(dynamic json) {
    id = json["id"];
    teacherId = json["teacher_id"];
    date = json["date"];
    ins = json["in"];
    out = json["out"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["teacher_id"] = teacherId;
    map["date"] = date;
    map["in"] = ins;
    map["out"] = out;
    return map;
  }

}