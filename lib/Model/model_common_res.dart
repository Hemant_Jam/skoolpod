/// status : 0
/// msg : "validation.required"

class ModelCommonRes {
  int? _status;
  String? _msg;

  int? get status => _status;
  String? get msg => _msg;

  ModelCommonRes({
      int? status, 
      String? msg}){
    _status = status;
    _msg = msg;
}

  ModelCommonRes.fromJson(dynamic json) {
    _status = json["status"];
    _msg = json["msg"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["msg"] = _msg;
    return map;
  }

}