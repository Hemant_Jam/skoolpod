class ModelChildList {
  int? status;
  String? msg;
  List<Data>? data;

  ModelChildList({this.status, this.msg, this.data});

  ModelChildList.fromJson(dynamic json) {
    status = json["status"];
    msg = json["msg"];
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["msg"] = msg;
    if (data != null) {
      map["data"] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int? id;
  String? firstname;
  dynamic? middlename;
  String? lastname;
  String? dob;
  String? image;
  String? thumb;
  String? gender;
  int? schoolId;
  int? standardId;
  int? divisionId;
  int? parentId;
  String? status;
  Standard? standard;
  Division? division;

  Data(
      {this.id,
      this.firstname,
      this.middlename,
      this.lastname,
      this.dob,
      this.image,
      this.thumb,
      this.gender,
      this.schoolId,
      this.standardId,
      this.divisionId,
      this.parentId,
      this.status,
      this.standard,
      this.division});

  Data.fromJson(dynamic json) {
    id = json["id"];
    firstname = json["firstname"];
    middlename = json["middlename"];
    lastname = json["lastname"];
    dob = json["dob"];
    image = json["image"];
    thumb = json["thumb"];
    gender = json["gender"];
    schoolId = json["school_id"];
    standardId = json["standard_id"];
    divisionId = json["division_id"];
    parentId = json["parent_id"];
    status = json["status"];
    standard =
        json["standard"] != null ? Standard.fromJson(json["standard"]) : null;
    division =
        json["division"] != null ? Division.fromJson(json["division"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firstname"] = firstname;
    map["middlename"] = middlename;
    map["lastname"] = lastname;
    map["dob"] = dob;
    map["image"] = image;
    map["thumb"] = thumb;
    map["gender"] = gender;
    map["school_id"] = schoolId;
    map["standard_id"] = standardId;
    map["division_id"] = divisionId;
    map["parent_id"] = parentId;
    map["status"] = status;
    if (standard != null) {
      map["standard"] = standard?.toJson();
    }
    if (division != null) {
      map["division"] = division?.toJson();
    }
    return map;
  }
}

class Division {
  int? id;
  String? name;
  int? schoolId;
  int? mdid;
  String? status;

  Division({this.id, this.name, this.schoolId, this.mdid, this.status});

  Division.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    schoolId = json["school_id"];
    mdid = json["mdid"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["school_id"] = schoolId;
    map["mdid"] = mdid;
    map["status"] = status;
    return map;
  }
}

class Standard {
  int? id;
  String? name;
  int? schoolId;
  int? msid;
  String? status;

  Standard({this.id, this.name, this.schoolId, this.msid, this.status});

  Standard.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    schoolId = json["school_id"];
    msid = json["msid"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["school_id"] = schoolId;
    map["msid"] = msid;
    map["status"] = status;
    return map;
  }
}
