class ModelStudentList {
  int? status;
  String? msg;
  List<Data>? data;

  ModelStudentList({
      this.status, 
      this.msg, 
      this.data});

  ModelStudentList.fromJson(dynamic json) {
    status = json["status"];
    msg = json["msg"];
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["msg"] = msg;
    if (data != null) {
      map["data"] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Data {
  int? id;
  String? firstname;
  dynamic? middlename;
  String? lastname;
  Attendance? attendance;
  String? dob;
  String? image;
  dynamic? thumb;
  String? gender;
  int? schoolId;
  int? standardId;
  int? divisionId;
  bool isPresent = true;
  dynamic? parentId;
  String? status;

  Data({
      this.id, 
      this.firstname, 
      this.middlename, 
      this.lastname, 
      this.dob, 
      this.image, 
      this.thumb, 
      this.gender, 
      this.schoolId, 
      this.standardId, 
      this.divisionId, 
      this.parentId, 
      this.status});

  Data.fromJson(dynamic json) {
    id = json["id"];
    firstname = json["firstname"];
    middlename = json["middlename"];
    lastname = json["lastname"];
    dob = json["dob"];
    image = json["image"];
    thumb = json["thumb"];
    gender = json["gender"];
    schoolId = json["school_id"];
    standardId = json["standard_id"];
    divisionId = json["division_id"];
    parentId = json["parent_id"];
    status = json["status"];
    isPresent = json["attendance"] != null ? json["attendance"]["date_present"] == "1" : true;
    attendance = json["attendance"] != null ? Attendance.fromJson(json["attendance"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firstname"] = firstname;
    map["middlename"] = middlename;
    map["lastname"] = lastname;
    map["dob"] = dob;
    map["image"] = image;
    map["thumb"] = thumb;
    map["gender"] = gender;
    map["school_id"] = schoolId;
    map["standard_id"] = standardId;
    map["division_id"] = divisionId;
    map["parent_id"] = parentId;
    map["status"] = status;
    return map;
  }

}
class Attendance {
  int? _lateid;
  int? _absent;
  int? _present;
  String? _date_present;
  String? _eltype;
  String? _time;


  int? get lateId => _lateid;
  String? get datePresent => _date_present;
  int? get present => _present;
  int? get absent => _absent;
  String? get elType => _eltype;
  String? get time => _time;

  Attendance({
    int? id,
    int? absent,
    int? present,
    String? date,
    int? divisionId}){
    _lateid = id;
    _absent = absent;
    _present = present;
    _date_present = date;
  }

  Attendance.fromJson(dynamic json) {
    _lateid = json["lateid"];
    _date_present = json["date_present"];
    _absent = json["absent"];
    _present = json["present"];
    _eltype = json["eltype"];
    _time = json["time"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["lateid"] = _lateid;
    map["date_present"] = _date_present;
    map["absent"] = _absent;
    map["present"] = _present;
    return map;
  }

}