/// status : 1
/// msg : "Login successfully."

class ModelLogin {
  int? _status;
  String? _msg;
  Data? _data;

  int? get status => _status;
  String? get msg => _msg;
  Data? get data => _data;

  ModelLogin({
      int? status, 
      String? msg, 
      Data? data}){
    _status = status;
    _msg = msg;
    _data = data;
}

  ModelLogin.fromJson(dynamic json) {
    _status = json["status"];
    _msg = json["msg"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["msg"] = _msg;
    if (_data != null) {
      map["data"] = _data?.toJson();
    }
    return map;
  }

}

/// id : 344
/// firstname : "Teacher"
/// middlename : null
/// lastname : "HMT"
/// email : "tehmt@mailinator.com"
/// username : null
/// dob : "2021-03-01"
/// image : "users/1615897584-72480.png"
/// thumb : "users/thumbnail/1615897584-72480.png"
/// phone : "1234567890"
/// address : "Ahmedabad"
/// gender : "Male"
/// status : "Active"
/// role : "Teacher"
/// school_id : 48
/// city_id : 1
/// app_token : "MjAyMTA3MTIxNjE0MDQ="
/// school : {"id":48,"name":"Hmt school","phone":"1234567890","email":"hmt@mailinator.com","address":"Ahmedabad","status":"Active","city_id":1,"logo":"logos/1625124791-97427.png","city":{"id":1,"name":"Ahmedabad","state_id":1,"status":"Active","state":{"id":1,"name":"Gujarat","country_id":1,"status":"Active","country":{"id":1,"name":"India","code":"IN","status":"Active"}}}}
/// city : {"id":1,"name":"Ahmedabad","state_id":1,"status":"Active","state":{"id":1,"name":"Gujarat","country_id":1,"status":"Active","country":{"id":1,"name":"India","code":"IN","status":"Active"}}}

class Data {
  int? _id;
  String? _firstname;
  dynamic? _middlename;
  String? _lastname;
  String? _email;
  dynamic? _username;
  String? _dob;
  String? _image;
  String? _thumb;
  String? _phone;
  String? _address;
  String? _gender;
  String? _status;
  String? _role;
  int? _schoolId;
  int? _cityId;
  String? _appToken;
  School? _school;
  City? _city;

  int? get id => _id;
  String? get firstname => _firstname;
  dynamic? get middlename => _middlename;
  String? get lastname => _lastname;
  String? get email => _email;
  dynamic? get username => _username;
  String? get dob => _dob;
  String? get image => _image;
  String? get thumb => _thumb;
  String? get phone => _phone;
  String? get address => _address;
  String? get gender => _gender;
  String? get status => _status;
  String? get role => _role;
  int? get schoolId => _schoolId;
  int? get cityId => _cityId;
  String? get appToken => _appToken;
  School? get school => _school;
  City? get city => _city;

  Data({
      int? id, 
      String? firstname, 
      dynamic? middlename, 
      String? lastname, 
      String? email, 
      dynamic? username, 
      String? dob, 
      String? image, 
      String? thumb, 
      String? phone, 
      String? address, 
      String? gender, 
      String? status, 
      String? role, 
      int? schoolId, 
      int? cityId, 
      String? appToken, 
      School? school, 
      City? city}){
    _id = id;
    _firstname = firstname;
    _middlename = middlename;
    _lastname = lastname;
    _email = email;
    _username = username;
    _dob = dob;
    _image = image;
    _thumb = thumb;
    _phone = phone;
    _address = address;
    _gender = gender;
    _status = status;
    _role = role;
    _schoolId = schoolId;
    _cityId = cityId;
    _appToken = appToken;
    _school = school;
    _city = city;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _firstname = json["firstname"];
    _middlename = json["middlename"];
    _lastname = json["lastname"];
    _email = json["email"];
    _username = json["username"];
    _dob = json["dob"];
    _image = json["image"];
    _thumb = json["thumb"];
    _phone = json["phone"];
    _address = json["address"];
    _gender = json["gender"];
    _status = json["status"];
    _role = json["role"];
    _schoolId = json["school_id"];
    _cityId = json["city_id"];
    _appToken = json["app_token"];
    _school = json["school"] != null ? School.fromJson(json["school"]) : null;
    _city = json["city"] != null ? City.fromJson(json["city"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["firstname"] = _firstname;
    map["middlename"] = _middlename;
    map["lastname"] = _lastname;
    map["email"] = _email;
    map["username"] = _username;
    map["dob"] = _dob;
    map["image"] = _image;
    map["thumb"] = _thumb;
    map["phone"] = _phone;
    map["address"] = _address;
    map["gender"] = _gender;
    map["status"] = _status;
    map["role"] = _role;
    map["school_id"] = _schoolId;
    map["city_id"] = _cityId;
    map["app_token"] = _appToken;
    if (_school != null) {
      map["school"] = _school?.toJson();
    }
    if (_city != null) {
      map["city"] = _city?.toJson();
    }
    return map;
  }

}

/// id : 48
/// name : "Hmt school"
/// phone : "1234567890"
/// email : "hmt@mailinator.com"
/// address : "Ahmedabad"
/// status : "Active"
/// city_id : 1
/// logo : "logos/1625124791-97427.png"
/// city : {"id":1,"name":"Ahmedabad","state_id":1,"status":"Active","state":{"id":1,"name":"Gujarat","country_id":1,"status":"Active","country":{"id":1,"name":"India","code":"IN","status":"Active"}}}

class School {
  int? _id;
  String? _name;
  String? _phone;
  String? _email;
  String? _address;
  String? _status;
  int? _cityId;
  String? _logo;
  City? _city;

  int? get id => _id;
  String? get name => _name;
  String? get phone => _phone;
  String? get email => _email;
  String? get address => _address;
  String? get status => _status;
  int? get cityId => _cityId;
  String? get logo => _logo;
  City? get city => _city;

  School({
      int? id, 
      String? name, 
      String? phone, 
      String? email, 
      String? address, 
      String? status, 
      int? cityId, 
      String? logo, 
      City? city}){
    _id = id;
    _name = name;
    _phone = phone;
    _email = email;
    _address = address;
    _status = status;
    _cityId = cityId;
    _logo = logo;
    _city = city;
}

  School.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _phone = json["phone"];
    _email = json["email"];
    _address = json["address"];
    _status = json["status"];
    _cityId = json["city_id"];
    _logo = json["logo"];
    _city = json["city"] != null ? City.fromJson(json["city"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["phone"] = _phone;
    map["email"] = _email;
    map["address"] = _address;
    map["status"] = _status;
    map["city_id"] = _cityId;
    map["logo"] = _logo;
    if (_city != null) {
      map["city"] = _city?.toJson();
    }
    return map;
  }

}

/// id : 1
/// name : "Ahmedabad"
/// state_id : 1
/// status : "Active"
/// state : {"id":1,"name":"Gujarat","country_id":1,"status":"Active","country":{"id":1,"name":"India","code":"IN","status":"Active"}}

class City {
  int? _id;
  String? _name;
  int? _stateId;
  String? _status;
  State? _state;

  int? get id => _id;
  String? get name => _name;
  int? get stateId => _stateId;
  String? get status => _status;
  State? get state => _state;

  City({
      int? id, 
      String? name, 
      int? stateId, 
      String? status, 
      State? state}){
    _id = id;
    _name = name;
    _stateId = stateId;
    _status = status;
    _state = state;
}

  City.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _stateId = json["state_id"];
    _status = json["status"];
    _state = json["state"] != null ? State.fromJson(json["state"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["state_id"] = _stateId;
    map["status"] = _status;
    if (_state != null) {
      map["state"] = _state?.toJson();
    }
    return map;
  }

}

/// id : 1
/// name : "Gujarat"
/// country_id : 1
/// status : "Active"
/// country : {"id":1,"name":"India","code":"IN","status":"Active"}

class State {
  int? _id;
  String? _name;
  int? _countryId;
  String? _status;
  Country? _country;

  int? get id => _id;
  String? get name => _name;
  int? get countryId => _countryId;
  String? get status => _status;
  Country? get country => _country;

  State({
      int? id, 
      String? name, 
      int? countryId, 
      String? status, 
      Country? country}){
    _id = id;
    _name = name;
    _countryId = countryId;
    _status = status;
    _country = country;
}

  State.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _countryId = json["country_id"];
    _status = json["status"];
    _country = json["country"] != null ? Country.fromJson(json["country"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["country_id"] = _countryId;
    map["status"] = _status;
    if (_country != null) {
      map["country"] = _country?.toJson();
    }
    return map;
  }

}

/// id : 1
/// name : "India"
/// code : "IN"
/// status : "Active"

class Country {
  int? _id;
  String? _name;
  String? _code;
  String? _status;

  int? get id => _id;
  String? get name => _name;
  String? get code => _code;
  String? get status => _status;

  Country({
      int? id, 
      String? name, 
      String? code, 
      String? status}){
    _id = id;
    _name = name;
    _code = code;
    _status = status;
}

  Country.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _code = json["code"];
    _status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["code"] = _code;
    map["status"] = _status;
    return map;
  }

}