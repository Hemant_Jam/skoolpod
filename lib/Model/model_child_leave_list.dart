class ModelChildLeaveList {
  int? status;
  String? msg;
  List<Data>? data;

  ModelChildLeaveList({this.status, this.msg, this.data});

  ModelChildLeaveList.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int? id;
  int? studentId;
  int? teacherId;
  int? schoolId;
  int? standardId;
  int? divisionId;
  String? start;
  String? end;
  int? days;
  String? reason;
  String? type;
  String? status;

  Data(
      {this.id,
      this.studentId,
      this.teacherId,
      this.schoolId,
      this.standardId,
      this.divisionId,
      this.start,
      this.end,
      this.days,
      this.reason,
      this.type,
      this.status});

  Data.fromJson(dynamic json) {
    id = json['id'];
    studentId = json['student_id'];
    teacherId = json['teacher_id'];
    schoolId = json['school_id'];
    standardId = json['standard_id'];
    divisionId = json['division_id'];
    start = json['start'];
    end = json['end'];
    days = json['days'];
    reason = json['reason'];
    type = json['type'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['student_id'] = studentId;
    map['teacher_id'] = teacherId;
    map['school_id'] = schoolId;
    map['standard_id'] = standardId;
    map['division_id'] = divisionId;
    map['start'] = start;
    map['end'] = end;
    map['days'] = days;
    map['reason'] = reason;
    map['type'] = type;
    map['status'] = status;
    return map;
  }
}
