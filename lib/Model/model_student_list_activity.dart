class ModelStudentListActivity {
  int? status;
  String? msg;
  List<Data>? data;

  ModelStudentListActivity({this.status, this.msg, this.data});

  ModelStudentListActivity.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int? id;
  String? firstname;
  String? lastname;
  String? thumb;

  Data({this.id, this.firstname, this.lastname, this.thumb});

  Data.fromJson(dynamic json) {
    id = json['id'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    thumb = json['thumb'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['firstname'] = firstname;
    map['lastname'] = lastname;
    map['thumb'] = thumb;
    return map;
  }
}
