class ModelAttendance {
  int? status;
  String? msg;
  List<StandardData>? data;

  ModelAttendance({this.status, this.msg, this.data});

  ModelAttendance.fromJson(dynamic json) {
    status = json["status"];
    msg = json["msg"];
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data?.add(StandardData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["msg"] = msg;
    if (data != null) {
      map["data"] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class StandardData {
  Name? name;
  List<DivData>? data;

  StandardData({this.name, this.data});

  StandardData.fromJson(dynamic json) {
    name = json["name"] != null ? Name.fromJson(json["name"]) : null;
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data?.add(DivData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (name != null) {
      map["name"] = name?.toJson();
    }
    if (data != null) {
      map["data"] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class DivData {
  Name? name;
  List<ClassData>? data;
  bool isExpand = false;

  DivData({this.name, this.data});

  DivData.fromJson(dynamic json) {
    name = json["name"] != null ? Name.fromJson(json["name"]) : null;
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data?.add(ClassData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (name != null) {
      map["name"] = name?.toJson();
    }
    if (data != null) {
      map["data"] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class ClassData {
  Name? name;
  List<Data4>? data;

  ClassData({this.name, this.data});

  ClassData.fromJson(dynamic json) {
    name = json["name"] != null ? Name.fromJson(json["name"]) : null;
    if (json["data"] != null) {
      data = [];
      json["data"].forEach((v) {
        data?.add(Data4.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (name != null) {
      map["name"] = name?.toJson();
    }
    if (data != null) {
      map["data"] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data4 {
  int? id;
  String? firstname;
  String? lastname;
  String? image;
  String? thumb;

  Data4({this.id, this.firstname, this.lastname, this.image, this.thumb});

  Data4.fromJson(dynamic json) {
    id = json["id"];
    firstname = json["firstname"];
    lastname = json["lastname"];
    image = json["image"];
    thumb = json["thumb"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firstname"] = firstname;
    map["lastname"] = lastname;
    map["image"] = image;
    map["thumb"] = thumb;
    return map;
  }
}

class Name {
  int? id;
  String? name;
  int? schoolId;
  int? standardId;
  String? status;
  int? mdid;
  String stdName = "";
  int stdID = 0;

  Name({this.id, this.name, this.schoolId, this.standardId, this.status});

  Name.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    schoolId = json["school_id"];
    standardId = json["standard_id"] != null ? json["standard_id"] : null;
    mdid = json["mdid"] != null ? json["mdid"] : null;
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["school_id"] = schoolId;
    map["standard_id"] = standardId;
    map["status"] = status;
    return map;
  }
}

/*
class Name {
  int? id;
  String? name;
  int? schoolId;
  int? mdid;
  String? status;

  Name({
      this.id,
      this.name,
      this.schoolId,
      this.mdid,
      this.status});

  Name.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    schoolId = json["school_id"];
    mdid = json["mdid"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["school_id"] = schoolId;
    map["mdid"] = mdid;
    map["status"] = status;
    return map;
  }

}

class Name {
  int? id;
  String? name;
  int? schoolId;
  int? msid;
  String? status;

  Name({
      this.id,
      this.name,
      this.schoolId,
      this.msid,
      this.status});

  Name.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    schoolId = json["school_id"];
    msid = json["msid"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["school_id"] = schoolId;
    map["msid"] = msid;
    map["status"] = status;
    return map;
  }

}*/
