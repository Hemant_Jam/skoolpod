class ModelStudentProfile {
  int? status;
  String? msg;
  Data? data;

  ModelStudentProfile({
      this.status, 
      this.msg, 
      this.data});

  ModelStudentProfile.fromJson(dynamic json) {
    status = json["status"];
    msg = json["msg"];
    data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["msg"] = msg;
    if (data != null) {
      map["data"] = data?.toJson();
    }
    return map;
  }

}

class Data {
  int? id;
  String? firstname;
  dynamic? middleName;
  String? lastname;
  String? dob;
  String? image;
  String? thumb;
  String? gender;
  int? schoolId;
  int? standardId;
  int? divisionId;
  int? parentId;
  String? status;
  Parent? parent;
  Standard? standard;
  Division? division;

  Data({
      this.id, 
      this.firstname, 
      this.middleName,
      this.lastname, 
      this.dob, 
      this.image, 
      this.thumb, 
      this.gender, 
      this.schoolId, 
      this.standardId, 
      this.divisionId, 
      this.parentId, 
      this.status, 
      this.parent, 
      this.standard, 
      this.division});

  Data.fromJson(dynamic json) {
    id = json["id"];
    firstname = json["firstname"];
    middleName = json["middlename"];
    lastname = json["lastname"];
    dob = json["dob"];
    image = json["image"];
    thumb = json["thumb"];
    gender = json["gender"];
    schoolId = json["school_id"];
    standardId = json["standard_id"];
    divisionId = json["division_id"];
    parentId = json["parent_id"];
    status = json["status"];
    parent = json["parent"] != null ? Parent.fromJson(json["parent"]) : null;
    standard = json["standard"] != null ? Standard.fromJson(json["standard"]) : null;
    division = json["division"] != null ? Division.fromJson(json["division"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firstname"] = firstname;
    map["middlename"] = middleName;
    map["lastname"] = lastname;
    map["dob"] = dob;
    map["image"] = image;
    map["thumb"] = thumb;
    map["gender"] = gender;
    map["school_id"] = schoolId;
    map["standard_id"] = standardId;
    map["division_id"] = divisionId;
    map["parent_id"] = parentId;
    map["status"] = status;
    if (parent != null) {
      map["parent"] = parent?.toJson();
    }
    if (standard != null) {
      map["standard"] = standard?.toJson();
    }
    if (division != null) {
      map["division"] = division?.toJson();
    }
    return map;
  }

}

class Division {
  int? id;
  String? name;
  int? schoolId;
  int? mdid;
  String? status;

  Division({
      this.id, 
      this.name, 
      this.schoolId, 
      this.mdid, 
      this.status});

  Division.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    schoolId = json["school_id"];
    mdid = json["mdid"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["school_id"] = schoolId;
    map["mdid"] = mdid;
    map["status"] = status;
    return map;
  }

}

class Standard {
  int? id;
  String? name;
  int? schoolId;
  int? msid;
  String? status;

  Standard({
      this.id, 
      this.name, 
      this.schoolId, 
      this.msid, 
      this.status});

  Standard.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    schoolId = json["school_id"];
    msid = json["msid"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["school_id"] = schoolId;
    map["msid"] = msid;
    map["status"] = status;
    return map;
  }

}

class Parent {
  int? id;
  String? firstname;
  dynamic? middlename;
  String? lastname;
  String? email;
  dynamic? username;
  String? dob;
  String? image;
  String? thumb;
  String? phone;
  String? address;
  String? gender;
  String? status;
  String? role;
  int? schoolId;
  int? cityId;
  String? appToken;

  Parent({
      this.id, 
      this.firstname, 
      this.middlename, 
      this.lastname, 
      this.email, 
      this.username, 
      this.dob, 
      this.image, 
      this.thumb, 
      this.phone, 
      this.address, 
      this.gender, 
      this.status, 
      this.role, 
      this.schoolId, 
      this.cityId, 
      this.appToken});

  Parent.fromJson(dynamic json) {
    id = json["id"];
    firstname = json["firstname"];
    middlename = json["middlename"];
    lastname = json["lastname"];
    email = json["email"];
    username = json["username"];
    dob = json["dob"];
    image = json["image"];
    thumb = json["thumb"];
    phone = json["phone"];
    address = json["address"];
    gender = json["gender"];
    status = json["status"];
    role = json["role"];
    schoolId = json["school_id"];
    cityId = json["city_id"];
    appToken = json["app_token"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["firstname"] = firstname;
    map["middlename"] = middlename;
    map["lastname"] = lastname;
    map["email"] = email;
    map["username"] = username;
    map["dob"] = dob;
    map["image"] = image;
    map["thumb"] = thumb;
    map["phone"] = phone;
    map["address"] = address;
    map["gender"] = gender;
    map["status"] = status;
    map["role"] = role;
    map["school_id"] = schoolId;
    map["city_id"] = cityId;
    map["app_token"] = appToken;
    return map;
  }

}