class ModelStudentActivity {
  int? status;
  String? msg;
  List<ActivityData>? data;

  ModelStudentActivity({this.status, this.msg, this.data});

  ModelStudentActivity.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(ActivityData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class ActivityData {
  DivisionName? name;
  List<StandardData>? data;

  ActivityData({this.name, this.data});

  ActivityData.fromJson(dynamic json) {
    name = json['name'] != null ? DivisionName.fromJson(json['name']) : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(StandardData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (name != null) {
      map['name'] = name?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class StandardData {
  DivisionName? name;
  List<LectureData>? data;

  StandardData({this.name, this.data});

  StandardData.fromJson(dynamic json) {
    name = json['name'] != null ? DivisionName.fromJson(json['name']) : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(LectureData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (name != null) {
      map['name'] = name?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class LectureData {
  int? id;
  String? name;
  int? schoolId;
  int? standardId;
  String? status;

  LectureData(
      {this.id, this.name, this.schoolId, this.standardId, this.status});

  LectureData.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    schoolId = json['school_id'];
    standardId = json['standard_id'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['school_id'] = schoolId;
    map['standard_id'] = standardId;
    map['status'] = status;
    return map;
  }
}

class DivisionName {
  int? id;
  String? name;
  int? schoolId;
  int? mdid;
  String? status;

  DivisionName({this.id, this.name, this.schoolId, this.mdid, this.status});

  DivisionName.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    schoolId = json['school_id'];
    mdid = json['mdid'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['school_id'] = schoolId;
    map['mdid'] = mdid;
    map['status'] = status;
    return map;
  }
}

class StandardName {
  int? id;
  String? name;
  int? schoolId;
  int? msid;
  String? status;
  int? studentsCount;

  StandardName(
      {this.id,
      this.name,
      this.schoolId,
      this.msid,
      this.status,
      this.studentsCount});

  StandardName.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    schoolId = json['school_id'];
    msid = json['msid'];
    status = json['status'];
    studentsCount = json['students_count'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['school_id'] = schoolId;
    map['msid'] = msid;
    map['status'] = status;
    map['students_count'] = studentsCount;
    return map;
  }
}
