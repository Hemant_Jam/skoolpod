// class ActivityModel {
//   int? status;
//   String? msg;
//   List<ClassData>? classData;
//
//   ActivityModel({this.status, this.msg, this.classData});
//
//   ActivityModel.fromJson(dynamic json) {
//     status = json['status'];
//     msg = json['msg'];
//     if (json['classData'] != null) {
//       classData = [];
//       json['classData'].forEach((v) {
//         classData?.add(ClassData.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['status'] = status;
//     map['msg'] = msg;
//     if (classData != null) {
//       map['classData'] = classData?.map((v) => v.toJson()).toList();
//     }
//     return map;
//   }
// }
//
// class ClassData {
//   StandardNameData? standardNameData;
//   List<DivisionData>? divisionData;
//
//   ClassData({this.standardNameData, this.divisionData});
//
//   ClassData.fromJson(dynamic json) {
//     standardNameData = json['standardNameData'] != null
//         ? StandardNameData.fromJson(json['standardNameData'])
//         : null;
//     if (json['divisionData'] != null) {
//       divisionData = [];
//       json['divisionData'].forEach((v) {
//         divisionData?.add(DivisionData.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     if (standardNameData != null) {
//       map['standardNameData'] = standardNameData?.toJson();
//     }
//     if (divisionData != null) {
//       map['divisionData'] = divisionData?.map((v) => v.toJson()).toList();
//     }
//     return map;
//   }
// }
//
// class DivisionData {
//   DivisionNameData? divisionNameData;
//   List<LectureData>? lectureData;
//
//   DivisionData({this.divisionNameData, this.lectureData});
//
//   DivisionData.fromJson(dynamic json) {
//     divisionNameData = json['divisionNameData'] != null
//         ? DivisionNameData.fromJson(json['divisionNameData'])
//         : null;
//     if (json['lectureData'] != null) {
//       lectureData = [];
//       json['lectureData'].forEach((v) {
//         lectureData?.add(LectureData.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     if (divisionNameData != null) {
//       map['divisionNameData'] = divisionNameData?.toJson();
//     }
//     if (lectureData != null) {
//       map['lectureData'] = lectureData?.map((v) => v.toJson()).toList();
//     }
//     return map;
//   }
// }
//
// class LectureData {
//   int? lectureId;
//   String? lectureName;
//   int? schoolId;
//   int? standardId;
//   String? status;
//
//   LectureData(
//       {this.lectureId,
//       this.lectureName,
//       this.schoolId,
//       this.standardId,
//       this.status});
//
//   LectureData.fromJson(dynamic json) {
//     lectureId = json['lectureId'];
//     lectureName = json['lectureName'];
//     schoolId = json['school_id'];
//     standardId = json['standard_id'];
//     status = json['status'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['lectureId'] = lectureId;
//     map['lectureName'] = lectureName;
//     map['school_id'] = schoolId;
//     map['standard_id'] = standardId;
//     map['status'] = status;
//     return map;
//   }
// }
//
// class DivisionNameData {
//   int? dId;
//   String? dName;
//   int? schoolId;
//   int? mdid;
//   String? status;
//
//   DivisionNameData(
//       {this.dId, this.dName, this.schoolId, this.mdid, this.status});
//
//   DivisionNameData.fromJson(dynamic json) {
//     dId = json['dId'];
//     dName = json['dName'];
//     schoolId = json['school_id'];
//     mdid = json['mdid'];
//     status = json['status'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['dId'] = dId;
//     map['dName'] = dName;
//     map['school_id'] = schoolId;
//     map['mdid'] = mdid;
//     map['status'] = status;
//     return map;
//   }
// }
//
// class StandardNameData {
//   int? sId;
//   String? sName;
//   int? schoolId;
//   int? msid;
//   String? status;
//   int? studentsCount;
//
//   StandardNameData(
//       {this.sId,
//       this.sName,
//       this.schoolId,
//       this.msid,
//       this.status,
//       this.studentsCount});
//
//   StandardNameData.fromJson(dynamic json) {
//     sId = json['sId'];
//     sName = json['sName'];
//     schoolId = json['school_id'];
//     msid = json['msid'];
//     status = json['status'];
//     studentsCount = json['students_count'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['sId'] = sId;
//     map['sName'] = sName;
//     map['school_id'] = schoolId;
//     map['msid'] = msid;
//     map['status'] = status;
//     map['students_count'] = studentsCount;
//     return map;
//   }
// }
