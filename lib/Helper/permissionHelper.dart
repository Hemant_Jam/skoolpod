import 'package:permission_handler/permission_handler.dart';

class PermissionHelper{
  static Future<void> takePermission() async {
    if (! await Permission.storage.isGranted) {
      await Permission.storage.request();
print("permission granted");
    } else if (await Permission.storage.request().isPermanentlyDenied) {
    await openAppSettings();
    }
    else{
      print("permission not granted");
    }
  }
}