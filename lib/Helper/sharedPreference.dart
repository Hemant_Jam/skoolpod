import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  static setItem(String key, dynamic value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(value is String)
    prefs.setString(key, value);
    if(value is bool)
      prefs.setBool(key, value);
  }
  static Future<dynamic>? getItem(String key, Type type) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    //Return String
    if(type == String)
    return preferences.getString(key) ;
    if(type == bool)
      return preferences.getBool(key);
  }
  static Future<bool> checkString(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    //Return String
    return preferences.containsKey(key);
  }

  static Future<void> clear() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    /*prefs.remove("token");
    prefs.remove("email");
    prefs.remove("firstName");*/
    prefs.clear();
  }
}