import 'package:intl/intl.dart';
import 'package:skoolpod/Resources/strings.dart';

class Utils{

  static String formateDate(String dateTime,String formatFrom, String formatTo){
    String formatDate="";
    formatDate = DateFormat(formatTo)
        .format(DateFormat(formatFrom).parse(dateTime));
    return formatDate;
  }
  static String getSimpleDateFormatOfToday() {
    final df = new DateFormat(Strings.FORMAT_SIMPLE);
    return df.format(DateTime.now());
  }
}