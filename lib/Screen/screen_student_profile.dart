import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Notifier/studentProfileNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Widgets/network_image_holder.dart';
import 'package:skoolpod/Widgets/ui_component.dart';
import 'package:skoolpod/Widgets/underLineTextFormFieldWithTitle.dart';

class ScreenStudentProfile extends StatefulWidget {
  static const String routeName = "studentProfile";
  ScreenStudentProfile({Key? key ,required this.argument}) : super(key: key);
  final Map<String,dynamic> argument;
  _ScreenProfile createState() => _ScreenProfile();
}

class _ScreenProfile extends State<ScreenStudentProfile> {
  final GlobalKey<FormState> _formKey=GlobalKey<FormState>();
  DateTime? selectedDate;

  int studentId = 0;
  bool childProfile = false;
  bool isTextFormFieldEnabled = false;
  int userId = 0;
  XFile? imageURI;
  @override
  void initState() {
    super.initState();
    studentId=widget.argument["studentId"];
    childProfile = widget.argument['childProfile'];

    checkPermission();
  }


  Widget build(BuildContext context) {
    int userId = Provider.of<SessionNotifier>(context).userData!.id!;
    this.userId = userId;

    return SafeArea(
      child: Scaffold(
        backgroundColor: PrimaryColor,
        appBar: AppBar(
          elevation: 0,
          iconTheme: IconThemeData(
            color: White, //change your color here
          ),
          leading: IconButton(
            onPressed: ()=>Navigator.of(context).pop(),iconSize: 13.0.sp,
            icon: Icon(Icons.arrow_back_ios_new_sharp),color: White,),
        ),
        body: ChangeNotifierProvider(
          create: (_)=> StudentProfileNotifier(context,studentId),
          child: SingleChildScrollView(
            child: Consumer<StudentProfileNotifier>(
                builder: (context, studentProfileNotifier,_) {

               return Form(key: _formKey,
                 child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          /** Student Profile */
                          const SizedBox(height: 5),
                          Stack(
                            children: <Widget>[
                              CircleAvatar(
                                minRadius: 60,
                                maxRadius: 65,
                                backgroundImage:studentProfileNotifier.modelStudentProfile?.data?.image!=null? Image.network(ApiServices.BASE_IMAGE_URL+studentProfileNotifier.modelStudentProfile!.data!.image!
                                  ,fit: BoxFit.contain,).image:
                                Image.asset("assets/images/ic_student_placeholder.png",).image,
                                backgroundColor: Colors.white,
                              ),
                              if(!childProfile)
                              Positioned.fill(
                                child: Align(alignment: Alignment.topRight,
                                  child: SizedBox(
                                    height: 40,
                                    width: 40,
                                    child: GestureDetector(onTap:(){_showSelectionDialog(context,studentProfileNotifier);}  ,
                                      child: Container(
                                          decoration: BoxDecoration(
                                              color: White,
                                              border: Border.all(color: PrimaryColor),
                                              borderRadius:BorderRadius.all(Radius.circular(40))),
                                          child: Icon(
                                            Icons.camera_alt_outlined,
                                            size: 20,
                                            color: PrimaryColor,
                                          )),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 2.2.h),
                          /** Student Form data */
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Card(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                              child: Column(children: [

                                Container(
                                  width: double.maxFinite,
                                  padding: const EdgeInsets.all(12),
                                  decoration: BoxDecoration(
                                      color: CardTitleBg,
                                      borderRadius: BorderRadius.only(topRight: Radius.circular(10),topLeft: Radius.circular(10))),
                                  child: Row(
                                    children: [
                                      Text(Strings.profile_details,style: TextStyle(fontSize: 15.0.sp)),
                                      const Spacer(),
                                      if(!childProfile)
                                      InkWell(onTap: (){
                                        if(isTextFormFieldEnabled){
                                          studentProfileNotifier.updateStudentProfile(context, _formKey, userId, studentId).then((response){
                                            if(response!=null && response.status==1){
                                              Ui.showSuccessToastMessage(Strings.student_profile_update_msg);
                                              studentProfileNotifier.getProfile(context, studentId);
                                            }
                                            else if(response!=null && response.status==0){
                                              Ui.showSuccessToastMessage(response.msg!);
                                            }
                                          });
                                        }
                                        setState(() {
                                        isTextFormFieldEnabled = !isTextFormFieldEnabled;
                                        });
                                      },
                                       child: isTextFormFieldEnabled?
                                       const ImageIcon(AssetImage("assets/images/ic_success.png")
                                         ,size: 22,color: PrimaryColor,):
                                       const ImageIcon(AssetImage("assets/images/ic_edit.png",)
                                         ,size: 22)
                                      ),
                                      const SizedBox(width: 10)
                                    ],
                                  ),
                                ),

                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 13,vertical: 10),
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 10),
                                      ///* First name  */
                                      Row(crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(height: 40,
                                              margin: const EdgeInsets.only(right: 10),
                                              child: Image.asset("assets/images/ic_user.png")),
                                          Expanded(
                                            child: TextFormFieldWithTitle(
                                              title: Strings.first_name,
                                              keyboardType: TextInputType.name,
                                              enabled: isTextFormFieldEnabled,
                                              inputFormatters: [ FilteringTextInputFormatter.allow(RegExp("[a-zA-Z ]")), ],
                                              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey[300]!)),
                                              textController: studentProfileNotifier.firstNameController,
                                              validation: studentProfileNotifier.validateFirstName,),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 15),
                                      ///*  Last name  */
                                      TextFormFieldWithTitle(
                                        title: "            "+Strings.last_name,
                                        keyboardType: TextInputType.name,
                                        enabled: isTextFormFieldEnabled,
                                        inputFormatters: [ FilteringTextInputFormatter.allow(RegExp("[a-zA-Z ]")), ],
                                        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey[300]!)),
                                        contentPadding: const EdgeInsets.only(top: 10,bottom: 12,left: 52),
                                        textController: studentProfileNotifier.lastNameController,
                                        validation: studentProfileNotifier.validateFirstName),

                                      const SizedBox(height: 30,),
                                      ///* Standard - division  */
                                      Row(crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(height: 40,
                                              margin: const EdgeInsets.only(right: 10),
                                              child: Image.asset("assets/images/ic_division.png")),
                                          Expanded(
                                            child: TextFormFieldWithTitle(
                                              title: Strings.standard_division,
                                              enabled: false,
                                              textController: studentProfileNotifier.standardDivisionController,
                                              validation: studentProfileNotifier.validateStandardDivision),
                                          ),
                                        ],
                                      ),
                                      Divider(color: Colors.grey,),

                                      const SizedBox(height: 20),
                                      ///* BirthDay  */
                                      Row(crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(height: 40,
                                              margin: const EdgeInsets.only(right: 10),
                                              child: Image.asset("assets/images/ic_birthday.png")),
                                          Expanded(
                                            child: GestureDetector(
                                              behavior: HitTestBehavior.translucent,
                                              onTap: (){
                                                if(isTextFormFieldEnabled)
                                                pickDate(studentProfileNotifier);
                                              },
                                              child: IgnorePointer(
                                                child: TextFormFieldWithTitle(
                                                  title: Strings.birthday,
                                                  textController: studentProfileNotifier.birthdayController,
                                                  /*validation: profileNotifier.validateStandardDivision,*/),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Divider(color: Colors.grey),

                                      const SizedBox(height: 20,),
                                      ///* Gender  */
                                      Row(crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(height: 40,
                                              margin: const EdgeInsets.only(right: 10),
                                              child: Image.asset("assets/images/ic_gender.png")),
                                          Expanded(
                                            child: GestureDetector(
                                                behavior: HitTestBehavior.translucent,
                                              onTap: (){
                                                  if(isTextFormFieldEnabled)
                                                    _showDialogForGender(context,studentProfileNotifier);
                                              },
                                              child: IgnorePointer(
                                                child: TextFormFieldWithTitle(
                                                  title: Strings.gender,
                                                  textController: studentProfileNotifier.genderController,
                                                  validation: studentProfileNotifier.validateGender,),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Divider(color: Colors.grey),
                                    ],
                                  ),
                                ),

                              ],),
                            ),
                          ),

                          ///*  Parent data */
                          if(childProfile)
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Card(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                              child: Column(children: [

                                Container(
                                  width: double.maxFinite,
                                  padding: EdgeInsets.all(12),
                                  decoration: BoxDecoration(
                                      color: CardTitleBg,
                                      borderRadius: BorderRadius.only(topRight: Radius.circular(10),topLeft: Radius.circular(10))),
                                  child: Text(Strings.parent_profile_details,style: TextStyle(fontSize: 15.0.sp)),
                                ),

                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 10,),
                                      ///* First name  */
                                      Row(crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(height: 40,
                                              margin: const EdgeInsets.only(right: 10),
                                              child: Image.asset("assets/images/ic_user.png")),
                                          Expanded(
                                            child: Stack(
                                              children: [
                                                TextFormFieldWithTitle(
                                                  title: Strings.first_name,
                                                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey[300]!)),
                                                  textController: studentProfileNotifier.parentFirstNameController,
                                                  validation: studentProfileNotifier.validateFirstName,),
                                                Align(alignment: Alignment.centerRight,
                                                  child: Container(height: 50,
                                                      child: NetworkImageHolder(imageUrl: studentProfileNotifier.modelStudentProfile?.data?.parent?.thumb,placeHolder: "assets/images/ic_user.png",)),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 12),
                                      ///*  Last name  */
                                      TextFormFieldWithTitle(
                                        title: "            "+Strings.last_name,
                                        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey[300]!)),
                                        contentPadding: const EdgeInsets.only(top: 10,bottom: 10,left: 52),
                                        textController: studentProfileNotifier.parentLastNameController,
                                        validation: studentProfileNotifier.validateFirstName,),

                                      const SizedBox(height: 30,),
                                      ///* Email  */
                                      Row(crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(height: 40,
                                              margin: const EdgeInsets.only(right: 10),
                                              child: Image.asset("assets/images/ic_mail.png")),
                                          Expanded(
                                            child: TextFormFieldWithTitle(
                                              title: Strings.email,
                                              textController: studentProfileNotifier.parentEmailController,
                                              /*validation: profileNotifier.validateStandardDivision,*/),
                                          ),
                                        ],
                                      ),
                                      Divider(color: Colors.grey,),

                                      const SizedBox(height: 20,),
                                      ///* Phone number  */
                                      Row(crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(height: 40,
                                              margin: const EdgeInsets.only(right: 10),
                                              child: Image.asset("assets/images/ic_mobile.png")),
                                          Expanded(
                                            child: TextFormFieldWithTitle(
                                              title: Strings.phone_number,
                                              textController: studentProfileNotifier.phoneController,
                                              /*validation: profileNotifier.validateStandardDivision,*/),
                                          ),
                                        ],
                                      ),
                                      Divider(color: Colors.grey),
                                    ],
                                  ),
                                ),



                                /*InkWell(onTap:()=> _showDialogForGender(context),
                                  child: IgnorePointer(
                                    child: TextFormField(controller: _dateOfBirthController,
                                      style: editTextStyle,
                                      decoration: InputDecoration(labelText: "Date of Birth",labelStyle: localStyle),),
                                  ),
                                ),
                                SizedBox(height: 10),
                                TextFormField(*//*controller: profileNotifier.cityController*//*
                                  style: editTextStyle,
                                  decoration: InputDecoration(labelText: "City",labelStyle: localStyle),),
                                SizedBox(height: 10),
                                TextFormField(controller: _sharableNameController,
                                  style: editTextStyle,
                                  decoration: InputDecoration(labelText: "Sharable Profile Name",labelStyle: localStyle),),*/

                                /*Row( children: <Widget>[
                                  Spacer(),
                                  FlatButton(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(25.0),),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 30),
                                        child: Text("Update",style: TextStyle(fontSize: 15, color: Colors.white),
                                        ),
                                      ),
                                      color: PrimaryColor,
                                      onPressed: () {
//                              Navigator.of(context).pushNamed("/changePassword");
                                        _validateInputs();
                                        if(_autoValidate){
                                          //var temp = DateFormat("dd-MM-yyyy").parse(_dateOfBirthController.text.trim());
                                          Map<String, dynamic> param = {
                                            "first_name": profileNotifier.firstNameController.text.trim(),
                                            "last_name": profileNotifier.lastNameController.text.trim(),
                                            "gender": _genderValue!=null?_genderValue.toString().toLowerCase() : item.gender,
                                            "city": profileNotifier.cityController.text.toString(),
                                            "dob": selectedDate!=null?selectedDate.toIso8601String():item.dob// DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(selectedDate),
                                          };

                                          profileNotifier.updateProfile(context, param);

                                        }
                                      })
                                ]),*/
                              ],),
                            ),
                          )
                        ],
                      ),
               );
                  /*}
                  else
                    return Container();*/
                }
            ),
          ),
        ),
      ),
    );
  }
  Future<void> _showSelectionDialog(BuildContext context,StudentProfileNotifier studentProfileNotifier) {
    return showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 20.0.h,width: double.maxFinite,
          padding: EdgeInsets.only(top: 3.0.h),
          child: Center(
            child: Row(mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: (){
                    getImageFromMobile("camera",studentProfileNotifier);
                    Navigator.of(context).pop();
                  },
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.camera_alt,color: PrimaryColor,size: 35.0.sp,),
                      ),
                      const Text('Capture Image',style: TextStyle(color: PrimaryColor),),
                    ],
                  ),
                ),
                SizedBox(width: 8.0.w,),
                Container(width: 1,height: 9.0.h,color: Colors.grey,),
                SizedBox(width: 8.0.w,),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: (){
                    getImageFromMobile("gallery",studentProfileNotifier);
                    Navigator.of(context).pop();
                  },
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.image,color: PrimaryColor,size: 35.0.sp,),
                      ),
                      const Text('Choose Image',style: TextStyle(color: PrimaryColor),),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
  void _showDialogForGender(BuildContext context, StudentProfileNotifier studentProfileNotifier) async {
    List<String> _genderList = ["Male","Female"];
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Select Gender"),
            content: SingleChildScrollView(
              child: ListBody(
                children: _genderList.map((e) {
                  return InkWell(
                    onTap: (){
                      studentProfileNotifier.genderController.text = e;
                      Navigator.of(context).pop();
                    },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text(e,style: TextStyle(fontSize: 12.0.sp),),
                      ));
                }).toList()
              ),
            ),
          );
        });
  }
  pickDate(StudentProfileNotifier studentProfileNotifier) {
    DateTime temp=DateTime.now();
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        contentPadding: EdgeInsets.all(4),
        actions: [
          TextButton(onPressed: ()=>Navigator.of(context).pop(), child: Text('CANCEL')),
          TextButton(onPressed: () {
            final df = new DateFormat(Strings.FORMAT_BIRTHDAY);
            studentProfileNotifier.birthdayController.text =df.format(temp);
            Navigator.of(context).pop();
          }, child: Text('OK')),
        ],
        content: SizedBox(
          height: MediaQuery.of(context).size.height / 4,
          child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            initialDateTime: DateTime.now(),
            onDateTimeChanged: (DateTime newDateTime) {
              temp = newDateTime;
            },
          ),
        ),
      ),
    );
  }


  Future getImageFromMobile(String source,StudentProfileNotifier studentProfileNotifier) async {
    var image;
    if(source=="gallery")
     image= await ImagePicker().pickImage(source: ImageSource.gallery);
    else
      image= await ImagePicker().pickImage(source: ImageSource.camera);
    if (image != null) {
      var response = await studentProfileNotifier.uploadImageOfChild(userId, File(image.path), context);
      if(response!=null && response.status==1) {
        Ui.showSuccessToastMessage(response.msg!);
        studentProfileNotifier.getProfile(context, studentId);
      }
      if(response!=null && response.status==0) {
        Ui.showToastMessage(response.msg!);
      }
    }
  }


  void checkPermission() async {
    var status = await Permission.storage.status;

    if(status.isGranted)
      return;
    if( status.isDenied)
    {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.storage,
        Permission.camera
      ].request();
    }
  }

}