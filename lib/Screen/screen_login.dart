import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Helper/permissionHelper.dart';
import 'package:skoolpod/Notifier/authNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Screen/Admin/screen_admin_home.dart';
import 'package:skoolpod/Screen/Parent/screen_parent_home.dart';
import 'package:skoolpod/Screen/Teacher/screen_teacher_home.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/square_corner_button.dart';
import 'package:skoolpod/Widgets/underLineEditTextFormField.dart';

class ScreenLogin extends StatefulWidget {
  const ScreenLogin({Key? key}) : super(key: key);
  static const String routeName = "login";

  @override
  _ScreenLoginState createState() => _ScreenLoginState();
}

class _ScreenLoginState extends State<ScreenLogin> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    PermissionHelper.takePermission();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // _emailController.text="tehmt@mailinator.com";
    //_emailController.text="hmt@mailinator.com";
    // _emailController.text="pahmt@mailinator.com";
    //_passwordController.text="123456789";
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: ChangeNotifierProvider(
          create: (BuildContext context) => AuthNotifier(),
          child: Consumer<AuthNotifier>(builder: (context, authNotifier, _) {
            return Form(
              key: _formKey,
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        ImageIcon(
                          Image.asset("assets/images/logo.png").image,
                          color: PrimaryColor,
                          size: 70,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          Strings.welcome_back,
                          style: TextStyle(
                              fontSize: 18.0.sp,
                              fontWeight: FontWeight.bold,
                              color: PrimaryColor),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          Strings.enter_email,
                          style: TextStyle(fontSize: 11.0.sp),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        UnderLineEditTextFormField(
                            textController: _emailController,
                            validation: authNotifier.validateEmail,
                            labelText: Strings.email_address),
                        SizedBox(
                          height: 20,
                        ),
                        UnderLineEditTextFormField(
                          textController: _passwordController,
                          validation: authNotifier.validatePassword,
                          errorMaxLines: 2,
                          labelText: Strings.password,
                          obscureText: true,
                          maxLines: 1,
                        ),
                      ],
                    ),
                  ),
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: SquareButton(
                        onPressed: () => authNotifier
                            .makeLogin(context, _formKey, _emailController,
                                _passwordController)
                            .then((userData) {
                          if (userData != null) {
                            Provider.of<SessionNotifier>(context, listen: false)
                                .setPreferenceData(userData);
                            String routeName = ScreenTeacherHome.routeName;
                            if (userData.data!.role == Strings.TEACHER)
                              routeName = ScreenTeacherHome.routeName;
                            else if (userData.data!.role == Strings.PARENT)
                              routeName = ScreenParentHome.routeName;
                            else if (userData.data!.role == Strings.PRINCIPAL)
                              routeName = ScreenAdminHome.routeName;

                            Navigator.of(context).popAndPushNamed(routeName);
                          }
                        }),
                        text: Strings.login,
                      )
                      /*Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              onPressed:() => authNotifier.makeLogin(context, _formKey, _emailController, _passwordController).then((userData){
                                if(userData!=null){
                                  Provider.of<SessionNotifier>(context,listen: false).setPreferenceData(userData);
                                Navigator.of(context).popAndPushNamed(ScreenTeacherHome.routeName);
                                }
                              }) ,
                              child:  Text(Strings.login, style: TextStyle(fontSize: 13.0.sp,color: Colors.white)),
                              style: ElevatedButton.styleFrom(primary: PrimaryColor,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0))),
                            ),
                          ),
                        ],
                      ),*/
                      )
                ],
              ),
            );
          }),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
