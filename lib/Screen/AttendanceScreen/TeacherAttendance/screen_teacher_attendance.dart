import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Model/model_class_detail_for_admin.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Notifier/teacherAttendanceNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Screen/AttendanceScreen/screen_attendance.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:flutter/rendering.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:sizer/sizer.dart';

class TeacherAttendancePage extends StatefulWidget {
  final AttendanceArguments arguments;
  final ModelClassDetailForAdmin modelClassDetailForAdmin =
      ModelClassDetailForAdmin();
  final TeacherAttendanceNotifier teacherAttendanceNotifier =
      TeacherAttendanceNotifier();
  static const String routeName = "attendancePage";
  TeacherAttendancePage({Key? key, required this.arguments}) : super(key: key);

  @override
  _TeacherAttendancePageState createState() => _TeacherAttendancePageState();
}

class _TeacherAttendancePageState extends State<TeacherAttendancePage> {
  @override
  void initState() {
    widget.teacherAttendanceNotifier.getCurrentMonth();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;
    widget.teacherAttendanceNotifier.webCallGetTeacherAttendance(
      context: context,
      divisionId: widget.arguments.divisionId,
      lectureId: widget.arguments.scheduleId,
      standardId: widget.arguments.standardId,
      userId: int.parse(userData.id.toString()),
    );
    return ChangeNotifierProvider(
      create: (_) => widget.teacherAttendanceNotifier,
      child: Consumer<TeacherAttendanceNotifier>(
        builder: (context, snapshot, child) {
          if (snapshot.modelClassDetailForAdmin != null) {
            return SafeArea(
              child: Scaffold(
                appBar: GlobalAppBar(title: widget.arguments.className),
                body: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: PrimaryColor, width: 2)),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Row(
                            children: [
                              Container(
                                  color: PrimaryColor,
                                  child: IconButton(
                                    icon: Icon(Icons.arrow_back_ios,
                                        color: White),
                                    onPressed: () {
                                      snapshot.monthCount--;
                                      snapshot.gotoPreviousMonth(
                                        month: snapshot.monthCount,
                                        context: context,
                                        divisionId: widget.arguments.divisionId,
                                        lectureId: widget.arguments.scheduleId,
                                        standardId: widget.arguments.standardId,
                                        userId:
                                            int.parse(userData.id.toString()),
                                      );
                                    },
                                  )),
                              Expanded(
                                child: InkWell(
                                  onTap: () {},
                                  child: Center(
                                    child: Text(
                                        "${snapshot.monthList[snapshot.titleDate.month - 1]}-${snapshot.titleDate.year}",
                                        style: TextStyle(color: PrimaryColor)),
                                  ),
                                ),
                              ),
                              Container(
                                color: PrimaryColor,
                                child: IconButton(
                                  icon: Icon(Icons.arrow_forward_ios,
                                      color: White),
                                  onPressed: () {
                                    snapshot.monthCount++;
                                    snapshot.gotoNextMonth(
                                      month: snapshot.monthCount,
                                      context: context,
                                      divisionId: widget.arguments.divisionId,
                                      lectureId: widget.arguments.scheduleId,
                                      standardId: widget.arguments.standardId,
                                      userId: int.parse(userData.id.toString()),
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      snapshot.modelClassDetailForAdmin!.data != null
                          ? Items(
                              teacherData: snapshot
                                  .modelClassDetailForAdmin!.data!.teacher,
                            )
                          : Container(
                              child: Center(
                                child: Container(
                                  child: CircularProgressIndicator(
                                    color: Colors.transparent,
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Scaffold(
              appBar: GlobalAppBar(
                title: widget.arguments.className,
              ),
              body: Container(
                child: Center(
                  child: CircularProgressIndicator(color: Colors.transparent),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}

class Items extends StatelessWidget {
  final List<Teacher>? teacherData;
  Items({Key? key, required this.teacherData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: teacherData!.length,
        itemBuilder: (context, int index) {
          return Card(
            child: ExpansionTile(
              tilePadding: EdgeInsets.all(18),
              leading: Container(
                width: 62,
                height: 62,
                child: teacherData![index].thumb!.isNotEmpty
                    ? CachedNetworkImage(
                        imageUrl: ApiServices.BASE_IMAGE_URL +
                            teacherData![index].thumb.toString(),
                        imageBuilder: (context, imageProvider) => Container(
                          width: 62.0,
                          height: 65.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.cover),
                          ),
                        ),
                      )
                    : Image.asset(
                        "assets/images/ic_student_placeholder.png",
                      ),
              ),
              title: Text(
                  "${teacherData![index].firstname}${teacherData![index].lastname}"),
              children: [
                ExpansionChildren(teacherData: teacherData![index].attendance!),
              ],
            ),
          );
        },
      ),
    );
  }
}

class ExpansionChildren extends StatelessWidget {
  final List<AttendanceTeacher> teacherData;
  ExpansionChildren({Key? key, required this.teacherData}) : super(key: key);
  final TeacherAttendanceNotifier teacherAttendanceNotifier =
      TeacherAttendanceNotifier();
  Widget customText(String text) {
    return Text(text,
        textScaleFactor: 1.2, style: TextStyle(fontWeight: FontWeight.w600));
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: teacherData.length,
      itemBuilder: (context, int index) {
        String checkIn = '';
        String checkOut = '';
        if (teacherData[index].ins!.isNotEmpty)
          checkIn =
              'Check In : ${DateFormat.jm().format(DateFormat("hh:mm:ss").parse(teacherData[index].ins.toString()))}';
        if (teacherData[index].out!.isNotEmpty)
          checkOut =
              'Check Out : ${DateFormat.jm().format(DateFormat("hh:mm:ss").parse(teacherData[index].out.toString()))}';

        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: IntrinsicHeight(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 22.0.w,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 18, vertical: 10),
                    child: Column(
                      children: [
                        customText(DateFormat('dd')
                            .format(DateTime.parse(
                                teacherData[index].date.toString()))
                            .toString()),
                        customText(teacherAttendanceNotifier.monthList[
                                int.parse(teacherData[index]
                                        .date!
                                        .split("-")
                                        .elementAt(1)) -
                                    1]
                            .toString()),
                        customText((DateFormat('EEEE').format(
                                DateTime.parse(teacherData[index].date!)))
                            .substring(0, 3)),
                      ],
                    ),
                  ),
                ),
                VerticalDivider(color: ColorYellow, thickness: 1, width: 20),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [customText(checkIn), customText(checkOut)],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
