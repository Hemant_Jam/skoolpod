import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Model/model_student_attendance_daily.dart';
import 'package:skoolpod/Notifier/StudentAttendanceDailyNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:sizer/sizer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:flutter/rendering.dart';
import '../screen_attendance.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;

class StudentAttendanceDaily extends StatefulWidget {
  final AttendanceArguments arguments;
  final StudentAttendanceDailyNotifier studentAttendanceDailyNotifier =
      StudentAttendanceDailyNotifier();

  StudentAttendanceDaily({Key? key, required this.arguments}) : super(key: key);

  @override
  _StudentAttendanceDailyState createState() => _StudentAttendanceDailyState();
}

class _StudentAttendanceDailyState extends State<StudentAttendanceDaily>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  Widget customText(String text) {
    return Text(
      text,
      textScaleFactor: 1.2,
      style: TextStyle(
          fontWeight: FontWeight.w600, color: PrimaryColor, fontSize: 11.0.sp),
    );
  }

  @override
  void initState() {
    widget.studentAttendanceDailyNotifier.getCurrentDate();
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;
    widget.studentAttendanceDailyNotifier.webCallGetStudentAttendanceDaily(
      context: context,
      divisionId: widget.arguments.divisionId,
      lectureId: widget.arguments.scheduleId,
      standardId: widget.arguments.standardId,
      userId: int.parse(userData.id.toString()),
    );
    return ChangeNotifierProvider(
      create: (_) => widget.studentAttendanceDailyNotifier,
      child: Consumer<StudentAttendanceDailyNotifier>(
        builder: (context, studentAttendanceDailyNotifier, child) {
          List<Student> presentList = [];
          List<Student> absentList = [];
          if (studentAttendanceDailyNotifier.modelStudentAttendanceDaily !=
              null) {
            studentAttendanceDailyNotifier
                .modelStudentAttendanceDaily!.data!.student!
                .forEach((element) {
              if (element.attendance!.datePresent == "1") {
                presentList.add(element);
              } else {
                absentList.add(element);
              }
            });
          }

          return Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: PrimaryColor, width: 2)),
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Row(
                    children: [
                      Container(
                        color: PrimaryColor,
                        child: IconButton(
                          icon: Icon(Icons.arrow_back_ios, color: White),
                          onPressed: () {
                            studentAttendanceDailyNotifier.day--;
                            studentAttendanceDailyNotifier.previousDate(
                              day: studentAttendanceDailyNotifier.day,
                              context: context,
                              divisionId: widget.arguments.divisionId,
                              lectureId: widget.arguments.scheduleId,
                              standardId: widget.arguments.standardId,
                              userId: int.parse(userData.id.toString()),
                            );
                          },
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            studentAttendanceDailyNotifier.pickDate(
                              context: context,
                              divisionId: widget.arguments.divisionId,
                              lectureId: widget.arguments.scheduleId,
                              standardId: widget.arguments.standardId,
                              userId: int.parse(userData.id.toString()),
                            );
                          },
                          child: Center(
                            child: Text(
                              "${studentAttendanceDailyNotifier.titleDate.day} - ${studentAttendanceDailyNotifier.monthList[studentAttendanceDailyNotifier.titleDate.month - 1]} - ${studentAttendanceDailyNotifier.titleDate.year}",
                              style: TextStyle(
                                color: PrimaryColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        color: PrimaryColor,
                        child: IconButton(
                          icon: Icon(Icons.arrow_forward_ios, color: White),
                          onPressed: () {
                            studentAttendanceDailyNotifier.day++;
                            studentAttendanceDailyNotifier.nextDate(
                              day: studentAttendanceDailyNotifier.day,
                              context: context,
                              divisionId: widget.arguments.divisionId,
                              lectureId: widget.arguments.scheduleId,
                              standardId: widget.arguments.standardId,
                              userId: int.parse(userData.id.toString()),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              studentAttendanceDailyNotifier.modelStudentAttendanceDaily != null
                  ? Expanded(
                      child: Column(
                        children: [
                          TabBar(
                            indicatorColor: PrimaryColor,
                            labelColor: PrimaryColor,
                            unselectedLabelColor: Colors.black,
                            controller: _tabController,
                            tabs: [
                              Tab(child: Text('PRESENT')),
                              Tab(child: Text('ABSENT')),
                            ],
                          ),
                          Expanded(
                            child: TabBarView(
                              controller: _tabController,
                              children: [
                                Attendance(presentList),
                                Attendance(absentList),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(
                      child: Center(
                        child: Container(
                          child: CircularProgressIndicator(
                            color: Colors.transparent,
                          ),
                        ),
                      ),
                    ),
            ],
          );
        },
      ),
    );
  }
}

class Attendance extends StatelessWidget {
  Attendance(this.attendanceList);

  final List<Student> attendanceList;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: attendanceList.length,
      padding: EdgeInsets.all(15),
      physics: BouncingScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 8, bottom: 4),
                  child: Container(
                    height: 62,
                    child: attendanceList[index].thumb!.isNotEmpty
                        ? CachedNetworkImage(
                            imageUrl: ApiServices.BASE_IMAGE_URL +
                                attendanceList[index].thumb.toString(),
                            imageBuilder: (context, imageProvider) => Container(
                              width: 62.0,
                              height: 65.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          )
                        : Image.asset(
                            "assets/images/ic_student_placeholder.png"),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Text(
                    "${attendanceList[index].firstname ?? ""} ${attendanceList[index].lastname ?? ""}",
                    style: TextStyle(
                        fontSize: 11.0.sp,
                        color: PrimaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
