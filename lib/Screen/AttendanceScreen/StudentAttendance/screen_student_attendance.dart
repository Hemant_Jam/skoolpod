import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Model/model_class_detail_for_admin.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Notifier/studentAttendanceNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Screen/AttendanceScreen/StudentAttendance/screen_student_attendance_daily.dart';
import 'package:skoolpod/Screen/AttendanceScreen/StudentAttendance/screen_student_attendance_monthly.dart';
import 'package:skoolpod/Screen/AttendanceScreen/StudentAttendance/screen_student_attendance_quarterly.dart';
import 'package:skoolpod/Screen/AttendanceScreen/StudentAttendance/screen_student_attendance_yearly.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import '../screen_attendance.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;

class StudentAttendancePage extends StatefulWidget {
  final AttendanceArguments arguments;
  final ModelClassDetailForAdmin modelClassDetailForAdmin =
      ModelClassDetailForAdmin();
  final StudentAttendanceNotifier studentAttendanceNotifier =
      StudentAttendanceNotifier();

  StudentAttendancePage({Key? key, required this.arguments}) : super(key: key);

  @override
  _StudentAttendancePageState createState() => _StudentAttendancePageState();
}

class _StudentAttendancePageState extends State<StudentAttendancePage> {
  static const String routeName = "attendancePage";

  String page = 'Daily';
  int index = 0;

  Widget customContainer(
      {required String monthName,
      required String pageName,
      required int indexNumber,
      double radius = 0,
      bool isFirst = true}) {
    return InkWell(
      onTap: () {
        changePage(pageName: pageName);
        setState(() {
          index = indexNumber;
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
        decoration: BoxDecoration(
            borderRadius: isFirst
                ? BorderRadius.only(
                    topLeft: Radius.circular(radius),
                    bottomLeft: Radius.circular(radius),
                  )
                : BorderRadius.only(
                    topRight: Radius.circular(radius),
                    bottomRight: Radius.circular(radius),
                  ),
            color: index == indexNumber ? PrimaryColor : White,
            border: Border.all(color: PrimaryColor, width: 2)),
        child: Center(
          child: Text(
            monthName,
            style: TextStyle(
                color: !(index == indexNumber) ? PrimaryColor : White),
          ),
        ),
      ),
    );
  }

  changePage({required String pageName}) {
    setState(() {
      page = pageName;
    });
  }

  Widget showPage() {
    switch (page) {
      case 'Daily':
        return StudentAttendanceDaily(
          arguments: widget.arguments,
        );
      case 'Monthly':
        return StudentAttendanceMonthly(arguments: widget.arguments);
      case 'Quarterly':
        return StudentAttendanceQuarterly(arguments: widget.arguments);
      case 'Yearly':
        return StudentAttendanceYearly(
          arguments: widget.arguments,
        );
      default:
        return StudentAttendanceDaily(
          arguments: widget.arguments,
        );
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;
    widget.studentAttendanceNotifier.webCallGetStudentAttendance(
      context: context,
      divisionId: widget.arguments.divisionId,
      lectureId: widget.arguments.scheduleId,
      standardId: widget.arguments.standardId,
      userId: int.parse(userData.id.toString()),
    );

    return ChangeNotifierProvider(
      create: (_) => widget.studentAttendanceNotifier,
      builder: (context, snapshot) {
        return Scaffold(
          appBar: GlobalAppBar(
            title: widget.arguments.className,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height / 12,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    children: [
                      customContainer(
                          monthName: 'Daily',
                          indexNumber: 0,
                          pageName: 'Daily',
                          radius: 10,
                          isFirst: true),
                      customContainer(
                          monthName: 'Monthly',
                          indexNumber: 1,
                          pageName: 'Monthly'),
                      customContainer(
                          monthName: 'Quarterly',
                          indexNumber: 2,
                          pageName: 'Quarterly'),
                      customContainer(
                        monthName: 'Yearly',
                        indexNumber: 3,
                        pageName: 'Yearly',
                        radius: 10,
                        isFirst: false,
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(child: showPage())
            ],
          ),
        );
      },
    );
  }
}
