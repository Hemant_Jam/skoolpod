import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Model/model_student_attendance_quarterly.dart';
import 'package:skoolpod/Notifier/StudentAttendanceQuarterlyNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Screen/AttendanceScreen/screen_attendance.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:flutter/rendering.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Widgets/syncFusion_circular_view.dart';

class StudentAttendanceQuarterly extends StatefulWidget {
  final AttendanceArguments arguments;

  final StudentAttendanceQuarterlyNotifier studentAttendanceQuarterlyNotifier =
      StudentAttendanceQuarterlyNotifier();

  StudentAttendanceQuarterly({Key? key, required this.arguments})
      : super(key: key);

  @override
  _StudentAttendanceQuarterlyState createState() =>
      _StudentAttendanceQuarterlyState();
}

class _StudentAttendanceQuarterlyState
    extends State<StudentAttendanceQuarterly> {
  @override
  void initState() {
    widget.studentAttendanceQuarterlyNotifier.getCurrentQuarter();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;
    widget.studentAttendanceQuarterlyNotifier
        .webCallGetStudentAttendanceQuarterly(
      context: context,
      divisionId: widget.arguments.divisionId,
      lectureId: widget.arguments.scheduleId,
      standardId: widget.arguments.standardId,
      userId: int.parse(userData.id.toString()),
    );
    return ChangeNotifierProvider(
      create: (_) => widget.studentAttendanceQuarterlyNotifier,
      child: Consumer<StudentAttendanceQuarterlyNotifier>(
        builder: (context, studentAttendanceQuarterlyNotifier, child) {
          if (studentAttendanceQuarterlyNotifier
                  .modelStudentAttendanceQuarterly !=
              null) {
            return Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: PrimaryColor, width: 2)),
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        Container(
                          color: PrimaryColor,
                          child: IconButton(
                            icon: Icon(Icons.arrow_back_ios, color: White),
                            onPressed: () {
                              studentAttendanceQuarterlyNotifier.monthCount -=
                                  3;
                              studentAttendanceQuarterlyNotifier
                                  .gotoPreviousQuarter(
                                month: studentAttendanceQuarterlyNotifier
                                    .monthCount,
                                context: context,
                                divisionId: widget.arguments.divisionId,
                                lectureId: widget.arguments.scheduleId,
                                standardId: widget.arguments.standardId,
                                userId: int.parse(userData.id.toString()),
                              );
                            },
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: Text(
                                "${DateFormat("MMM").format(studentAttendanceQuarterlyNotifier.titleStartDate)}-${DateFormat("MMM").format(DateTime(studentAttendanceQuarterlyNotifier.titleStartDate.year, studentAttendanceQuarterlyNotifier.titleStartDate.month + 2, studentAttendanceQuarterlyNotifier.titleStartDate.day))}",
                                style: TextStyle(color: PrimaryColor)),
                          ),
                        ),
                        Container(
                          color: PrimaryColor,
                          child: IconButton(
                            icon: Icon(Icons.arrow_forward_ios, color: White),
                            onPressed: () {
                              studentAttendanceQuarterlyNotifier.monthCount +=
                                  3;
                              studentAttendanceQuarterlyNotifier
                                  .gotoNextQuarter(
                                month: studentAttendanceQuarterlyNotifier
                                    .monthCount,
                                context: context,
                                divisionId: widget.arguments.divisionId,
                                lectureId: widget.arguments.scheduleId,
                                standardId: widget.arguments.standardId,
                                userId: int.parse(userData.id.toString()),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                studentAttendanceQuarterlyNotifier
                            .modelStudentAttendanceQuarterly!.data !=
                        null
                    ? Items(
                        studentData: studentAttendanceQuarterlyNotifier
                            .modelStudentAttendanceQuarterly!.data!.student)
                    : Container(
                        child: Center(
                          child: Container(
                            child: CircularProgressIndicator(
                              color: Colors.transparent,
                            ),
                          ),
                        ),
                      ),
              ],
            );
          } else {
            return Scaffold(
              body: Container(
                child: Center(
                  child: CircularProgressIndicator(color: Colors.transparent),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}

class Items extends StatelessWidget {
  final List<Student>? studentData;
  Items({Key? key, required this.studentData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: studentData!.length,
        itemBuilder: (context, int index) {
          int present =
              int.parse(studentData![index].attendance!.present.toString());
          int totalAttendance = ((studentData![index].attendance!.present!) +
              (int.parse(studentData![index].attendance!.absent.toString())));
          double percentage = (present * 100) / totalAttendance;

          return Card(
            child: ListTile(
              leading: Container(
                width: 62,
                height: 62,
                child: studentData![index].thumb!.isNotEmpty
                    ? CachedNetworkImage(
                        imageUrl: ApiServices.BASE_IMAGE_URL +
                            studentData![index].thumb.toString(),
                        imageBuilder: (context, imageProvider) => Container(
                          width: 62.0,
                          height: 65.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.cover),
                          ),
                        ),
                      )
                    : Image.asset("assets/images/ic_student_placeholder.png"),
              ),
              title: Text(
                  "${studentData![index].firstname}${studentData![index].lastname}"),
              subtitle: Text("Total Attendance $present/$totalAttendance"),
              trailing: Container(
                height: 50,
                width: 50,
                child: SyncfusionCircularView(
                    value: percentage.toString() == "NaN"
                        ? 0.0
                        : percentage.floor().toDouble()),
              ),
            ),
          );
        },
      ),
    );
  }
}
