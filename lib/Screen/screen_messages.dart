import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Helper/utils.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Model/model_messages.dart';
import 'package:skoolpod/Notifier/messageNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Screen/Admin/screen_push_message.dart';
import 'package:skoolpod/Screen/Teacher/screen_add_assignment.dart';
import 'package:skoolpod/Screen/screen_chat.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:skoolpod/Widgets/speed_dial_floating_button.dart';

class ScreenMessages extends StatelessWidget {
  ScreenMessages({Key? key}) : super(key: key);
  static const String routeName = "teacherMessages";
  int lastConvoId = -1;
  Results? result;
  ValueNotifier<bool> isDialOpen = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;

    void onTap(String navigateTo){
      String routeName =ScreenAddAssignment.routeName;
      if(navigateTo=="ASSIGNMENT")
        routeName = ScreenAddAssignment.routeName;
      else
        routeName = ScreenPushMessage.routeName;

      Navigator.of(context).pushNamed(routeName,arguments: result).then((value) {
        Provider.of<MessageNotifier>(context,listen: false).getMessages(userData.id.toString(), context);
      });
    }

    return SafeArea(
        child: ChangeNotifierProvider(
          create: (_)=> MessageNotifier(userData.id.toString(),context),
          child: Scaffold(
            appBar: GlobalAppBar(title: Strings.message,),
              floatingActionButton: userData.role == Strings.TEACHER ?
              FloatingActionButton(
                 isExtended: true,
                child: Icon(Icons.add),
                backgroundColor: PrimaryColor,
                onPressed: () {
                  Navigator.of(context).pushNamed(ScreenAddAssignment.routeName,arguments: result).then((value) {
                    Provider.of<MessageNotifier>(context,listen: false).getMessages(userData.id.toString(), context);
                  });
                },
              ):
              SpeedDialFloatingButton().getFloatingButton(isDialOpen,onTap),

              body: Consumer<MessageNotifier>(
                builder: (context, messageNotifier,_) {
                  if(messageNotifier.modelMessages!=null)
                  result = messageNotifier.modelMessages!.results!;
                  return Container(
                    child: messageNotifier.modelMessages!=null && messageNotifier.modelMessages!.results!.data!.isNotEmpty ?
                    ListView.builder
                      (
                        itemCount: messageNotifier.modelMessages!.results!.data!.length,
                        padding: EdgeInsets.all(10),
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return MessageItem(messageNotifier.modelMessages!.results!.data![index],userData.id.toString(),this);
                        }
                    ):
                    Container(child: Center(child: Text("No Messages found"))),
                  );
                }
              )),
        ));
  }
}
class MessageItem extends StatelessWidget {

  MessageItem(this.conversationItem, this.userId, this.screenMessages);
  final Data conversationItem;
  final ScreenMessages screenMessages;
  final String userId;

  @override
  Widget build(BuildContext context) {
    String lastMsg='',dateTime= "";
    if(conversationItem.lastMessage!=null){
      if(conversationItem.lastMessage!.message!.type==Strings.MESSAGE_TEXT)
        lastMsg= conversationItem.lastMessage?.message?.body??"";
      else
        lastMsg= conversationItem.lastMessage!.message!.type!;
      dateTime = Utils.formateDate(conversationItem.lastMessage!.updatedAt!,Strings.FORMAT_SERVER,"MMM dd, yyyy hh:mm a");
    }
    /**Message Item*/
    return
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: InkWell(
          onTap: (){
            screenMessages.lastConvoId = conversationItem.id!;
            Navigator.of(context).pushNamed(ScreenChat.routeName,arguments: conversationItem).then((value){
              Provider.of<MessageNotifier>(context,listen: false).getMessages(userId, context);
            });
          },
          child: Column(
            children: [
              Container(
                child: Row(mainAxisSize: MainAxisSize.max,
                  children: [
                    Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 4,bottom: 4),
                          child: conversationItem.image!.isNotEmpty ?
                          CachedNetworkImage(
                            imageUrl: conversationItem.image!,
                            imageBuilder: (context, imageProvider) => Container(
                              width: 68.0,
                              height: 68.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                            //placeholder: (context, url) => Image.asset("assets/images/ic_student_placeholder.png"),
                            errorWidget: (context, url, error) => Container(height: 68,
                                child: Image.asset("assets/images/ic_student_placeholder.png",)),
                          ):
                          Container(height: 68,child: Image.asset("assets/images/ic_student_placeholder.png",)),
                        ),
                        if (conversationItem.unreadMessagesCount! > 0)
                        Visibility(visible: screenMessages.lastConvoId == -1 || screenMessages.lastConvoId != conversationItem.id,
                          child: Positioned.fill(
                            child: Align(alignment: Alignment.bottomRight,
                              child: Icon(Icons.brightness_1,
                                  size: 18.5, color: PrimaryColor),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: 10,),
                    Expanded(flex: 1,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Text(conversationItem.name??"",style: TextStyle(fontSize: 14.0.sp,color: PrimaryColor,fontFamily: Strings.BALOO),),
                          Text(dateTime,style: TextStyle(fontSize: 12.0.sp,color: Colors.grey[600],fontFamily: Strings.BALOO),),

                          Text(lastMsg,style: TextStyle(fontSize: 12.0.sp, color: Colors.grey,fontFamily: Strings.VARELA,),overflow: TextOverflow.ellipsis,maxLines: 1,),
                        ],
                      ),
                    ),
                    if (conversationItem.unreadMessagesCount! > 0)
                    Visibility(visible: screenMessages.lastConvoId == -1 || screenMessages.lastConvoId != conversationItem.id,
                      child: Stack(
                        children: <Widget>[
                          Icon(Icons.brightness_1,size: 21, color: PrimaryColor),
                          Positioned.fill(
                            child:  Align(alignment: Alignment.center,
                                child:  Text(
                                  conversationItem.unreadMessagesCount!.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 10.5,
                                      fontWeight: FontWeight.w500),
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Divider(color: Colors.grey,)
            ],
          ),
        ),
      );
  }
}