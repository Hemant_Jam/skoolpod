import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Notifier/teacherLeaveListNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;

class TeacherLeaveList extends StatefulWidget {
  TeacherLeaveList({Key? key}) : super(key: key);
  final TeacherLeaveListNotifier teacherLeaveListNotifier =
      TeacherLeaveListNotifier();
  static const String routeName = "teacherLeaveList";
  @override
  _TeacherLeaveListState createState() => _TeacherLeaveListState();
}

class _TeacherLeaveListState extends State<TeacherLeaveList> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  build(BuildContext context) {
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    widget.teacherLeaveListNotifier
        .teacherLeaveList(context: context, userId: userData.id!);
    return Scaffold(
      key: _scaffoldKey,
      appBar: GlobalAppBar(
        title: Strings.leaves,
      ),
      body: ChangeNotifierProvider(
        create: (_) => widget.teacherLeaveListNotifier,
        child: Consumer<TeacherLeaveListNotifier>(
          builder: (context, teacherLeaveList, child) {
            if (teacherLeaveList.modelTeacherLeaveList.data != null) {
              return SingleChildScrollView(
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: 15,
                  itemBuilder: (context, int index) {
                    Color statusColor = Colors.black;
                    switch (teacherLeaveList
                        .modelTeacherLeaveList.data![index].status
                        .toString()) {
                      case "Pending":
                        statusColor = ColorYellow;
                        break;
                      case "Canceled":
                        statusColor = ColorUsername;
                        break;
                      case "Approved":
                        statusColor = Green;
                        break;
                      case "Rejected":
                        statusColor = Red;
                        break;
                      default:
                        statusColor = Colors.black;
                    }
                    return Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 4),
                                        child: Text(
                                          "Date : ",
                                          style: TextStyle(color: PrimaryColor),
                                        ),
                                      ),
                                      Text(
                                        'From ${DateFormat("MMM-dd-yyyy").format(DateTime.parse(teacherLeaveList.modelTeacherLeaveList.data![index].start.toString()))} To ${DateFormat("MMM-dd-yyyy").format(
                                          DateTime.parse(
                                            teacherLeaveList
                                                .modelTeacherLeaveList
                                                .data![index]
                                                .end
                                                .toString(),
                                          ),
                                        )}',
                                        style: TextStyle(fontSize: 11.sp),
                                      )
                                    ],
                                  ),
                                  Text(
                                    "Reason:",
                                    style: TextStyle(color: PrimaryColor),
                                  ),
                                  Text(
                                      "${teacherLeaveList.modelTeacherLeaveList.data![index].reason}"),
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text(
                                          "Status:",
                                          style: TextStyle(color: PrimaryColor),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 4),
                                          child: Text(
                                            "${teacherLeaveList.modelTeacherLeaveList.data![index].status}",
                                            style:
                                                TextStyle(color: statusColor),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                width: 1.5,
                                height: 60,
                                color: statusColor,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "${teacherLeaveList.modelTeacherLeaveList.data![index].days!.floor()}",
                                    textScaleFactor: 1.5,
                                  ),
                                  Text(
                                    teacherLeaveList.modelTeacherLeaveList
                                                .data![index].days! >
                                            1
                                        ? "Days"
                                        : "Day",
                                    style: TextStyle(color: PrimaryColor),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          teacherLeaveList
                                      .modelTeacherLeaveList.data![index].status
                                      .toString() ==
                                  "Pending"
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () => showConfirmationDialogue(
                                          //   context: context,
                                          statusOfLeave: "Rejected",
                                          principalId: userData.id!,
                                          leaveId: teacherLeaveList
                                              .modelTeacherLeaveList
                                              .data![index]
                                              .id!,
                                          text: "Reject"),
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 8),
                                        color: Red,
                                        child: Text(
                                          "REJECT",
                                          style: TextStyle(
                                              color: White, fontSize: 10.sp),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    InkWell(
                                      onTap: () => showConfirmationDialogue(
                                          //    context: context,
                                          principalId: userData.id!,
                                          leaveId: teacherLeaveList
                                              .modelTeacherLeaveList
                                              .data![index]
                                              .id!,
                                          statusOfLeave: "Approved",
                                          text: "Accept"),
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 8),
                                        color: Green,
                                        child: Text(
                                          "ACCEPT",
                                          style: TextStyle(
                                              color: White, fontSize: 10.sp),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox(),
                          Divider(thickness: 1, color: GreyLight),
                        ],
                      ),
                    );
                  },
                ),
              );
            } else {
              return Center(
                child: Container(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 14),
                      child: Text("No Leaves"),
                    ),
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  showConfirmationDialogue(
      {required int leaveId,
      required String statusOfLeave,
      required int principalId,
      required text}) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(Strings.confirmation),
        content: Text(
          "Are you sure you want to $text this leave request?",
          textScaleFactor: 0.9,
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("NO"),
          ),
          TextButton(
              onPressed: () {
                widget.teacherLeaveListNotifier
                    .leaveStatus(
                        context: _scaffoldKey.currentState!.context,
                        leaveId: leaveId,
                        principalId: principalId,
                        statusOfLeave: statusOfLeave)
                    .then((value) => setState(() {}));
                Navigator.pop(context);
              },
              child: Text("YES")),
        ],
      ),
    );
  }
}
