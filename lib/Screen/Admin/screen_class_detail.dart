import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Model/model_class_detail_for_admin.dart';
import 'package:skoolpod/Notifier/classDetailNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Screen/Admin/screen_class_list.dart';
import 'package:skoolpod/Screen/screen_student_profile.dart';
import 'package:skoolpod/Widgets/network_image_holder.dart';

class ScreenAdminClassDetail extends StatefulWidget {
  ScreenAdminClassDetail({Key? key, this.arguments}) : super(key: key);
  final Arguments? arguments;
  static const String routeName = "adminClassDetail";
  final ClassDetailNotifier classDetailNotifier = ClassDetailNotifier();

  @override
  _ScreenAdminClassDetailState createState() => _ScreenAdminClassDetailState();
}

class _ScreenAdminClassDetailState extends State<ScreenAdminClassDetail> {
  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;
    widget.classDetailNotifier.webCallGetStudentAttendanceForAdmin(
        context,
        userData.id!,
        widget.arguments!.standardId,
        widget.arguments!.divisionId,
        widget.arguments!.scheduleId);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            leading: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              iconSize: 13.0.sp,
              icon: Icon(Icons.arrow_back_ios_new_sharp),
              color: White,
            ),
            title: Text(widget.arguments?.className ?? "",
                style: TextStyle(color: White)),
            centerTitle: false),
        body: ChangeNotifierProvider(
          create: (_) => widget.classDetailNotifier,
          child: Consumer<ClassDetailNotifier>(
            builder: (context, classDetailNotifier, _) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Class taken by:",
                          style: TextStyle(
                            fontSize: 11.0.sp,
                            color: PrimaryColor,
                          ),
                        ),
                        Text(
                          widget.arguments!.teacherFirstName +
                              " " +
                              widget.arguments!.teacherLastName,
                          style: TextStyle(
                            fontSize: 11.0.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 45,
                    width: double.maxFinite,
                    color: PrimaryColor,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "  Total Student (${widget.classDetailNotifier.modelClassDetailForAdmin?.data?.student?.length ?? 0})",
                        style: TextStyle(
                          fontSize: 11.0.sp,
                          color: White,
                        ),
                      ),
                    ),
                  ),
                  if (widget.classDetailNotifier.modelClassDetailForAdmin !=
                      null)
                    Expanded(
                      child: ListView.builder(
                        itemCount: widget.classDetailNotifier
                            .modelClassDetailForAdmin!.data!.student!.length,
                        padding: EdgeInsets.all(15),
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return StudentsItem(
                            widget.classDetailNotifier.modelClassDetailForAdmin!
                                .data!.student![index],
                          );
                        },
                      ),
                    )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

class StudentsItem extends StatelessWidget {
  StudentsItem(this.student);

  final Student student;
  @override
  Widget build(BuildContext context) {
    /**Student Item*/
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Map<String, dynamic> arguments = {
            "studentId": student.id,
            "childProfile": true
          };
          Navigator.of(context)
              .pushNamed(ScreenStudentProfile.routeName, arguments: arguments);
        },
        child: Container(
          margin: EdgeInsets.only(bottom: 10),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8, bottom: 4),
                child: Container(
                  height: 62,
                  child: NetworkImageHolder(imageUrl: student.thumb!),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${student.firstname ?? ""} ${student.lastname ?? ""}",
                      style: TextStyle(
                          fontSize: 11.0.sp,
                          color: PrimaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
