import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Notifier/notificationNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_messages.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:skoolpod/Widgets/square_corner_button.dart';
import 'package:skoolpod/Widgets/ui_component.dart';
import 'package:skoolpod/Widgets/underLineEditTextFormField.dart';

class ScreenPushMessage extends StatefulWidget {
  const ScreenPushMessage({Key? key}) : super(key: key);
  static const String routeName = "pushMessage";

  @override
  _ScreenPushMessageState createState() => _ScreenPushMessageState();
}

class _ScreenPushMessageState extends State<ScreenPushMessage> {
  final _notificationTitleController = TextEditingController();
  final _notificationMessageController = TextEditingController();
  Data? data;
  var _packageType = "SendNow";
  String _sendNotificationTo = "";
  bool _isTeacherSelected = false, _isParentSelected = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: GlobalAppBar(
          title: Strings.push_message,
          showAction: false,
        ),
        body: ChangeNotifierProvider(
          create: (_) => NotificationNotifier(),
          child: Consumer<NotificationNotifier>(
            builder: (context, notificationNotifier, _) {
              return Form(
                key: _formKey,
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UnderLineEditTextFormField(
                            textController: _notificationTitleController,
                            labelText: "Message title",
                            validation:
                                notificationNotifier.validateNotificationTitle,
                          ),
                          SizedBox(
                            height: 10,
                          ),

                          UnderLineEditTextFormField(
                            textController: _notificationMessageController,
                            labelText: "Type your message here",
                            validation: notificationNotifier
                                .validateNotificationMessage,
                            maxLines: 3,
                          ),

                          SizedBox(
                            height: 12,
                          ),

                          /// Radio button  /*
                          Row(
                            children: <Widget>[
                              Radio(
                                value: "SendNow",
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                groupValue: _packageType,
                                activeColor: PrimaryColor,
                                onChanged: (value) {
                                  setState(
                                    () {
                                      _packageType = value.toString();
                                    },
                                  );
                                },
                              ),
                              Text(
                                'Send Now',
                                style: TextStyle(
                                    fontSize: 10.0.sp, color: PrimaryColor),
                              ),

                              /*Radio(value: "Later",
                                      groupValue: _packageType,activeColor: PrimaryColor,
                                      onChanged: (value){
                                        setState(() {
                                          _packageType=value.toString();
                                        });
                                      }),
                                  Text('Send Later',style: TextStyle(fontSize: 10.0.sp,PrimaryColor)),*/
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 40,
                            width: double.maxFinite,
                            padding: EdgeInsets.all(10),
                            color: PrimaryColor,
                            child: Text(
                              "Select user",
                              style: TextStyle(color: White, fontSize: 12.0.sp),
                            ),
                          ),

                          /// Check box  /*
                          Row(
                            children: <Widget>[
                              Checkbox(
                                value: _isTeacherSelected,
                                activeColor: PrimaryColor,
                                onChanged: (value) {
                                  setState(() {});
                                  _isTeacherSelected = value as bool;
                                },
                              ),
                              Text(
                                'Teachers',
                                style: TextStyle(fontSize: 10.0.sp),
                              ),
                              Checkbox(
                                value: _isParentSelected,
                                activeColor: PrimaryColor,
                                onChanged: (value) {
                                  setState(
                                    () {
                                      _isParentSelected = value as bool;
                                    },
                                  );
                                },
                              ),
                              Text(
                                'Parents',
                                style: TextStyle(
                                  fontSize: 10.0.sp,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: SquareButton(
                        text: Strings.send,
                        onPressed: () async {
                          final isValid = _formKey.currentState?.validate();
                          if (isValid! && validateSelectedUser()) {
                            _formKey.currentState?.save();
                            Ui.showConfirmationAlert(
                              context,
                              "Are you sure you want to send this notification to all selected users?",
                              () async {
                                Map<String, String> param = new Map();
                                var id = Provider.of<SessionNotifier>(context,
                                        listen: false)
                                    .userData!
                                    .id
                                    .toString();
                                param['principal_id'] = id;
                                param['title'] =
                                    _notificationTitleController.text;
                                param['message'] =
                                    _notificationMessageController.text;
                                param['type'] = _sendNotificationTo;

                                var response = await notificationNotifier
                                    .sendNotification(param, context);
                                if (response != null && response.status == 1) {
                                  Ui.showSuccessToastMessage(
                                      response.msg ?? "Notification send");
                                  Navigator.of(context).pop();
                                }
                              },
                            );
                          }
                        },
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  bool validateSelectedUser() {
    if (!_isTeacherSelected && !_isParentSelected) {
      Ui.showErrorToastMessage(
          "Select user whom you want to send a notification");
      return false;
    } else {
      if (_isTeacherSelected && _isParentSelected) {
        _sendNotificationTo = "All";
      } else if (!_isTeacherSelected && _isParentSelected) {
        _sendNotificationTo = "Parent";
      } else if (_isTeacherSelected && !_isParentSelected) {
        _sendNotificationTo = "Teacher";
      }
      return true;
    }
  }

  @override
  void dispose() {
    _notificationTitleController.dispose();
    _notificationMessageController.dispose();
    super.dispose();
  }
}
