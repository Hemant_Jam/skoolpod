import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Model/model_standerd_devision.dart';
import 'package:skoolpod/Notifier/adminClassListNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Admin/screen_class_detail.dart';
import 'package:skoolpod/Widgets/appbar.dart';

class ScreenClassListForAdmin extends StatelessWidget {
  ScreenClassListForAdmin({Key? key}) : super(key: key);
  static const String routeName = "classListForAdmin";

  @override
  Widget build(BuildContext context) {
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;

    return SafeArea(
      child: ChangeNotifierProvider(
        create: (_) => AdminClassDivisionNotifier(
            context, userData.id!, userData.school!.id!),
        child: Scaffold(
          appBar: GlobalAppBar(title: Strings.class_list),
          body: Consumer<AdminClassDivisionNotifier>(
            builder: (context, classDivisionNotifier, _) {
              return classDivisionNotifier.divList.isNotEmpty
                  ? ListView.builder(
                      itemCount: classDivisionNotifier.divList.length,
                      padding: const EdgeInsets.all(8),
                      itemBuilder: (BuildContext context, int index) {
                        return ClassItem(
                          classDivisionNotifier.divList[index],
                        );
                      },
                    )
                  : Container(
                      child: Center(
                        child: Text("No Classes found"),
                      ),
                    );
            },
          ),
        ),
      ),
    );
  }
}

class ClassItem extends StatefulWidget {
  ClassItem(this.divList, {Key? key}) : super(key: key);
  final DivData divList;
  @override
  _ClassItemState createState() => _ClassItemState();
}

class _ClassItemState extends State<ClassItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ExpansionTile(
        iconColor: PrimaryColor,
        title: Text(
          "Std: ${widget.divList.name!.stdName}",
          style: TextStyle(color: PrimaryColor, fontSize: 11.0.sp),
        ),
        subtitle: Text(
          "Dive: ${widget.divList.name!.name}",
          style: TextStyle(fontSize: 11.0.sp),
        ),
        children: widget.divList.data!.map(
          (classData) {
            return SubjectItem(
              stdId: widget.divList.name!.stdID,
              divID: widget.divList.name!.id!,
              classData: classData,
            );
          },
        ).toList(),
      ),
    );
  }
}

class SubjectItem extends StatelessWidget {
  const SubjectItem(
      {Key? key,
      required this.classData,
      required this.divID,
      required this.stdId})
      : super(key: key);
  final int stdId;
  final int divID;
  final ClassData classData;

  @override
  Widget build(BuildContext context) {
    double mostUsedFontSized = 10.0.sp;

    /**Subject Item Widget*/
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 12),
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8, bottom: 4),
              child: Container(
                  height: 68,
                  child: Image.asset("assets/images/ic_class_board.png")),
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 7,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  Arguments arguments = Arguments()
                    ..scheduleId = classData.name!.id!
                    ..className = classData.name!.name!
                    ..standardId = stdId
                    ..divisionId = divID;
                  if (classData.data != null) {
                    arguments.teacherFirstName = classData.data![0].firstname!;
                    arguments.teacherLastName = classData.data![0].lastname!;
                  }
                  Navigator.of(context).pushNamed(
                      ScreenAdminClassDetail.routeName,
                      arguments: arguments);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      classData.name!.name ?? "",
                      style: TextStyle(
                          fontSize: 11.0.sp,
                          color: PrimaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 2),
                    if (classData.data != null)
                      Text(
                        "Taken by: ${classData.data![0].firstname} ${classData.data![0].lastname}",
                        style: TextStyle(
                          fontSize: mostUsedFontSized,
                        ),
                      ),
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }
}

class Arguments {
  int scheduleId = 0;
  String className = "";
  int standardId = 0;
  int divisionId = 0;
  String teacherFirstName = "";
  String teacherLastName = "";
}
