import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Model/model_admin_dashboard.dart' as AdminData;
import 'package:skoolpod/Notifier/adminDashboardNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_login.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Screen/Admin/screen_class_list.dart';
import 'package:skoolpod/Screen/AttendanceScreen/screen_attendance.dart';
import 'package:skoolpod/Screen/Principal/screen_teacher_leave_list.dart';
import 'package:skoolpod/Screen/screen_login.dart';
import 'package:skoolpod/Screen/screen_messages.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/home_appbar.dart';
import 'package:skoolpod/Widgets/iCardRowItem.dart';
import 'package:skoolpod/Widgets/iCardSingleItem.dart';
import 'package:skoolpod/Widgets/network_image_holder.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class ScreenAdminHome extends StatelessWidget {
  const ScreenAdminHome({Key? key}) : super(key: key);
  static const String routeName = "adminHome";
  @override
  Widget build(BuildContext context) {
    Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    const double commonWidth = 5;

    void logout() {
      Provider.of<SessionNotifier>(context, listen: false).clearSession();
      Navigator.of(context).popAndPushNamed(ScreenLogin.routeName);
    }

    return SafeArea(
      child: Scaffold(
        appBar: HomeScreenAppBar(userData: userData),
        body: ChangeNotifierProvider(
          create: (_) => AdminDashBoardNotifier(context, userData.id!),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            child: ListView(
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 14,
                    ),
                    Container(
                        height: 45,
                        width: 45,
                        child: NetworkImageHolder(
                          imageUrl: userData.school?.logo,
                          placeHolder: "assets/images/logo.png",
                        )
                        /*CachedNetworkImage(
                              imageUrl: ApiServices.BASE_IMAGE_URL+userData.school!.logo!,
                              imageBuilder: (context, imageProvider) => Container(
                                width: 68.0,
                                height: 68.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              //placeholder: (context, url) => Image.asset("assets/images/ic_student_placeholder.png"),
                              errorWidget: (context, url, error) => Container(height: 68,
                                  child: Image.asset("assets/images/logo.png",)),
                            )
                            : Image.asset("assets/images/logo.png",)*/
                        ),
                    SizedBox(
                      width: 13,
                    ),
                    Text(
                      userData.school?.name ?? "",
                      style: TextStyle(
                          fontSize: 13.0.sp,
                          fontFamily: Strings.BALOO,
                          color: Colors.grey[700]),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Stack(
                  children: [
                    ICardSingleItem(
                      title: Strings.attendance,
                      backgroundImage: "assets/images/dash_bg_attendance.png",
                      iconImage: "assets/images/ic_dash_attendance.png",
                      navigationPage: AttendancePage.routeName,
                    ),
                    Consumer<AdminDashBoardNotifier>(
                      builder: (context, adminDashBoardNotifier, _) {
                        AdminData.Data? data;
                        if (adminDashBoardNotifier.modelAdminDashboard != null)
                          data =
                              adminDashBoardNotifier.modelAdminDashboard!.data;
                        return Padding(
                          padding: const EdgeInsets.only(top: 12, left: 10),
                          child: data != null
                              ? Column(
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                        text: "Teacher: ",
                                        style: TextStyle(
                                            color: White,
                                            fontFamily: Strings.VARELA,
                                            fontSize: 12.0.sp),
                                        children: <TextSpan>[
                                          TextSpan(
                                            text:
                                                '${data.teacher!.present}/${data.teacher!.total}',
                                            style: TextStyle(
                                              color: White,
                                              fontFamily: Strings.VARELA,
                                              fontSize: 13.0.sp,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    RichText(
                                      text: TextSpan(
                                        text: "Student: ",
                                        style: TextStyle(
                                            color: White,
                                            fontFamily: Strings.VARELA,
                                            fontSize: 12.0.sp),
                                        children: <TextSpan>[
                                          TextSpan(
                                            text:
                                                '${data.student!.present}/${data.student!.total}',
                                            style: TextStyle(
                                              color: White,
                                              fontFamily: Strings.VARELA,
                                              fontSize: 13.0.sp,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                )
                              : Container(),
                        );
                      },
                    )
                  ],
                ),
                const SizedBox(height: commonWidth),
                Row(
                  children: [
                    ICardRowItem(
                        title: Strings.message,
                        backgroundImage: "assets/images/dash_bg_message.png",
                        iconImage: "assets/images/ic_dash_messages.png",
                        navigationPage: ScreenMessages.routeName),
                    const SizedBox(
                      width: commonWidth,
                    ),
                    ICardRowItem(
                      title: Strings.calender,
                      backgroundImage: "assets/images/dash_bg_reminder.png",
                      iconImage: "assets/images/ic_dash_reminder.png",
                    ),
                  ],
                ),
                const SizedBox(
                  height: commonWidth,
                ),
                ICardSingleItem(
                  title: Strings.classes,
                  backgroundImage: "assets/images/dash_bg_class.png",
                  iconImage: "assets/images/ic_dash_class.png",
                  navigationPage: ScreenClassListForAdmin.routeName,
                ),
                const SizedBox(
                  height: commonWidth,
                ),
                Row(
                  children: [
                    ICardRowItem(
                      title: Strings.leaves,
                      backgroundImage: "assets/images/dash_bg_leave.png",
                      iconImage: "assets/images/ic_dash_leave.png",
                      navigationPage: TeacherLeaveList.routeName,
                    ),
                    const SizedBox(
                      width: commonWidth,
                    ),
                    ICardRowItem(
                      title: Strings.query,
                      backgroundImage: "assets/images/dash_bg_faq.png",
                      iconImage: "assets/images/ic_dash_query_color.png",
                    ),
                  ],
                ),
                const SizedBox(
                  height: commonWidth,
                ),
                Row(
                  children: [
                    ICardRowItem(
                      title: Strings.staff_directory,
                      backgroundImage: "assets/images/dash_bg_reminder.png",
                      iconImage: "assets/images/ic_dash_staff.png",
                    ),
                    const SizedBox(width: commonWidth),
                    ICardRowItem(
                      title: Strings.video_library,
                      backgroundImage: "assets/images/dash_bg_message.png",
                      iconImage: "assets/images/ic_dash_feed.png",
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                GestureDetector(
                  onTap: () {
                    Ui.showConfirmationAlert(
                      context,
                      Strings.sure_to_logout,
                      logout,
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ImageIcon(
                        AssetImage("assets/images/ic_dash_logout.png"),
                        color: Colors.red,
                        size: 3.5.h,
                      ),
                      Text(
                        "  Logout",
                        style: TextStyle(
                          color: PrimaryColor,
                          fontFamily: "Baloo",
                          fontSize: 17.0.sp,
                        ),
                      ),
                      const SizedBox(width: commonWidth),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
