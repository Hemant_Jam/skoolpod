import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Notifier/childListNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Parent/screen_child_leave_list.dart';
import 'package:skoolpod/Screen/screen_student_profile.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:skoolpod/Widgets/network_image_holder.dart';

class ScreenChildList extends StatelessWidget {
  ScreenChildList({Key? key, this.type}) : super(key: key);
  static const String routeName = "childList";
  final String? type;
  final ChildListNotifier childListNotifier = ChildListNotifier();

  @override
  Widget build(BuildContext context) {
    int userId = Provider.of<SessionNotifier>(context).userData!.id!;
    childListNotifier.webCallGetChildList(context, userId);
    return SafeArea(
      child: Scaffold(
        appBar: GlobalAppBar(
            title: type == "Profile" ? Strings.select_child : Strings.leaves),
        body: ChangeNotifierProvider(
          create: (_) => childListNotifier,
          child: Consumer<ChildListNotifier>(
            builder: (context, childListNotifier, _) {
              if (childListNotifier.data != null)
                return ListView.builder(
                  itemCount: childListNotifier.data!.length,
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            String routeName = "";
                            Map<String, dynamic> arguments = {
                              "studentId": childListNotifier.data![index].id,
                              "parentId":
                                  childListNotifier.data![index].parentId,
                              "studentName":
                                  "${childListNotifier.data![index].firstname ?? "Student"} ${childListNotifier.data![index].lastname ?? "Name"}",
                              "childProfile": false
                            };
                            if (type == "Profile") {
                              routeName = ScreenStudentProfile.routeName;
                              Navigator.of(context)
                                  .pushNamed(routeName, arguments: arguments)
                                  .then((value) => childListNotifier
                                      .webCallGetChildList(context, userId));
                            } else {
                              routeName = ScreenStudentLeaveList.routeName;
                              Navigator.of(context)
                                  .pushNamed(routeName, arguments: arguments);
                            }
                          },
                          child: Row(
                            children: [
                              Container(
                                height: 62,
                                margin: const EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 16),
                                child: NetworkImageHolder(
                                    imageUrl:
                                        childListNotifier.data![index].image),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${childListNotifier.data![index].firstname ?? "Student"} ${childListNotifier.data![index].lastname ?? "Name"}",
                                    style: TextStyle(
                                        fontSize: 12.0.sp, color: PrimaryColor),
                                  ),
                                  Text(
                                      "${childListNotifier.data![index].standard?.name ?? "Teacher"} ${childListNotifier.data![index].division?.name ?? "Name"}"),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Divider(thickness: 1),
                      ],
                    );
                  },
                );
              else
                return Container();
            },
          ),
        ),
      ),
    );
  }
}
