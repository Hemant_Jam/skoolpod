import 'package:flutter/material.dart';
import 'package:skoolpod/Notifier/childLeaveAddNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Widgets/ui_component.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;

class ScreenChildLeaveAdd extends StatefulWidget {
  final Map<String, dynamic> argument;
  ScreenChildLeaveAdd({Key? key, required this.argument}) : super(key: key);
  static const String routeName = "screenChildLeaveAdd";
  final ChildLeaveAddNotifier childLeaveAddNotifier = ChildLeaveAddNotifier();
  @override
  _ScreenChildLeaveAddState createState() => _ScreenChildLeaveAddState();
}

class _ScreenChildLeaveAddState extends State<ScreenChildLeaveAdd> {
  final formKey = new GlobalKey<FormState>();

  TextEditingController selectFromDateController = TextEditingController();
  TextEditingController selectToDateController = TextEditingController();
  TextEditingController selectReasonController = TextEditingController();
  bool showLeave = false;
  DateTime fromDate = DateTime.now();
  bool fromDateSelected = false;
  bool toDateSelected = false;
  DateTime toDate = DateTime.now();
  FocusNode focusNode = FocusNode();
  String hintText = Strings.type_your_reason_here;
  int dayDifference = 0;

  @override
  void initState() {
    if (widget.argument["leaveId"] != null) {
      widget.childLeaveAddNotifier
          .changeLeaveType(leaveType: widget.argument["leaveType"]);
      selectFromDateController.text = DateFormat("MMM-dd-yyyy")
          .format(DateTime.parse(widget.argument["startDate"]));
      selectToDateController.text = DateFormat("MMM-dd-yyyy")
          .format(DateTime.parse(widget.argument["endDate"]));
      showLeave = true;
      dayDifference = widget.argument["days"];
      selectReasonController.text = widget.argument["reason"];
    }
    super.initState();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = Strings.type_your_reason_here;
      }
      setState(() {});
    });
  }

  @override
  void didChangeDependencies() {
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    widget.childLeaveAddNotifier
        .getTeacherList(context: context, studentId: userData.schoolId!)
        .then(
      (value) {
        List dataList = value['data'];
        dataList.forEach(
          (element) {
            if (widget.argument["leaveId"] != null) if (element["id"] ==
                widget.argument["teacherId"]) {
              widget.childLeaveAddNotifier.changeTeacherName(
                  teacherName: "${element["firstname"]} ${element["lastname"]}",
                  teacherID: widget.argument["teacherId"]);
            }
          },
        );
      },
    );
    super.didChangeDependencies();
  }

  void showDialogOfTeacher() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(
            "Select your teacher",
            textScaleFactor: 1.2,
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          content: widget.childLeaveAddNotifier.modelAddChildLeave.data != null
              ? Container(
                  width: double.maxFinite,
                  child: ListView.builder(
                    itemCount: widget
                        .childLeaveAddNotifier.modelAddChildLeave.data!.length,
                    shrinkWrap: true,
                    itemBuilder: (context, int index) {
                      String teacherName =
                          "${widget.childLeaveAddNotifier.modelAddChildLeave.data![index].firstname} ${widget.childLeaveAddNotifier.modelAddChildLeave.data![index].lastname}";
                      return ListTile(
                        onTap: () {
                          widget.childLeaveAddNotifier.changeTeacherName(
                              teacherID: widget.childLeaveAddNotifier
                                  .modelAddChildLeave.data![index].id!,
                              teacherName:
                                  "${widget.childLeaveAddNotifier.modelAddChildLeave.data![index].firstname} ${widget.childLeaveAddNotifier.modelAddChildLeave.data![index].lastname}");
                          Navigator.pop(context);
                        },
                        title: Text(teacherName),
                      );
                    },
                  ),
                )
              : CircularProgressIndicator(color: Colors.transparent),
        );
      },
    );
  }

  void selectLeaveType() {
    List<String> reasonList = ["Sick", "SOS", "Special", "Other"];
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(
            "Select leave type",
            textScaleFactor: 1.2,
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          content: ListView.builder(
            shrinkWrap: true,
            itemCount: reasonList.length,
            // padding: EdgeInsets.all(2),
            physics: NeverScrollableScrollPhysics(),
            addAutomaticKeepAlives: true,
            itemBuilder: (context, int index) => ListTile(
              contentPadding: EdgeInsets.zero,
              //  dense: true,
              horizontalTitleGap: 00,
              onTap: () {
                Navigator.pop(context);
                widget.childLeaveAddNotifier
                    .changeLeaveType(leaveType: reasonList[index]);
              },
              title: Text(reasonList[index]),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;

    return Scaffold(
      appBar: GlobalAppBar(title: Strings.add_leaves, showAction: false),
      body: ChangeNotifierProvider(
        create: (_) => widget.childLeaveAddNotifier,
        child: Consumer<ChildLeaveAddNotifier>(
          builder: (context, addChildLeaveNotifier, child) {
            return Form(
              key: formKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    TextFormField(
                      style: TextStyle(color: PrimaryColor),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Select teacher";
                        }
                      },
                      onTap: () => showDialogOfTeacher(),
                      readOnly: true,
                      controller: addChildLeaveNotifier.selectTeacherController,
                      decoration: InputDecoration(
                        hintText: "Select Teacher",
                        suffixIcon: Icon(
                          Icons.arrow_drop_down_sharp,
                          color: PrimaryColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            style: TextStyle(color: PrimaryColor),
                            validator: (value) {
                              if (value!.isEmpty) return Strings.select_date;
                            },
                            controller: selectFromDateController,
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.all(10),
                                      content: Container(
                                        height: 30.h,
                                        child: CupertinoDatePicker(
                                          minimumDate: DateTime.now(),
                                          initialDateTime: fromDate
                                                      .difference(
                                                          DateTime.now())
                                                      .inDays ==
                                                  0
                                              ? fromDate.add(Duration(days: 1))
                                              : fromDate,
                                          onDateTimeChanged:
                                              (DateTime newFromDate) {
                                            fromDate = newFromDate;
                                          },
                                          mode: CupertinoDatePickerMode.date,
                                        ),
                                      ),
                                      actions: [
                                        TextButton(
                                          onPressed: () {
                                            dayDifference = toDate
                                                    .difference(fromDate)
                                                    .inDays +
                                                1;
                                            showLeave = true;
                                            selectFromDateController.value =
                                                TextEditingValue(
                                                    text: DateFormat(
                                                            "MMM-dd-yyyy")
                                                        .format(fromDate));
                                            fromDateSelected = true;
                                            Navigator.pop(context);
                                          },
                                          child: Text("OK"),
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Text("CANCEL"),
                                        ),
                                      ],
                                    );
                                  }).then(
                                (value) => setState(() {}),
                              );
                            },
                            readOnly: true,
                            decoration: InputDecoration(
                              hintText: Strings.from_date,
                              suffixIcon: Icon(
                                Icons.calendar_today_outlined,
                                color: PrimaryColor,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            style: TextStyle(color: PrimaryColor),
                            validator: (value) {
                              if (value!.isEmpty) return Strings.select_date;
                            },
                            controller: selectToDateController,
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.all(10),
                                      content: Container(
                                        height: 30.h,
                                        child: CupertinoDatePicker(
                                          minimumDate: DateTime.now(),
                                          initialDateTime: toDate
                                                      .difference(
                                                          DateTime.now())
                                                      .inDays ==
                                                  0
                                              ? toDate.add(Duration(days: 1))
                                              : toDate,
                                          onDateTimeChanged:
                                              (DateTime newToDate) {
                                            toDate = newToDate;
                                          },
                                          mode: CupertinoDatePickerMode.date,
                                        ),
                                      ),
                                      actions: [
                                        TextButton(
                                          onPressed: () {
                                            selectToDateController.value =
                                                TextEditingValue(
                                                    text: DateFormat(
                                                            "MMM-dd-yyyy")
                                                        .format(toDate));

                                            toDateSelected = true;
                                            dayDifference = toDate
                                                    .difference(fromDate)
                                                    .inDays +
                                                1;
                                            Navigator.pop(context);
                                          },
                                          child: Text("OK"),
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Text("CANCEL"),
                                        )
                                      ],
                                    );
                                  }).then(
                                (value) => setState(() {}),
                              );
                            },
                            readOnly: true,
                            decoration: InputDecoration(
                              hintText: Strings.to_date,
                              suffixIcon: Icon(
                                Icons.calendar_today_outlined,
                                color: PrimaryColor,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: Row(
                            children: [
                              Text("Days: ",
                                  style: TextStyle(color: PrimaryColor)),
                              dayDifference > 0
                                  ? Text(
                                      dayDifference.toString(),
                                    )
                                  : Text(""),
                            ],
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            style: TextStyle(color: PrimaryColor),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "select Type";
                              }
                            },
                            onTap: () => selectLeaveType(),
                            readOnly: true,
                            controller:
                                addChildLeaveNotifier.selectLeaveTypeController,
                            decoration: InputDecoration(
                              hintText: "Type",
                              suffixIcon: Icon(
                                Icons.arrow_drop_down_sharp,
                                color: PrimaryColor,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      focusNode: focusNode,
                      validator: (String? value) {
                        if (value!.isEmpty)
                          return Strings.enter_reason_for_leave;
                      },
                      controller: selectReasonController,
                      decoration: InputDecoration(
                          labelText: Strings.type_your_reason_here,
                          hintText: hintText),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            );
          },
        ),
      ),
      bottomNavigationBar: InkWell(
        onTap: () async {
          if (widget.childLeaveAddNotifier.selectTeacherController.text
              .isNotEmpty) if (fromDateSelected) if (toDateSelected) if (formKey.currentState!.validate()) {
            widget.argument["leaveId"] == null
                ? await widget.childLeaveAddNotifier

                    /// for new leave for child
                    .addChildLeave(
                        context: context,
                        days: dayDifference,
                        startDate: DateFormat("yyyy-MM-dd")
                            .format(fromDate)
                            .toString(),
                        endDate:
                            DateFormat("yyyy-MM-dd").format(toDate).toString(),
                        reason: selectReasonController.text,
                        parentId: userData.id!,
                        studentId: widget.argument["studentId"])
                    .then((bool response) {
                    if (response) {
                      Navigator.pop(context, true);
                    }
                  })
                : await widget.childLeaveAddNotifier

                    /// for update existing leave of child
                    .updateChildLeave(
                        leaveId: widget.argument["leaveId"],
                        context: context,
                        days: dayDifference,
                        startDate: DateFormat("yyyy-MM-dd")
                            .format(fromDate)
                            .toString(),
                        endDate:
                            DateFormat("yyyy-MM-dd").format(toDate).toString(),
                        reason: selectReasonController.text,
                        parentId: userData.id!,
                        studentId: widget.argument["studentId"])
                    .then((bool response) {
                    if (response) {
                      Navigator.pop(context, true);
                    }
                  });
          } else
            Ui.showErrorToastMessage(Strings.enter_reason_for_leave);
          else
            Ui.showErrorToastMessage(Strings.select_date);
          else
            Ui.showErrorToastMessage(Strings.select_date);
          else
            Ui.showErrorToastMessage(Strings.select_your_principal);
        },
        child: Container(
          height: 55,
          color: PrimaryColor,
          width: 100.w,
          child: Center(
            child: Text(
              widget.argument["leaveId"] == null ? "ADD" : "UPDATE",
              style: TextStyle(fontSize: 13.0.sp, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
