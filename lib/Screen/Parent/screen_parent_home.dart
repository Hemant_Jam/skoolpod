import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Model/model_login.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Screen/screen_messages.dart';
import 'package:skoolpod/Screen/Teacher/screen_more_option.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/home_appbar.dart';
import 'package:skoolpod/Widgets/iCardRowItem.dart';
import 'package:skoolpod/Widgets/iCardSingleItem.dart';
import 'package:skoolpod/Widgets/network_image_holder.dart';

class ScreenParentHome extends StatelessWidget {
  const ScreenParentHome({Key? key}) : super(key: key);
  static const String routeName = "parentHome";
  @override
  Widget build(BuildContext context) {
    Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    const double commonWidth = 5;
    return SafeArea(
      child: Scaffold(
        appBar: HomeScreenAppBar(userData: userData),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: ListView(
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 14,
                  ),
                  Container(
                      height: 45,
                      width: 45,
                      child: NetworkImageHolder(
                        imageUrl: userData.school!.logo,
                        placeHolder: "assets/images/logo.png",
                      )),
                  //Image.network(ApiServices.BASE_IMAGE_URL+userData.school!.logo!)),
                  SizedBox(
                    width: 13,
                  ),
                  Text(
                    userData.school?.name ?? "",
                    style: TextStyle(
                        fontSize: 13.0.sp,
                        fontFamily: "Baloo",
                        color: Colors.grey[700]),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  ICardRowItem(
                      title: Strings.message,
                      backgroundImage: "assets/images/dash_bg_message.png",
                      iconImage: "assets/images/ic_dash_messages.png",
                      navigationPage: ScreenMessages.routeName),
                  const SizedBox(
                    width: commonWidth,
                  ),
                  ICardRowItem(
                    title: Strings.child_diaries,
                    backgroundImage: "assets/images/dash_bg_leave.png",
                    iconImage: "assets/images/ic_dash_report.png",
                  ),
                ],
              ),
              const SizedBox(
                height: commonWidth,
              ),
              ICardSingleItem(
                title: Strings.child_attendance,
                backgroundImage: "assets/images/dash_bg_class.png",
                iconImage: "assets/images/ic_dash_attendance.png",
              ),
              const SizedBox(
                height: commonWidth,
              ),
              Row(
                children: [
                  ICardRowItem(
                    title: Strings.video_library,
                    backgroundImage: "assets/images/dash_bg_reminder.png",
                    iconImage: "assets/images/ic_dash_feed.png",
                  ),
                  const SizedBox(
                    width: commonWidth,
                  ),
                  ICardRowItem(
                    title: Strings.events,
                    backgroundImage: "assets/images/dash_bg_faq.png",
                    iconImage: "assets/images/ic_dash_reminder.png",
                  ),
                ],
              ),
              const SizedBox(
                height: commonWidth,
              ),
              ICardSingleItem(
                title: Strings.more,
                backgroundImage: "assets/images/dash_bg_attendance.png",
                iconImage: "assets/images/ic_dash_more_menu.png",
                navigationPage: ScreenMoreOption.routeName,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
