import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Notifier/childLeaveListNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Parent/screen_child_leave_add.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:sizer/sizer.dart';

class ScreenStudentLeaveList extends StatefulWidget {
  final Map<String, dynamic> argument;
  final ChildLeaveListNotifier childLeaveListNotifier =
      ChildLeaveListNotifier();
  static const String routeName = "screenStudentLeaveList";
  ScreenStudentLeaveList({Key? key, required this.argument}) : super(key: key);

  @override
  _ScreenStudentLeaveListState createState() => _ScreenStudentLeaveListState();
}

class _ScreenStudentLeaveListState extends State<ScreenStudentLeaveList> {
  @override
  Widget build(BuildContext context) {
    widget.childLeaveListNotifier.getChildLeaveList(
        context: context,
        parentId: widget.argument["parentId"],
        studentId: widget.argument["studentId"]);
    return Scaffold(
      appBar: GlobalAppBar(
          title: "${widget.argument["studentName"]}'s Leave",
          showAction: false),
      body: ChangeNotifierProvider(
        create: (_) => widget.childLeaveListNotifier,
        child: Consumer<ChildLeaveListNotifier>(
          builder: (context, childLeaveList, child) {
            if (childLeaveList.modelChildLeaveList.data != null) {
              return SingleChildScrollView(
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: childLeaveList.modelChildLeaveList.data!.length,
                  itemBuilder: (context, int index) {
                    Color statusColor = Colors.black;
                    switch (childLeaveList
                        .modelChildLeaveList.data![index].status
                        .toString()) {
                      case "Pending":
                        statusColor = ColorYellow;
                        break;
                      case "Canceled":
                        statusColor = ColorUsername;
                        break;
                      case "Approved":
                        statusColor = Green;
                        break;
                      case "Rejected":
                        statusColor = Red;
                        break;
                      default:
                        statusColor = Colors.black;
                    }
                    return Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 4),
                                        child: Text(
                                          "Date : ",
                                          style: TextStyle(
                                            color: PrimaryColor,
                                          ),
                                        ),
                                      ),
                                      Text(
                                        'From ${DateFormat("MMM-dd-yyyy").format(DateTime.parse(childLeaveList.modelChildLeaveList.data![index].start!))} To ${DateFormat("MMM-dd-yyyy").format(DateTime.parse(childLeaveList.modelChildLeaveList.data![index].end!))}',
                                        style: TextStyle(fontSize: 11.sp),
                                      )
                                    ],
                                  ),
                                  Text(
                                    "Reason:",
                                    style: TextStyle(color: PrimaryColor),
                                  ),
                                  Text(
                                      "${childLeaveList.modelChildLeaveList.data![index].reason}"),
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text("Status:",
                                            style:
                                                TextStyle(color: PrimaryColor)),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 4),
                                          child: Text(
                                            "${childLeaveList.modelChildLeaveList.data![index].status}",
                                            style:
                                                TextStyle(color: statusColor),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                  width: 1.5, height: 60, color: statusColor),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                      "${childLeaveList.modelChildLeaveList.data![index].days!.floor()}",
                                      textScaleFactor: 1.5),
                                  Text(
                                      childLeaveList.modelChildLeaveList
                                                  .data![index].days! >
                                              1
                                          ? "Days"
                                          : "Day",
                                      style: TextStyle(color: PrimaryColor)),
                                ],
                              ),
                              childLeaveList.modelChildLeaveList.data![index]
                                              .status
                                              .toString() ==
                                          "Pending" &&
                                      DateTime.parse(childLeaveList
                                              .modelChildLeaveList
                                              .data![index]
                                              .end!)
                                          .isAfter((DateTime.now()))
                                  ? Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        InkWell(
                                          onTap: () async {
                                            Map<String, dynamic> args = {};
                                            args["days"] = childLeaveList
                                                .modelChildLeaveList
                                                .data![index]
                                                .days!;
                                            args["startDate"] = childLeaveList
                                                .modelChildLeaveList
                                                .data![index]
                                                .start!;
                                            args["endDate"] = childLeaveList
                                                .modelChildLeaveList
                                                .data![index]
                                                .end!;
                                            args["reason"] = childLeaveList
                                                .modelChildLeaveList
                                                .data![index]
                                                .reason!;
                                            args["leaveType"] = childLeaveList
                                                .modelChildLeaveList
                                                .data![index]
                                                .type!;
                                            args["teacherId"] = childLeaveList
                                                .modelChildLeaveList
                                                .data![index]
                                                .teacherId!;
                                            args["studentId"] =
                                                widget.argument["studentId"];
                                            args["leaveId"] = childLeaveList
                                                .modelChildLeaveList
                                                .data![index]
                                                .id!;
                                            dynamic isReload =
                                                await Navigator.of(context)
                                                    .pushNamed(
                                                        ScreenChildLeaveAdd
                                                            .routeName,
                                                        arguments: args);
                                            if (isReload) setState(() {});
                                          },
                                          child: Container(
                                            margin: EdgeInsets.all(2),
                                            color: YellowDash,
                                            child: Padding(
                                                padding:
                                                    const EdgeInsets.all(4.0),
                                                child: Icon(Icons.edit,
                                                    color: White)),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () => showDialog(
                                              barrierDismissible: false,
                                              context: context,
                                              builder: (_) => AlertDialog(
                                                    actionsPadding:
                                                        EdgeInsets.zero,
                                                    title: Text(
                                                        Strings.confirmation),
                                                    content: Text(
                                                        Strings.are_you_sure,
                                                        textScaleFactor: 0.8),
                                                    actions: [
                                                      TextButton(
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Text("No",
                                                            style: TextStyle(
                                                                color:
                                                                    PrimaryColor)),
                                                      ),
                                                      TextButton(
                                                        onPressed: () {
                                                          childLeaveList
                                                              .deleteChildLeave(
                                                                  parentId: widget
                                                                          .argument[
                                                                      "parentId"],
                                                                  leaveId: childLeaveList
                                                                      .modelChildLeaveList
                                                                      .data![
                                                                          index]
                                                                      .id!,
                                                                  studentId: widget
                                                                          .argument[
                                                                      "studentId"],
                                                                  context:
                                                                      context)
                                                              .then((value) =>
                                                                  setState(
                                                                      () {}));
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: Text("Yes",
                                                            style: TextStyle(
                                                                color:
                                                                    PrimaryColor)),
                                                      )
                                                    ],
                                                  )).then((value) {
                                            setState(() {});
                                          }),
                                          child: Container(
                                            margin: EdgeInsets.all(2),
                                            color: Red,
                                            child: Padding(
                                                padding:
                                                    const EdgeInsets.all(4.0),
                                                child: Icon(Icons.delete,
                                                    color: White)),
                                          ),
                                        ),
                                      ],
                                    )
                                  : SizedBox()
                            ],
                          ),
                          Divider(thickness: 1, color: GreyLight),
                        ],
                      ),
                    );
                  },
                ),
              );
            } else {
              return Scaffold(
                body: Center(
                  child: Container(
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 14),
                        child: Text(""),
                      ),
                    ),
                  ),
                ),
              );
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: "heroTag1",
        backgroundColor: PrimaryColor,
        onPressed: () async {
          Map<String, dynamic> args = {};
          args["studentId"] = widget.argument["studentId"];
          args["leaveId"] = null;
          args["leaveType"] = "";
          args["teacherId"] = "";
          args["days"] = "";
          args["startDate"] = "";
          args["endDate"] = "";
          args["reason"] = "";
          dynamic isReload = await Navigator.of(context)
              .pushNamed(ScreenChildLeaveAdd.routeName, arguments: args);
          if (isReload) {
            setState(() {});
          }
        },
        child: Icon(Icons.add, color: White),
      ),
    );
  }
}
