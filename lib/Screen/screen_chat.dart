import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter/material.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_messages.dart';
import 'package:skoolpod/Notifier/chatNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Widgets/chat_item.dart';

class ScreenChat extends StatefulWidget {
  ScreenChat({Key? key,required this.conversationItem}) : super(key: key);
  static const String routeName = "chatRoom";
  final Data conversationItem;

  @override
  _ScreenChatState createState() => _ScreenChatState();
}

class _ScreenChatState extends State<ScreenChat> {
  final _messageController = TextEditingController();

  String initialUrl = ApiServices.GET_SINGLE_CONVERSATION;
  ScrollController listScrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {
    int userId = Provider.of<SessionNotifier>(context,listen: false).userData!.id!;
    Map<String,dynamic> param = {"user_id":userId,"conversation_id":widget.conversationItem.id};
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: Align(
            child: IconButton(
              onPressed: ()=>Navigator.of(context).pop()
              ,iconSize: 13.0.sp,
              icon: Icon(Icons.arrow_back_ios_new_sharp),color: White,),
          ),
          centerTitle: false,
          title: Row(
            children: [
              Container(height: 40,
              child: widget.conversationItem.image!.isNotEmpty?
              CachedNetworkImage(
                imageUrl: widget.conversationItem.image!,
                imageBuilder: (context, imageProvider) => Container(
                  width: 68.0,
                  height: 68.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                errorWidget: (context, url, error) => Container(height: 68,
                    child: Image.asset("assets/images/ic_student_placeholder.png",)),
              ):
              Image.asset("assets/images/ic_student_placeholder.png",)
            ),
              SizedBox(width: 10,),
              Text(widget.conversationItem.name!,style: TextStyle(color: White,fontSize: 14.0.sp),),
            ],
          ),
        ),

        body: ChangeNotifierProvider(
          create: (context)=> ChatNotifier(context, initialUrl, param),
          child: Consumer<ChatNotifier>(
            builder: (context, chatNotifier,_) {

              return Stack(
                children: <Widget>[
                  ///*Background image */
                  Container(
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: new ExactAssetImage('assets/images/splash_new.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Container(color: Colors.white.withAlpha(150),),
                  ),

                  ///* Chat list */
                  if(chatNotifier.data!=null && chatNotifier.data!.data!.isNotEmpty)
                  ListView.builder(
                    padding: EdgeInsets.fromLTRB(10.0,10,10,100),
                    itemBuilder: (context, index) => ChatItemWidget(chatNotifier.data!.data![index],index,userId),
                    itemCount: chatNotifier.data!.data?.length??0,
                    reverse: true,
                    controller: listScrollController,),

                  ///* Bottom Text Field */

                  Align(
                    alignment: Alignment.bottomLeft,
                    child:
                    Container(color: Colors.white,
                      margin: EdgeInsets.symmetric(horizontal: 8,),
                      height: 62,
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 15,right: 15),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(35.0),
                                boxShadow: [
                                  BoxShadow(
                                      offset: Offset(0, 3),
                                      blurRadius: 5,
                                      color: Colors.grey)
                                ],
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: TextField(
                                      controller: _messageController,
                                      keyboardType: TextInputType.multiline,
                                      maxLines: null,
                                      decoration: InputDecoration(
                                          hintText: "Type a message...",
                                          border: InputBorder.none),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(width: 8),
                          Container(
                            padding: const EdgeInsets.all(15.0),
                            decoration: BoxDecoration(
                                color: PrimaryColor, shape: BoxShape.circle),
                            child: InkWell(
                              child: Icon(Icons.send,color: Colors.white,),
                              onTap: () {
                                if(_messageController.text.isNotEmpty){
                                  Map<String,String> messageData = {
                                    "apikey":ApiServices.API_KEY,
                                    "user_id":userId.toString(),
                                    "conversation_id":widget.conversationItem.id.toString(),
                                    "body":_messageController.text.trim(),
                                    "title":"Test",
                                    "type":Strings.MESSAGE_TEXT,
                                  };
                                  chatNotifier.sendMessage(messageData).then((value) {
                                    if(value)
                                  _messageController.clear();
                                  });
                                }
                              },
                            ),
                          )
                        ],
                      ),
                    ) ,
                  ),
                ],
              );
            }
          ),
        ),
      ),
    );
  }
}
