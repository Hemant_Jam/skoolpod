import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Model/model_login.dart' as LoginData;
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Screen/Admin/screen_admin_home.dart';
import 'package:skoolpod/Screen/Parent/screen_parent_home.dart';
import 'package:skoolpod/Screen/Teacher/screen_teacher_home.dart';
import 'package:skoolpod/Screen/screen_login.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Resources/strings.dart';

class ScreenSplash extends StatefulWidget {
  static const String routeName = "splash";
  @override
  _ScreenSplashState createState() => _ScreenSplashState();
}

class _ScreenSplashState extends State<ScreenSplash> {
  @override
  void initState() {
    super.initState();
    Provider.of<SessionNotifier>(context,listen: false).getPreferenceData().then((userData){
      if(userData!=null){
        String routeName=ScreenTeacherHome.routeName;
        if(userData.role == Strings.TEACHER)
          routeName=ScreenTeacherHome.routeName;
        else if(userData.role == Strings.PARENT)
          routeName=ScreenParentHome.routeName;
        else if(userData.role == Strings.PRINCIPAL)
          routeName=ScreenAdminHome.routeName;
        Timer(
            Duration(seconds: 4),
                () {
              Navigator.of(context).popAndPushNamed(routeName);
            } );
      }
      else{
        Timer(
            Duration(seconds: 4),
                () {
              Navigator.of(context).popAndPushNamed(ScreenLogin.routeName);
            } );
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    //String imageUrl = Provider.of<SessionNotifier>(context,listen: false).userData?.school?.logo??"";
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Center(
            child: Image.asset('assets/images/splash.png'),
          ),
          Center(
              child:Container(width:38.0.w,
                child: FutureBuilder<LoginData.Data?>(
                  future: Provider.of<SessionNotifier>(context,listen: false).getPreferenceData(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return CachedNetworkImage(
                        imageUrl: ApiServices.BASE_IMAGE_URL+snapshot.data!.school!.logo!,
                        errorWidget: (context, url, error) => Image.asset("assets/images/logo.png",),

                      );
                    } else {
                      return Container(
                        child: Image.asset('assets/images/logo.png'),
                      );
                    }
                  },
                ),
              )
            /*Container(width:110 ,
              child: Image.asset('assets/images/logo.png'),
            ),*/
          ),
        ],
      ),
    );
  }
}
