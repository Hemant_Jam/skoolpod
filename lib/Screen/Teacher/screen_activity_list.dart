import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Notifier/activityListNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;

class ScreenActivityList extends StatefulWidget {
  ScreenActivityList({Key? key, required this.argument}) : super(key: key);
  final ActivityListNotifier activityListNotifier = ActivityListNotifier();
  final Map<String, dynamic> argument;
  static const String routeName = "screenActivityList";
  @override
  _ScreenActivityListState createState() => _ScreenActivityListState();
}

class _ScreenActivityListState extends State<ScreenActivityList> {
  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;
    widget.activityListNotifier.webCallGetActivityList(
        context: context,
        studentId: widget.argument["studentId"],
        teacherId: userData.id!);
    return Scaffold(
      appBar: GlobalAppBar(
          title: widget.argument["studentName"], showAction: false),
      body: ChangeNotifierProvider(
        create: (_) => widget.activityListNotifier,
        child: Consumer<ActivityListNotifier>(
          builder: (context, activityList, child) {
            if (activityList.modelStudentActivityList != null) {
              return ListView.builder(
                shrinkWrap: true,
                itemCount: activityList.modelStudentActivityList!.data!.length,
                itemBuilder: (context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "${activityList.modelStudentActivityList!.data![index].activity!.name ?? ""}",
                              style:
                                  TextStyle(color: PrimaryColor, fontSize: 18),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 4.0),
                              child: Text(
                                  "${activityList.modelStudentActivityList!.data![index].lecture!.name ?? ""}"),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 4.0),
                                  child: Container(
                                    height: 50,
                                    width: 50,
                                    child: CachedNetworkImage(
                                      errorWidget: (_, s, e) =>
                                          Icon(Icons.error),
                                      imageUrl:
                                          "${activityList.modelStudentActivityList!.data![index].image}",
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text("Comment :"),
                                        Text(
                                            "${activityList.modelStudentActivityList!.data![index].description}"),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(bottom: 4),
                                        width: 20,
                                        height: 20,
                                        child: CachedNetworkImage(
                                          errorWidget: (_, s, e) =>
                                              Icon(Icons.error),
                                          imageUrl:
                                              "${activityList.modelStudentActivityList!.data![index].badge!.image ?? ""}",
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${activityList.modelStudentActivityList!.data![index].badge!.name ?? ""}",
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Container(
                              alignment: Alignment.bottomRight,
                              child: Text(
                                DateFormat("MMM dd,yyyy hh:mm a").format(
                                    DateTime.parse(activityList
                                        .modelStudentActivityList!
                                        .data![index]
                                        .createdAt!)),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            } else {
              return SizedBox();
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: PrimaryColor,
        onPressed: () {},
        child: Icon(
          Icons.add,
          color: White,
        ),
      ),
    );
  }
}
