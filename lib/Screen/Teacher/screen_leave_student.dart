import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:skoolpod/Notifier/studentLeavesNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/strings.dart';

class LeaveItemsStudent extends StatefulWidget {
  final StudentLeavesNotifier studentLeavesNotifier = StudentLeavesNotifier();
  LeaveItemsStudent({Key? key}) : super(key: key);
  @override
  _LeaveItemsStudentState createState() => _LeaveItemsStudentState();
}

class _LeaveItemsStudentState extends State<LeaveItemsStudent>
    with AutomaticKeepAliveClientMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    widget.studentLeavesNotifier
        .webCallGetStudentLeaves(context: context, userId: userData.id!);
    return Scaffold(
      key: _scaffoldKey,
      body: ChangeNotifierProvider(
        create: (_) => widget.studentLeavesNotifier,
        child: Consumer<StudentLeavesNotifier>(
          builder: (context, studentLeavesData, child) {
            if (studentLeavesData.modelStudentLeave.data != null) {
              return ListView.builder(
                shrinkWrap: true,
                itemCount: studentLeavesData.modelStudentLeave.data!.length,
                itemBuilder: (context, int index) {
                  Color statusColor = Colors.black;
                  switch (studentLeavesData
                      .modelStudentLeave.data![index].status
                      .toString()) {
                    case "Pending":
                      statusColor = ColorYellow;
                      break;
                    case "Canceled":
                      statusColor = ColorUsername;
                      break;
                    case "Rejected":
                      statusColor = Red;
                      break;
                    case "Approved":
                      statusColor = Green;
                      break;
                    default:
                      statusColor = Colors.black;
                  }
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 4),
                                  child: Text(
                                    "${studentLeavesData.modelStudentLeave.data![index].student!.firstname}-${studentLeavesData.modelStudentLeave.data![index].student!.lastname}",
                                    textScaleFactor: 1.3,
                                    style: TextStyle(color: PrimaryColor),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Text("Date : ",
                                        style: TextStyle(color: PrimaryColor)),
                                    Text(
                                      'From ${DateFormat("MMM-dd-yyyy").format(DateTime.parse(studentLeavesData.modelStudentLeave.data![index].start.toString()))} To ${DateFormat("MMM-dd-yyyy").format(DateTime.parse(studentLeavesData.modelStudentLeave.data![index].end.toString()))}',
                                      style: TextStyle(fontSize: 11.sp),
                                    )
                                  ],
                                )
                              ],
                            ),
                            Container(
                                width: 0.8, height: 60, color: statusColor),
                            Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                      "${studentLeavesData.modelStudentLeave.data![index].days}",
                                      textScaleFactor: 1.5),
                                  Text(
                                    studentLeavesData.modelStudentLeave
                                                .data![index].days! >
                                            1
                                        ? "Days"
                                        : "Day",
                                    style: TextStyle(
                                      color: PrimaryColor,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Text("Reason:", style: TextStyle(color: PrimaryColor)),
                        Text(
                            "${studentLeavesData.modelStudentLeave.data![index].reason}"),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "Status: ",
                                style: TextStyle(
                                  color: PrimaryColor,
                                ),
                              ),
                              Text(
                                "${studentLeavesData.modelStudentLeave.data![index].status}",
                                style: TextStyle(
                                  color: statusColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                        studentLeavesData.modelStudentLeave.data![index].status
                                        .toString() ==
                                    "Pending" &&
                                DateTime.parse(studentLeavesData
                                        .modelStudentLeave.data![index].end!)
                                    .isAfter((DateTime.now()))
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () => showConfirmationDialogue(
                                        statusOfLeave: "Rejected",
                                        teacherId: userData.id!,
                                        leaveId: studentLeavesData
                                            .modelStudentLeave.data![index].id!,
                                        text: "Reject"),
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 8),
                                      color: Red,
                                      child: Text(
                                        "REJECT",
                                        style: TextStyle(
                                          color: White,
                                          fontSize: 10.sp,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  InkWell(
                                    onTap: () => showConfirmationDialogue(
                                        teacherId: userData.id!,
                                        leaveId: studentLeavesData
                                            .modelStudentLeave.data![index].id!,
                                        statusOfLeave: "Approved",
                                        text: "Accept"),
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 8),
                                      color: Green,
                                      child: Text(
                                        "ACCEPT",
                                        style: TextStyle(
                                          color: White,
                                          fontSize: 10.sp,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            : SizedBox(),
                        Divider(
                          thickness: 1,
                          color: GreyLight,
                        ),
                      ],
                    ),
                  );
                },
              );
            } else {
              return Center(
                child: Container(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 14),
                      child: Text("No Leaves"),
                    ),
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  showConfirmationDialogue(
      {required int leaveId,
      required String statusOfLeave,
      required int teacherId,
      required text}) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(Strings.confirmation),
        content: Text(
          "Are you sure you want to $text this leave request?",
          textScaleFactor: 0.9,
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("NO"),
          ),
          TextButton(
              onPressed: () {
                widget.studentLeavesNotifier
                    .studentLeaveStatusChange(
                        context: _scaffoldKey.currentState!.context,
                        leaveId: leaveId,
                        teacherId: teacherId,
                        statusOfLeave: statusOfLeave)
                    .then((value) => setState(() {}));
                Navigator.pop(context);
              },
              child: Text("YES")),
        ],
      ),
    ).then((value) => setState(() {}));
  }
}
