import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Helper/permissionHelper.dart';
import 'package:skoolpod/Model/model_messages.dart';
import 'package:skoolpod/Notifier/assignmentNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:skoolpod/Widgets/square_corner_button.dart';
import 'package:skoolpod/Widgets/ui_component.dart';
import 'package:skoolpod/Widgets/underLineEditTextFormField.dart';

class ScreenAddAssignment extends StatefulWidget {
  const ScreenAddAssignment({Key? key,required this.result}) : super(key: key);
  static const String routeName = "addAssignment";
  final Results result;

  @override
  _ScreenAddAssignmentState createState() => _ScreenAddAssignmentState();
}

class _ScreenAddAssignmentState extends State<ScreenAddAssignment> {
  final _classController = TextEditingController();
  final _assignmentTitleController = TextEditingController();
  final _assignmentDescController = TextEditingController();
  Data? data;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String? fileType;
  String? imageToDisplay;
  PlatformFile? file;
  bool isAssignmentAdded = true;

  @override
  void initState() {
    PermissionHelper.takePermission();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: GlobalAppBar(title: Strings.add_assignment,showAction: false,),
            body: ChangeNotifierProvider(
              create: (_)=> AssignmentNotifier(),
              child: Consumer<AssignmentNotifier>(
                builder: (context, assignmentNotifier,_) {
                  return
                    Form(key: _formKey,
                    child: Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              InkWell(onTap: _showClassListDialog,
                                child: IgnorePointer(
                                  child: UnderLineEditTextFormField(textController: _classController,
                                  labelText: "Select Class",validation: assignmentNotifier.validateClass,),
                                ),
                              ),
                              SizedBox(height: 10,),
                              UnderLineEditTextFormField(textController: _assignmentTitleController,
                                labelText: "Assignment title",validation: assignmentNotifier.validateAssignmentTitle,),
                              SizedBox(height: 10,),
                              UnderLineEditTextFormField(textController: _assignmentDescController,
                                labelText: "Description",validation: assignmentNotifier.validateAssignmentDescription,
                              maxLines: 3,),
                              SizedBox(height: 12,),
                              Row(
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      FilePickerResult? result = await FilePicker.platform.pickFiles();

                                      if(result != null) {
                                        PlatformFile file = result.files.first;
                                        setFile(file);
                                        this.file = file;
                                      }
                                    },
                                    child: Container(height: 11.0.h,
                                        child: file!=null && imageToDisplay=="Image"? Image.file(File(file!.path!)) :Image.asset(imageToDisplay??"assets/images/ic_add_file.png",)),
                                  ),
                                  Expanded(child: Container(child: Center(child: Text(Strings.txt_upload_assignment,style: TextStyle(color: PrimaryColor),)))),
                                  SizedBox(width: 10,),
                                ],
                              ),
                              SizedBox(height: 12,),
                              if(!isAssignmentAdded)
                              Text("Add assignment file",style: TextStyle(color: Colors.red),)

                            ],
                          ),
                        ),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child:SquareButton(
                              onPressed:() async {
                                final isValid = _formKey.currentState?.validate();
                                if (!isValid! || file==null) {
                                  setState(() {
                                    isAssignmentAdded = false;
                                  return null;
                                  });
                                } else {
                                  _formKey.currentState?.save();
                                  Map<String, String> param = new Map();
                                  var id=Provider.of<SessionNotifier>(context,listen: false).userData!.id.toString();
                                  param['user_id'] = id;//MultipartFile.fromString(id);
                                  param['conversation_id'] = data!.id.toString();//MultipartFile.fromString(data!.id.toString());
                                  param['title'] = _assignmentTitleController.text+"\n"+_assignmentDescController.text ;//MultipartFile.fromString(_assignmentTitleController.text);
                                  param['type'] = fileType!;//MultipartFile.fromString(fileType!);
                                  var response = await assignmentNotifier.sendAssignment(param, File(file!.path!), context);
                                  if(response!=null && response.status==1) {
                                    Ui.showSuccessToastMessage("Assignment posted successfully!");
                                    Navigator.of(context).pop();
                                  }
                                }
                              },
                              text: Strings.add,
                            )
                        )
                      ],
                    ),
                  );
                }
              ),
            )));

  }
  void _showClassListDialog() async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(Strings.select_class),
            content: Container(
              width: double.maxFinite,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: widget.result.data!.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(widget.result.data![index].name!),
                    onTap: () {
                      data=widget.result.data![index];
                      setState(() {
                        _classController.text =widget.result.data![index].name!;
                      });
                      Navigator.pop(context);
                    },
                  );
                },
              ),
            ),
          );
        });
  }

  void setFile(PlatformFile file){
    String type = file.extension!;
    type = type.toLowerCase();

    print("Type"+type);
    if (type == "doc" || type == "pdf" || type == "docx") {
      setState(() {
        isAssignmentAdded = true;
      imageToDisplay = "assets/images/ic_file_doc.png";
      });
      fileType = Strings.MESSAGE_DOCUMENT;
      //isFileSelected = true
    } else if (type == "jpg" || type == "jpeg" || type == "png") {
      setState(() {
        isAssignmentAdded = true;
        imageToDisplay = "Image";
      });
    fileType = Strings.MESSAGE_IMAGE;
    //isFileSelected = true
    } else if (type == "mp3" || type == "aac" || type == "wav") {
      setState(() {
        isAssignmentAdded = true;
        imageToDisplay = "assets/images/ic_file_audio.png";
      });
    fileType = Strings.MESSAGE_AUDIO;
    //isFileSelected = true
    } else {
    Ui.showToastMessage("Invalid file selection, please choose supported file") ;
    //file = null;
    //isFileSelected = false
    }
  }

  @override
  void dispose() {
    _classController.dispose();
    _assignmentTitleController.dispose();
    _assignmentDescController.dispose();
    super.dispose();
  }
}
