import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Notifier/studentListActivityNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Screen/Teacher/screen_activity_list.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Widgets/network_image_holder.dart';
import 'package:sizer/sizer.dart';

class ScreenStudentListActivity extends StatefulWidget {
  final Map<String, dynamic> arguments;
  final StudentListActivityNotifier studentListActivityNotifier =
      StudentListActivityNotifier();
  ScreenStudentListActivity({Key? key, required this.arguments})
      : super(key: key);
  static const String routeName = "screenStudentListActivity";
  @override
  _ScreenStudentListActivityState createState() =>
      _ScreenStudentListActivityState();
}

class _ScreenStudentListActivityState extends State<ScreenStudentListActivity> {
  @override
  Widget build(BuildContext context) {
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    widget.studentListActivityNotifier.webCallGetStudentListActivity(
        context: context,
        divisionId: widget.arguments["divisionId"],
        standardId: widget.arguments["standardId"],
        teacherId: userData.id!);
    return Scaffold(
      appBar: GlobalAppBar(
        title: widget.arguments["subjectName"],
        showAction: false,
      ),
      body: ChangeNotifierProvider(
        create: (_) => widget.studentListActivityNotifier,
        child: Consumer<StudentListActivityNotifier>(
          builder: (context, model, child) {
            if (model.modelStudentListActivity != null) {
              return ListView.builder(
                itemCount: model.modelStudentListActivity!.data!.length,
                padding: const EdgeInsets.symmetric(vertical: 10),
                physics: BouncingScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      Map<String, dynamic> args = {};
                      args["studentId"] =
                          model.modelStudentListActivity!.data![index].id;
                      args["studentName"] =
                          "${model.modelStudentListActivity!.data![index].firstname ?? "Student"} ${model.modelStudentListActivity!.data![index].lastname ?? "Name"}";
                      Navigator.pushNamed(context, ScreenActivityList.routeName,
                          arguments: args);
                    },
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              height: 62,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 16),
                              child: NetworkImageHolder(
                                  imageUrl: model.modelStudentListActivity!
                                      .data![index].thumb),
                            ),
                            Text(
                              "${model.modelStudentListActivity!.data![index].firstname ?? "Student"} ${model.modelStudentListActivity!.data![index].lastname ?? "Name"}",
                              style: TextStyle(
                                  fontSize: 12.0.sp, color: PrimaryColor),
                            ),
                          ],
                        ),
                        Divider(thickness: 1),
                      ],
                    ),
                  );
                },
              );
            } else {
              return SizedBox();
            }
          },
        ),
      ),
    );
  }
}
