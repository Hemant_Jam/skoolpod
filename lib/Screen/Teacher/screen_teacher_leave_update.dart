import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Notifier/addTeacherLeaveNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Widgets/ui_component.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;

class ScreenTeacherUpdateLeaves extends StatefulWidget {
  final int leaveId;
  final int principalId;
  final String startDate;
  final String endDate;
  final int days;
  final String reason;
  ScreenTeacherUpdateLeaves(
      {Key? key,
      required this.principalId,
      required this.startDate,
      required this.endDate,
      required this.days,
      required this.reason,
      required this.leaveId})
      : super(key: key);
  final AddTeacherLeaveNotifier addLeaveNotifier = AddTeacherLeaveNotifier();
  static const String routeName = "screenTeacherAddLeave";

  @override
  _ScreenTeacherUpdateLeavesState createState() =>
      _ScreenTeacherUpdateLeavesState();
}

class _ScreenTeacherUpdateLeavesState extends State<ScreenTeacherUpdateLeaves> {
  final formKey = new GlobalKey<FormState>();

  TextEditingController selectFromDateController = TextEditingController();
  TextEditingController selectToDateController = TextEditingController();
  TextEditingController selectReasonController = TextEditingController();
  bool showLeave = false;
  DateTime fromDate = DateTime.now();
  bool fromDateSelected = false;
  bool toDateSelected = false;
  DateTime toDate = DateTime.now();
  FocusNode focusNode = FocusNode();
  String hintText = Strings.type_your_reason_here;
  int dayDifference = 0;

  @override
  void initState() {
    super.initState();
    selectFromDateController.text =
        DateFormat("MMM-dd-yyyy").format(DateTime.parse(widget.startDate));
    selectToDateController.text =
        DateFormat("MMM-dd-yyyy").format(DateTime.parse(widget.endDate));
    showLeave = true;
    dayDifference = widget.days;
    selectReasonController.text = widget.reason;
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = Strings.type_your_reason_here;
      }
      setState(() {});
    });
  }

  @override
  void didChangeDependencies() {
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    widget.addLeaveNotifier
        .getPrincipalList(context: context, schoolId: userData.schoolId!)
        .then(
      (value) {
        List dataList = value['data'];
        dataList.forEach(
          (element) {
            if (element["id"] == widget.principalId) {
              widget.addLeaveNotifier.changePrincipalName(
                  pName: "${element["firstname"]} ${element["lastname"]}",
                  pId: widget.principalId);
            }
          },
        );
      },
    );
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;

    return ChangeNotifierProvider(
      create: (_) => widget.addLeaveNotifier,
      child: Consumer<AddTeacherLeaveNotifier>(
        builder: (context, addLeaveNotifier, child) {
          return Scaffold(
            appBar: GlobalAppBar(title: Strings.add_leaves, showAction: false),
            body: Form(
              key: formKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    TextFormField(
                      style: TextStyle(color: PrimaryColor),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return Strings.select_principal;
                        }
                      },
                      onTap: () =>
                          showDialogOfPrincipal(widget.addLeaveNotifier),
                      readOnly: true,
                      controller: addLeaveNotifier.selectPrincipalController,
                      decoration: InputDecoration(
                          hintText: Strings.select_principal,
                          suffixIcon: Icon(Icons.arrow_drop_down_sharp,
                              color: PrimaryColor)),
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            style: TextStyle(color: PrimaryColor),
                            validator: (value) {
                              if (value!.isEmpty) return Strings.select_date;
                            },
                            controller: selectFromDateController,
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.all(10),
                                      content: Container(
                                        height: 30.h,
                                        child: CupertinoDatePicker(
                                          minimumDate: DateTime.now(),
                                          initialDateTime: fromDate
                                                      .difference(
                                                          DateTime.now())
                                                      .inDays ==
                                                  0
                                              ? fromDate.add(Duration(days: 1))
                                              : fromDate,
                                          onDateTimeChanged:
                                              (DateTime newFromDate) {
                                            fromDate = newFromDate;
                                          },
                                          mode: CupertinoDatePickerMode.date,
                                        ),
                                      ),
                                      actions: [
                                        TextButton(
                                            onPressed: () {
                                              dayDifference = toDate
                                                      .difference(fromDate)
                                                      .inDays +
                                                  1;
                                              showLeave = true;
                                              selectFromDateController.value =
                                                  TextEditingValue(
                                                      text: DateFormat(
                                                              "MMM-dd-yyyy")
                                                          .format(fromDate));
                                              fromDateSelected = true;
                                              Navigator.pop(context);
                                            },
                                            child: Text("OK")),
                                        TextButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text("CANCEL"))
                                      ],
                                    );
                                  }).then(
                                (value) => setState(() {}),
                              );
                            },
                            readOnly: true,
                            decoration: InputDecoration(
                                hintText: Strings.from_date,
                                suffixIcon: Icon(Icons.calendar_today_outlined,
                                    color: PrimaryColor)),
                          ),
                        ),
                        SizedBox(width: 10),
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            style: TextStyle(color: PrimaryColor),
                            validator: (value) {
                              if (value!.isEmpty) return Strings.select_date;
                            },
                            controller: selectToDateController,
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      contentPadding: EdgeInsets.all(10),
                                      content: Container(
                                        height: 30.h,
                                        child: CupertinoDatePicker(
                                          minimumDate: DateTime.now(),
                                          initialDateTime: toDate
                                                      .difference(
                                                          DateTime.now())
                                                      .inDays ==
                                                  0
                                              ? toDate.add(Duration(days: 1))
                                              : toDate,
                                          onDateTimeChanged:
                                              (DateTime newToDate) {
                                            toDate = newToDate;
                                          },
                                          mode: CupertinoDatePickerMode.date,
                                        ),
                                      ),
                                      actions: [
                                        TextButton(
                                            onPressed: () {
                                              selectToDateController.value =
                                                  TextEditingValue(
                                                      text: DateFormat(
                                                              "MMM-dd-yyyy")
                                                          .format(toDate));

                                              toDateSelected = true;
                                              dayDifference = toDate
                                                      .difference(fromDate)
                                                      .inDays +
                                                  1;
                                              Navigator.pop(context);
                                            },
                                            child: Text("OK")),
                                        TextButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text("CANCEL"))
                                      ],
                                    );
                                  }).then(
                                (value) => setState(() {}),
                              );
                            },
                            readOnly: true,
                            decoration: InputDecoration(
                              hintText: Strings.to_date,
                              suffixIcon: Icon(Icons.calendar_today_outlined,
                                  color: PrimaryColor),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        Text("Days: ", style: TextStyle(color: PrimaryColor)),
                        dayDifference > 0
                            ? Text(dayDifference.toString())
                            : Text("")
                      ],
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      focusNode: focusNode,
                      validator: (String? value) {
                        if (value!.isEmpty)
                          return Strings.enter_reason_for_leave;
                      },
                      controller: selectReasonController,
                      decoration: InputDecoration(
                          labelText: Strings.type_your_reason_here,
                          hintText: hintText),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            ),
            bottomNavigationBar: InkWell(
              onTap: () async {
                /// apply for leave method
                if (addLeaveNotifier.principalName
                    .isNotEmpty) if (formKey.currentState!.validate()) {
                  await addLeaveNotifier
                      .updateTeacherLeave(
                    principalID: widget.principalId,
                    leaveId: widget.leaveId,
                    context: context,
                    days: dayDifference,
                    startDate:
                        DateFormat("yyyy-MM-dd").format(fromDate).toString(),
                    endDate: DateFormat("yyyy-MM-dd").format(toDate).toString(),
                    teacherId: userData.id!,
                    reason: selectReasonController.text,
                  )
                      .then((bool response) {
                    if (response) {
                      Navigator.pop(context);
                    }
                  });
                } else
                  Ui.showErrorToastMessage(Strings.select_principal);
              },
              child: Container(
                height: 55,
                color: PrimaryColor,
                width: 100.w,
                child: Center(
                  child: Text(
                    "APPLY",
                    style: TextStyle(fontSize: 13.0.sp, color: Colors.white),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void showDialogOfPrincipal(AddTeacherLeaveNotifier addLeaveNotifier) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(
            Strings.select_your_principal,
            textScaleFactor: 1.3,
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          content: widget.addLeaveNotifier.modelAddTeacherLeave.data != null
              ? Container(
                  width: double.maxFinite,
                  child: ListView.builder(
                    itemCount: widget
                        .addLeaveNotifier.modelAddTeacherLeave.data!.length,
                    shrinkWrap: true,
                    itemBuilder: (context, int index) {
                      String principalName =
                          "${widget.addLeaveNotifier.modelAddTeacherLeave.data![index].firstname} ${widget.addLeaveNotifier.modelAddTeacherLeave.data![index].lastname}";
                      return ListTile(
                        onTap: () {
                          widget.addLeaveNotifier.changePrincipalName(
                              pId: widget.addLeaveNotifier.modelAddTeacherLeave
                                  .data![index].id!,
                              pName:
                                  "${widget.addLeaveNotifier.modelAddTeacherLeave.data![index].firstname} ${widget.addLeaveNotifier.modelAddTeacherLeave.data![index].lastname}");
                          Navigator.pop(context);
                        },
                        title: Text(principalName),
                      );
                    },
                  ),
                )
              : CircularProgressIndicator(color: Colors.transparent),
        );
      },
    );
  }
}
