import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_login.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Screen/Teacher/screen_classes.dart';
import 'package:skoolpod/Screen/Teacher/screen_student_activity.dart';
import 'package:skoolpod/Screen/screen_messages.dart';
import 'package:skoolpod/Screen/Teacher/screen_more_option.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Widgets/home_appbar.dart';
import 'package:skoolpod/Widgets/iCardRowItem.dart';
import 'package:skoolpod/Widgets/iCardSingleItem.dart';
import 'package:skoolpod/Widgets/network_image_holder.dart';

class ScreenTeacherHome extends StatelessWidget {
  const ScreenTeacherHome({Key? key}) : super(key: key);
  static const String routeName = "teacherHome";
  @override
  Widget build(BuildContext context) {
    Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    const double commonWidth = 5;
    return SafeArea(
      child: Scaffold(
        appBar: HomeScreenAppBar(userData: userData),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: ListView(
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 14,
                  ),
                  Container(
                      height: 45,
                      width: 45,
                      child: NetworkImageHolder(
                        imageUrl: userData.school?.logo,
                        placeHolder: "assets/images/logo.png",
                      )),
                  SizedBox(
                    width: 13,
                  ),
                  Text(
                    userData.school?.name ?? "",
                    style: TextStyle(
                        fontSize: 13.0.sp,
                        fontFamily: "Baloo",
                        color: Colors.grey[700]),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Container(
                      height: 4.0.h,
                      child: Image.asset("assets/images/ic_check_in.png")),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    Strings.check_in,
                    style: TextStyle(color: Green),
                  ),
                  Spacer(),
                  Text(
                    Strings.check_out,
                    style: TextStyle(color: Red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      height: 4.0.h,
                      child: Image.asset("assets/images/ic_check_out.png")),
                ],
              ),
              const SizedBox(height: commonWidth),
              Row(
                children: [
                  ICardRowItem(
                      title: Strings.message,
                      backgroundImage: "assets/images/dash_bg_message.png",
                      iconImage: "assets/images/ic_dash_messages.png",
                      navigationPage: ScreenMessages.routeName),
                  const SizedBox(width: commonWidth),
                  ICardRowItem(
                    title: Strings.calender,
                    backgroundImage: "assets/images/dash_bg_reminder.png",
                    iconImage: "assets/images/ic_dash_reminder.png",
                  ),
                ],
              ),
              const SizedBox(
                height: commonWidth,
              ),
              ICardSingleItem(
                title: Strings.student_attendance,
                backgroundImage: "assets/images/dash_bg_attendance.png",
                iconImage: "assets/images/ic_dash_attendance.png",
              ),
              const SizedBox(
                height: commonWidth,
              ),
              Row(
                children: [
                  ICardRowItem(
                      title: Strings.classes,
                      backgroundImage: "assets/images/dash_bg_leave.png",
                      iconImage: "assets/images/ic_dash_class.png",
                      navigationPage: ScreenClasses.routeName),
                  const SizedBox(
                    width: commonWidth,
                  ),
                  ICardRowItem(
                    title: Strings.student_activity,
                    backgroundImage: "assets/images/dash_bg_faq.png",
                    iconImage: "assets/images/ic_dash_report.png",
                    navigationPage: ScreenStudentActivity.routeName,
                  ),
                ],
              ),
              const SizedBox(
                height: commonWidth,
              ),
              ICardSingleItem(
                title: Strings.more,
                backgroundImage: "assets/images/dash_bg_class.png",
                iconImage: "assets/images/ic_dash_more_menu.png",
                navigationPage: ScreenMoreOption.routeName,
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
