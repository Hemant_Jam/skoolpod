import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Helper/utils.dart';
import 'package:skoolpod/Model/model_class_detail.dart' as ModelClassDetail;
import 'package:skoolpod/Model/model_classes.dart';
import 'package:skoolpod/Notifier/classDetailNotifier.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Widgets/syncFusion_circular_view.dart';

class ScreenClassDetail extends StatefulWidget {
  ScreenClassDetail({Key? key, this.classData}) : super(key: key);
  final Data? classData;
  static const String routeName = "classDetail";
  ClassDetailNotifier classDetailNotifier = ClassDetailNotifier();

  @override
  _ScreenClassDetailState createState() => _ScreenClassDetailState();
}

class _ScreenClassDetailState extends State<ScreenClassDetail> {
  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;
    widget.classDetailNotifier.webCallGetStudentAttendance(
        context, userData.id!, widget.classData!.id!);

    String? timeStr = "From " +
        Utils.formateDate(widget.classData!.ins!, "HH:mm:ss", "hh:mm a") +
        " to " +
        Utils.formateDate(widget.classData!.out!, "HH:mm:ss", "hh:mm a");
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => Navigator.of(context).pop(),
            iconSize: 13.0.sp,
            icon: Icon(Icons.arrow_back_ios_new_sharp),
            color: White,
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.classData?.lecture?.name ?? "",
                style: TextStyle(color: White),
              ),
              Text(
                timeStr,
                style: TextStyle(color: White, fontSize: 09.0.sp),
              ),
            ],
          ),
          centerTitle: false,
        ),
        body: ChangeNotifierProvider.value(
          value: widget.classDetailNotifier,
          child: Consumer<ClassDetailNotifier>(
            builder: (context, classDetailNotifier, _) {
              List<ModelClassDetail.Data> presentList = [];
              List<ModelClassDetail.Data> absentList = [];
              if (classDetailNotifier.modelClassDetail != null) {
                classDetailNotifier.modelClassDetail!.data!.forEach((element) {
                  if (element.attendance!.datePresent == "1")
                    presentList.add(element);
                  else
                    absentList.add(element);
                });
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      timeStr,
                      style: TextStyle(fontSize: 11.0.sp),
                    ),
                  ),
                  Container(
                    height: 45,
                    width: double.maxFinite,
                    color: PrimaryColor,
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                            "  Total Student (${classDetailNotifier.modelClassDetail?.data?.length.toString() ?? "0"})",
                            style: TextStyle(fontSize: 11.0.sp, color: White))),
                  ),

                  /// TabBar */
                  DefaultTabController(
                    length: 2, // length of tabs
                    initialIndex: 0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Container(
                          child: TabBar(
                            labelColor: PrimaryColor,
                            unselectedLabelColor: Colors.black,
                            labelStyle: TextStyle(fontSize: 11.0.sp),
                            indicatorColor: PrimaryColor,
                            tabs: [
                              Tab(
                                text: Strings.present,
                              ),
                              Tab(
                                text: Strings.absent,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 70.0.h, //height of TabBarView
                          child: TabBarView(
                            children: <Widget>[
                              Attendance(presentList),
                              Attendance(absentList),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

class Attendance extends StatelessWidget {
  Attendance(this.attendanceList);

  final List<ModelClassDetail.Data> attendanceList;
  @override
  Widget build(BuildContext context) {
    double mostUsedFontSized = 10.0.sp;

    /**Attendance List*/
    return ListView.builder(
      itemCount: attendanceList.length,
      padding: EdgeInsets.all(15),
      physics: BouncingScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        int totalLecture = attendanceList[index].attendance?.present ??
            0 + attendanceList[index].attendance!.absent!.toInt();
        double percentage = 0;
        if (totalLecture != 0)
          percentage = ((attendanceList[index].attendance!.present! * 100) /
              totalLecture);

        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 8, bottom: 4),
                  child: Container(
                    height: 62,
                    child: attendanceList[index].image.isNotEmpty
                        ? CachedNetworkImage(
                            imageUrl: ApiServices.BASE_IMAGE_URL +
                                attendanceList[index].image,
                            imageBuilder: (context, imageProvider) => Container(
                              width: 62.0,
                              height: 65.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                          )
                        : Image.asset(
                            "assets/images/ic_student_placeholder.png"),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${attendanceList[index].firstname ?? ""} ${attendanceList[index].lastname ?? ""}",
                        style: TextStyle(
                            fontSize: 11.0.sp,
                            color: PrimaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 2),
                      Text(
                        "Total Attendance ${attendanceList[index].attendance?.present ?? "0"}/$totalLecture",
                        style: TextStyle(fontSize: mostUsedFontSized),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  flex: 4,
                  child: SyncfusionCircularView(
                    value: percentage,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
