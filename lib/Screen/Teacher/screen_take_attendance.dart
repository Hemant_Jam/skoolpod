import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Helper/utils.dart';
import 'package:skoolpod/Model/model_classes.dart';
import 'package:skoolpod/Model/model_student_list.dart' as ModelStudent;
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Notifier/takeAttendanceNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Widgets/square_corner_button.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

List<String> absentList = [];

class ScreenTakeAttendance extends StatefulWidget {
  ScreenTakeAttendance({Key? key, required this.classData}) : super(key: key);
  final Data classData;
  static const String routeName = "takeAttendance";
  TakeAttendanceNotifier takeAttendanceNotifier = TakeAttendanceNotifier();

  @override
  _ScreenTakeAttendanceState createState() => _ScreenTakeAttendanceState();
}

class _ScreenTakeAttendanceState extends State<ScreenTakeAttendance> {
  @override
  void initState() {
    absentList.clear();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserData.Data userData = Provider.of<SessionNotifier>(context).userData!;
    widget.takeAttendanceNotifier
        .webCallGetTeacherAttendance(context, userData.id!);
    if (widget.classData.type.isNotEmpty)
      widget.takeAttendanceNotifier.webCallGetStudentAttendance(
          context, userData.id!, widget.classData.id!);
    else
      widget.takeAttendanceNotifier
          .webCallGetStudentList(context, userData.id!, widget.classData.id!);

    String? timeStr = "From " +
        Utils.formateDate(widget.classData.ins!, "HH:mm:ss", "hh:mm a") +
        " to " +
        Utils.formateDate(widget.classData.out!, "HH:mm:ss", "hh:mm a");
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => Navigator.of(context).pop(),
            iconSize: 13.0.sp,
            icon: Icon(Icons.arrow_back_ios_new_sharp),
            color: White,
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.classData.lecture?.name ?? "",
                style: TextStyle(color: White),
              ),
              Text(
                timeStr,
                style: TextStyle(color: White, fontSize: 09.0.sp),
              ),
            ],
          ),
          centerTitle: false,
        ),
        body: ChangeNotifierProvider.value(
          value: widget.takeAttendanceNotifier,
          child: Consumer<TakeAttendanceNotifier>(
            builder: (context, takeAttendanceNotifier, _) {
              return Stack(
                children: [
                  if (takeAttendanceNotifier.modelStudentList != null)
                    ListView.builder(
                        itemCount: takeAttendanceNotifier
                            .modelStudentList!.data!.length,
                        padding: EdgeInsets.symmetric(vertical: 15),
                        itemBuilder: (BuildContext context, int index) {
                          return AttendanceItem(
                              takeAttendanceNotifier
                                  .modelStudentList!.data![index],
                              widget.classData.type,
                              takeAttendanceNotifier);
                        }),
                  Visibility(
                    visible: takeAttendanceNotifier.isTeacherCheckedIn,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: SquareButton(
                        text: Strings.submit_attendance,
                        onPressed: () {
                          Ui.showConfirmationAlert(
                            context,
                            "Are you sure you want to submit attendance for this class?",
                            () {
                              takeAttendanceNotifier
                                  .webCallSubmitAttendance(
                                      context,
                                      userData.id!,
                                      widget.classData.id!,
                                      absentList)
                                  .then(
                                (commonResp) {
                                  if (commonResp != null &&
                                      commonResp.status == 1) {
                                    absentList.clear();
                                    Ui.showSuccessToastMessage(
                                        Strings.attendance_submit_msg);
                                    Navigator.of(context).pop();
                                  } else if (commonResp != null &&
                                      commonResp.status == 0)
                                    Ui.showSuccessToastMessage(
                                        commonResp.msg ?? "");
                                },
                              );
                            },
                          );
                        },
                      ),
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

class AttendanceItem extends StatefulWidget {
  AttendanceItem(this.student, this.type, this.takeAttendanceNotifier);

  final ModelStudent.Data student;
  final String type;
  final TakeAttendanceNotifier takeAttendanceNotifier;
  @override
  _AttendanceItemState createState() => _AttendanceItemState();
}

class _AttendanceItemState extends State<AttendanceItem> {
  var textValue = 'Present';

  @override
  Widget build(BuildContext context) {
    /**Take Attendance Item*/
    return Container(
      margin: EdgeInsets.only(bottom: 0),
      child: Column(
        children: [
          Row(
            children: [
              const SizedBox(width: 14),
              Padding(
                padding: const EdgeInsets.only(right: 8, bottom: 4),
                child: Container(
                  margin: const EdgeInsets.only(top: 5),
                  height: 65,
                  child: widget.student.image!.isNotEmpty
                      ? CachedNetworkImage(
                          imageUrl: ApiServices.BASE_IMAGE_URL +
                              widget.student.image!,
                          imageBuilder: (context, imageProvider) => Container(
                            width: 62.0,
                            height: 65.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                        )
                      : Image.asset("assets/images/ic_student_placeholder.png"),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                //  flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${widget.student.firstname ?? ""} ${widget.student.lastname ?? ""}",
                      style: TextStyle(
                          fontSize: 11.0.sp,
                          color: PrimaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
              const SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  if (widget.type == "edit" || widget.type.isEmpty)
                    Transform.scale(
                        scale: 1.2,
                        child: Switch(
                          onChanged: toggleSwitch,
                          value: widget.student.isPresent,
                          activeColor: Green,
                          activeTrackColor: PrimaryLight,
                          inactiveThumbColor: Red,
                          inactiveTrackColor: GreyLight,
                        )),
                  Text(
                    '$textValue',
                    style: TextStyle(
                        fontSize: 9.0.sp,
                        color: widget.student.isPresent ? Green : Red),
                  ),
                  const SizedBox(height: 5),
                  if (widget.type == "late")
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "${widget.takeAttendanceNotifier.lateMinutes} mins late",
                          style: TextStyle(fontSize: 9.sp, color: Red),
                        ),
                        InkWell(
                            child: Icon(Icons.cancel, size: 15.sp, color: Red)),
                        InkWell(
                          onTap: () => showDialog(
                              context: context,
                              builder: (_) {
                                return LateAttendanceDialog();
                              }),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 4),
                            decoration: BoxDecoration(
                                color: YellowDash,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    bottomLeft: Radius.circular(8))),
                            child: Center(
                              child: Text(
                                "Mark as Late",
                                style:
                                    TextStyle(color: White, fontSize: 7.0.sp),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                ],
              ),
            ],
          ),
          const Divider(color: GreyLight, thickness: 0.2)
        ],
      ),
    );
  }

  void toggleSwitch(bool value) {
    if (widget.student.isPresent == false) {
      setState(() {
        widget.student.isPresent = true;
        textValue = 'Present';
        absentList.remove(widget.student.id.toString());
      });
    } else {
      setState(() {
        widget.student.isPresent = false;
        textValue = 'Absent';
        absentList.add(widget.student.id.toString());
      });
    }
  }
}

class LateAttendanceDialog extends StatefulWidget {
  const LateAttendanceDialog({Key? key}) : super(key: key);

  @override
  _LateAttendanceDialogState createState() => _LateAttendanceDialogState();
}

class _LateAttendanceDialogState extends State<LateAttendanceDialog> {
  int lateMinutes = 00;
  void decrease(int minutes) {
    if (lateMinutes == 00) {
      lateMinutes = 60;
    }
    lateMinutes += minutes;

    print("this is late minutes ======$lateMinutes");
    setState(() {});
  }

  void increaseLateMinutes(int minutes) {
    if (lateMinutes == 55) {
      lateMinutes = -5;
    }
    lateMinutes += minutes;
    print("this is late minutes ======$lateMinutes");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      actionsPadding: EdgeInsets.zero,
      actions: [
        ButtonBar(
          alignment: MainAxisAlignment.spaceEvenly,
          children: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('CANCEL', style: TextStyle(color: Colors.black)),
            ),
            ElevatedButton(
              onPressed: () {
                if (lateMinutes != 0)
                  Navigator.pop(context);
                else
                  Ui.showErrorToastMessage("select late minutes");
              },
              child: Text('SUBMIT', style: TextStyle(color: White)),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(PrimaryColor),
              ),
            )
          ],
        ),
      ],
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                          onPressed: () {
                            increaseLateMinutes(5);
                          },
                          icon: Icon(Icons.keyboard_arrow_up,
                              color: PrimaryColor)),
                      Text(lateMinutes.toString(), textScaleFactor: 1.4),
                      IconButton(
                          onPressed: () {
                            decrease(-5);
                          },
                          icon: Icon(Icons.keyboard_arrow_down,
                              color: PrimaryColor)),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "mins",
                        style: TextStyle(fontSize: 11.sp, color: PrimaryColor),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
