import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Notifier/studentActivityNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Teacher/screen_student_list_activity.dart';
import 'package:skoolpod/Widgets/appbar.dart';

class ScreenStudentActivity extends StatefulWidget {
  ScreenStudentActivity({Key? key}) : super(key: key);
  static const String routeName = "screenStudentActivity";
  final StudentActivityNotifier studentActivityNotifier =
      StudentActivityNotifier();
  @override
  _ScreenStudentActivityState createState() => _ScreenStudentActivityState();
}

class _ScreenStudentActivityState extends State<ScreenStudentActivity> {
  @override
  Widget build(BuildContext context) {
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    widget.studentActivityNotifier
        .webCallGetTeacherLecture(context: context, teacherId: userData.id!);
    return SafeArea(
      child: ChangeNotifierProvider(
        create: (_) => widget.studentActivityNotifier,
        child: Scaffold(
          appBar: GlobalAppBar(title: Strings.select_class),
          body: Consumer<StudentActivityNotifier>(
            builder: (context, studentActivity, _) {
              if (studentActivity.activityModel.activityData != null) {
                return ListView.builder(
                  itemCount: studentActivity.activityModel.activityData?.length,
                  itemBuilder: (context, int index) => Card(
                    child: ExpansionTile(
                      iconColor: PrimaryColor,
                      title: Text(
                        "Std: ${studentActivity.activityModel.activityData![index].standardName!.name!}",
                        style:
                            TextStyle(color: PrimaryColor, fontSize: 11.0.sp),
                      ),
                      subtitle: Text(
                        "Dive: ${studentActivity.activityModel.activityData![index].standardData![index].name!.name!}",
                        style: TextStyle(fontSize: 11.0.sp),
                      ),
                      children: studentActivity.activityModel
                          .activityData![index].standardData![index].data!
                          .map(
                            (e) => InkWell(
                              onTap: () {
                                Map<String, dynamic> arguments = {};
                                arguments["subjectName"] = e.name!;
                                arguments["divisionId"] = studentActivity
                                    .activityModel
                                    .activityData![index]
                                    .standardData![index]
                                    .name!
                                    .id!;
                                arguments["standardId"] = e.standardId!;
                                Navigator.pushNamed(context,
                                    ScreenStudentListActivity.routeName,
                                    arguments: arguments);
                              },
                              child: SubjectItem(
                                studentCount: studentActivity
                                    .activityModel
                                    .activityData![index]
                                    .standardName!
                                    .studentsCount
                                    .toString(),
                                subjectName: e.name!,
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                );
              } else {
                return Center(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("No data"),
                    ),
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}

class ClassItem extends StatefulWidget {
  ClassItem(
      {Key? key,
      required this.standard,
      required this.division,
      required this.studentCount,
      required this.subjectName})
      : super(key: key);

  final String standard;
  final String division;
  final String studentCount;
  final String subjectName;
  @override
  _ClassItemState createState() => _ClassItemState();
}

class _ClassItemState extends State<ClassItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ExpansionTile(
          iconColor: PrimaryColor,
          title: Text(
            "Std: ${widget.standard}",
            style: TextStyle(color: PrimaryColor, fontSize: 11.0.sp),
          ),
          subtitle: Text(
            "Dive: ${widget.division}",
            style: TextStyle(fontSize: 11.0.sp),
          ),
          children: [
            SubjectItem(
                studentCount: widget.studentCount,
                subjectName: widget.subjectName)
          ]
          // children: widget.divList.data!.map(
          //   (classData) {
          //     return SubjectItem(
          //       stdId: widget.divList.name!.stdID,
          //       divID: widget.divList.name!.id!,
          //       classData: classData,
          //     );
          //   },
          // ).toList(),
          ),
    );
  }
}

class SubjectItem extends StatelessWidget {
  const SubjectItem(
      {Key? key, required this.studentCount, required this.subjectName})
      : super(key: key);
  final String subjectName;
  final String studentCount;

  @override
  Widget build(BuildContext context) {
    double mostUsedFontSized = 10.0.sp;

    ///*/ /**Subject Item Widget*/ /*
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 12),
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8, bottom: 4),
              child: Container(
                  height: 68,
                  child: Image.asset("assets/images/ic_class_board.png")),
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 7,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      subjectName,
                      style: TextStyle(
                          fontSize: 11.0.sp,
                          color: PrimaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 2),
                    //  if (classData.data != null)
                    Text(
                      "Total student($studentCount)",
                      style: TextStyle(
                        fontSize: mostUsedFontSized,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }
}
