import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Teacher/screen_leave_student.dart';
import 'package:skoolpod/Screen/Teacher/screen_leave_teacher.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:sizer/sizer.dart';

class ScreenLeaveManage extends StatefulWidget {
  ScreenLeaveManage({Key? key}) : super(key: key);
  static const String routeName = "screenTeacherLeave";

  @override
  _ScreenLeaveManageState createState() => _ScreenLeaveManageState();
}

class _ScreenLeaveManageState extends State<ScreenLeaveManage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GlobalAppBar(title: Strings.leaves, showAction: false),
      body: DefaultTabController(
        length: 2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              child: TabBar(
                labelColor: PrimaryColor,
                unselectedLabelColor: Colors.black,
                labelStyle: TextStyle(fontSize: 11.0.sp),
                indicatorColor: PrimaryColor,
                tabs: [
                  Tab(text: Strings.you),
                  Tab(text: Strings.student),
                ],
              ),
            ),
            Expanded(
              child: Container(
                height: 70.0.h, //height of TabBarView
                child: TabBarView(
                  children: <Widget>[
                    LeaveItemsTeacher(),
                    LeaveItemsStudent(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
