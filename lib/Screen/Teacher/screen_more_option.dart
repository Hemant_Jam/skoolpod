import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Model/model_login.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Notifier/userProfileNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/screen_login.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Widgets/appbar.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class ScreenMoreOption extends StatelessWidget {
  static const String routeName = "moreOption";
  ScreenMoreOption({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Data userData = Provider.of<SessionNotifier>(context).userData!;
    String imageUrl = userData.image ?? "";
    void logout() {
      Provider.of<SessionNotifier>(context, listen: false).clearSession();
      Navigator.of(context).pushNamedAndRemoveUntil(
          ScreenLogin.routeName, (Route<dynamic> route) => false);
    }

    return SafeArea(
      child: Scaffold(
        appBar: GlobalAppBar(title: Strings.more_),
        body: Column(
          //  mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  child: Column(
                    children: [
                      SizedBox(height: 2.7.h),
                      CircleAvatar(
                        minRadius: 8.0.h,
                        maxRadius: 8.5.h,
                        backgroundImage: imageUrl.isNotEmpty
                            ? Image.network(
                                ApiServices.BASE_IMAGE_URL + imageUrl,
                                fit: BoxFit.contain,
                              ).image
                            : Image.asset(
                                "assets/images/ic_student_placeholder.png",
                              ).image,
                        backgroundColor: Colors.white,
                      ),
                      SizedBox(height: 2.0.h),
                      Text(
                        "${userData.firstname ?? "Teacher"} ${userData.lastname ?? "Name"}",
                        style:
                            TextStyle(fontSize: 13.0.sp, color: PrimaryColor),
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    height: 35,
                    width: 35,
                    margin: const EdgeInsets.only(right: 20, top: 30),
                    decoration: BoxDecoration(
                      border: Border.all(color: PrimaryColor),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Center(
                      child: ImageIcon(
                        Image.asset("assets/images/ic_edit.png").image,
                        color: PrimaryColor,
                        size: 18,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Divider(color: PrimaryColor),
            ListenableProvider(
              create: (_) =>
                  UserProfileNotifier(userData.role ?? Strings.PARENT),
              child: Consumer<UserProfileNotifier>(
                builder: (context, userProfileNotifier, _) {
                  //  print("this is user role------------${userData.role}");
                  return Column(
                    children: userProfileNotifier.listMap.keys.map(
                      (title) {
                        return ListTile(
                          leading: ImageIcon(
                            AssetImage(userProfileNotifier.listMap[title]!),
                            color: PrimaryColor,
                            size: 32.0.sp,
                          ),
                          title: Text(
                            title,
                            style: TextStyle(fontSize: 14.0.sp),
                          ),
                          // onTap: () {
                          //   if (title == Strings.child_profile) {
                          //     Navigator.of(context).pushNamed(
                          //         userProfileNotifier.routeMap[title]!,
                          //         arguments: "Profile");
                          //   }
                          // },
                          onTap: () {
                            if (title == Strings.child_profile) {
                              Navigator.of(context).pushNamed(
                                  userProfileNotifier.routeMap[title]!,
                                  arguments: "Profile");
                            } else
                              Navigator.of(context).pushNamed(
                                  userProfileNotifier.routeMap[title]!,
                                  arguments: "");
                            //   print(
                            //    "this is routemap ${userProfileNotifier.routeMap[title]!}");
                          },
                        );
                      },
                    ).toList(),
                  );
                },
              ),
            ),
            Divider(color: PrimaryColor),
            ListTile(
              leading: ImageIcon(AssetImage("assets/images/ic_dash_logout.png"),
                  color: PrimaryColor, size: 32.0.sp),
              title: Text(
                Strings.logout,
                style: TextStyle(fontSize: 14.0.sp),
              ),
              onTap: () => Ui.showConfirmationAlert(
                  context, Strings.sure_to_logout, logout),
            ),
            // OutlinedButton(
            //     onPressed: () => Navigator.of(context)
            //         .pushNamed(ScreenChildList.routeName, arguments: ""),
            //     child: Text("child leave"))
          ],
        ),
        // floatingActionButton: FloatingActionButton(
        //     child: Text(Strings.leaves),
        //     onPressed: () =>
        //         Navigator.of(context).pushNamed(ScreenLeaveManage.routeName)),
      ),
    );
  }
}
