import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Helper/utils.dart';
import 'package:skoolpod/Model/model_classes.dart';
import 'package:skoolpod/Notifier/classListNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Teacher/screen_take_attendance.dart';
import 'package:skoolpod/Screen/Teacher/screen_class_detail.dart';
import 'package:skoolpod/Widgets/appbar.dart';

class ScreenClasses extends StatelessWidget {
  ScreenClasses({Key? key}) : super(key: key);
  static const String routeName = "classes";

  @override
  Widget build(BuildContext context) {
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;

    return SafeArea(
        child: ChangeNotifierProvider(
      create: (_) => ClassesNotifier(context, userData.id!),
      child: Scaffold(
          appBar: GlobalAppBar(
            title: Strings.class_list,
          ),
          body:
              Consumer<ClassesNotifier>(builder: (context, classesNotifier, _) {
            List<Data> data = [];
            if (classesNotifier.modelClasses != null)
              data = classesNotifier.modelClasses!.data ?? [];
            return Column(
              children: [
                Container(
                  height: 5.0.h,
                  width: double.maxFinite,
                  color: GreyLight,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "  Classes Today",
                        style: TextStyle(fontSize: 12.0.sp, color: White),
                      )),
                ),
                Expanded(
                  child: Container(
                    child: data.isNotEmpty
                        ? ListView.builder(
                            itemCount: data.length,
                            padding: EdgeInsets.all(15),
                            physics: BouncingScrollPhysics(),
                            itemBuilder: (BuildContext context, int index) {
                              return ClassItem(data[index], classesNotifier);
                            })
                        : Container(
                            child: Center(child: Text("No Classes found"))),
                  ),
                ),
              ],
            );
          })),
    ));
  }
}

class ClassItem extends StatelessWidget {
  ClassItem(this.data, this.classesNotifier);
  final Data data;
  final ClassesNotifier classesNotifier;
  @override
  Widget build(BuildContext context) {
    double mostUsedFontSized = 10.0.sp;
    String lectureName = data.lecture?.name ?? "";
    String timeStr = "From " +
        Utils.formateDate(data.ins!, "HH:mm:ss", "hh:mm a") +
        " to " +
        Utils.formateDate(data.out!, "HH:mm:ss", "hh:mm a");

    /**Message Item*/
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8, bottom: 4),
              child: Container(
                  height: 68,
                  child: Image.asset(
                    "assets/images/ic_class_board.png",
                  )),
            ),
            const SizedBox(width: 10),
            Expanded(
              flex: 7,
              child: GestureDetector(
                onTap: () {
                  data.role = Strings.TEACHER;
                  Navigator.of(context)
                      .pushNamed(ScreenClassDetail.routeName, arguments: data)
                      .then((value) {
                    classesNotifier.webCallGetTeacherClasses();
                  });
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      lectureName +
                          " " +
                          "(" +
                          data.standard!.name! +
                          " - " +
                          data.division!.name! +
                          ")" //"Science and Technology (6th)"
                      ,
                      style: TextStyle(
                          fontSize: 11.0.sp,
                          color: PrimaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 2),
                    Text(
                      timeStr,
                      style: TextStyle(fontSize: mostUsedFontSized),
                    ),
                    const SizedBox(height: 2),
                    Text(
                      "Total student (${data.total!})",
                      style: TextStyle(fontSize: mostUsedFontSized),
                    ),
                    if (data.lectureAttendance != null)
                      Row(
                        children: [
                          Container(
                              height: 18,
                              margin: EdgeInsets.all(5),
                              child: Image.asset("assets/images/checked.png")),
                          Text(
                            data.lectureAttendance?.present.toString() ?? "0",
                            style: TextStyle(
                                fontSize: mostUsedFontSized, color: Green),
                          ),
                          const SizedBox(width: 18),
                          Container(
                              height: 18,
                              margin: EdgeInsets.all(5),
                              child: Image.asset("assets/images/cancel.png")),
                          Text(
                            data.lectureAttendance?.absent.toString() ?? "0",
                            style: TextStyle(
                                fontSize: mostUsedFontSized, color: Red),
                          ),
                        ],
                      ),
                  ],
                ),
              ),
            ),
            const SizedBox(width: 10),
            Container(width: 0.8, height: 60, color: YellowDash),
            const SizedBox(width: 5),
            Expanded(
                flex: 4,
                child: GestureDetector(
                  onTap: () {
                    if (data.lectureAttendance != null)
                      _showDialogForAttendance(context);
                    else
                      Navigator.of(context)
                          .pushNamed(ScreenTakeAttendance.routeName,
                              arguments: data)
                          .then((value) {
                        classesNotifier.webCallGetTeacherClasses();
                      });
                  },
                  child: Text(
                    data.lectureAttendance == null
                        ? Strings.take_attendance
                        : Strings.edit_attendance,
                    style: TextStyle(
                      color: PrimaryColor,
                      fontSize: mostUsedFontSized,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  void _showDialogForAttendance(BuildContext context) async {
    List<String> option = ['Take attendance again', "Submit latemark"];
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Container(
            width: double.maxFinite,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: option.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(
                    option[index],
                    style: TextStyle(fontSize: 15.0.sp),
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                    if (index == 0)
                      data.type = "edit";
                    else
                      data.type = "late";
                    Navigator.of(context)
                        .pushNamed(ScreenTakeAttendance.routeName,
                            arguments: data)
                        .then((value) {
                      classesNotifier.webCallGetTeacherClasses();
                    });
                  },
                );
              },
            ),
          ),
        );
      },
    );
  }
}
