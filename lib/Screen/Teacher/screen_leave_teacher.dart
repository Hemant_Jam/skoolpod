import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:skoolpod/Notifier/sessionNotifier.dart';
import 'package:skoolpod/Notifier/teacherLeavesNotifier.dart';
import 'package:skoolpod/Resources/colorProperties.dart';
import 'package:sizer/sizer.dart';
import 'package:skoolpod/Model/model_login.dart' as UserData;
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Teacher/screen_teacher_add_leave.dart';
import 'package:skoolpod/Screen/Teacher/screen_teacher_leave_update.dart';

class LeaveItemsTeacher extends StatefulWidget {
  final TeacherLeavesNotifier teacherLeavesNotifier = TeacherLeavesNotifier();
  LeaveItemsTeacher({
    Key? key,
  }) : super(key: key);
  @override
  _LeaveItemsTeacherState createState() => _LeaveItemsTeacherState();
}

class _LeaveItemsTeacherState extends State<LeaveItemsTeacher>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    UserData.Data userData =
        Provider.of<SessionNotifier>(context, listen: false).userData!;
    widget.teacherLeavesNotifier
        .webCallGetTeacherLeaves(context: context, userId: userData.id!);
    return ChangeNotifierProvider(
      create: (_) => widget.teacherLeavesNotifier,
      child: Consumer<TeacherLeavesNotifier>(
        builder: (context, teacherLeavesData, child) {
          if (teacherLeavesData.modelTeacherLeave.data != null) {
            return Scaffold(
              body: SingleChildScrollView(
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: teacherLeavesData.modelTeacherLeave.data!.length,
                  itemBuilder: (context, int index) {
                    Color statusColor = Colors.black;
                    switch (teacherLeavesData
                        .modelTeacherLeave.data![index].status
                        .toString()) {
                      case "Pending":
                        statusColor = ColorYellow;
                        break;
                      case "Canceled":
                        statusColor = ColorUsername;
                        break;
                      case "Approved":
                        statusColor = Green;
                        break;
                      case "Rejected":
                        statusColor = Red;
                        break;
                      default:
                        statusColor = Colors.black;
                    }
                    return Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 4),
                                        child: Text(
                                          "Date : ",
                                          style: TextStyle(color: PrimaryColor),
                                        ),
                                      ),
                                      Text(
                                        'From ${DateFormat("MMM-dd-yyyy").format(DateTime.parse(teacherLeavesData.modelTeacherLeave.data![index].start.toString()))} To ${DateFormat("MMM-dd-yyyy").format(
                                          DateTime.parse(
                                            teacherLeavesData.modelTeacherLeave
                                                .data![index].end
                                                .toString(),
                                          ),
                                        )}',
                                        style: TextStyle(fontSize: 11.sp),
                                      )
                                    ],
                                  ),
                                  Text(
                                    "Reason:",
                                    style: TextStyle(color: PrimaryColor),
                                  ),
                                  Text(
                                      "${teacherLeavesData.modelTeacherLeave.data![index].reason}"),
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text(
                                          "Status:",
                                          style: TextStyle(color: PrimaryColor),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 4),
                                          child: Text(
                                            "${teacherLeavesData.modelTeacherLeave.data![index].status}",
                                            style:
                                                TextStyle(color: statusColor),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                width: 1.5,
                                height: 60,
                                color: statusColor,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                      "${teacherLeavesData.modelTeacherLeave.data![index].days!.floor()}",
                                      textScaleFactor: 1.5),
                                  Text(
                                    teacherLeavesData.modelTeacherLeave
                                                .data![index].days! >
                                            1
                                        ? "Days"
                                        : "Day",
                                    style: TextStyle(color: PrimaryColor),
                                  ),
                                ],
                              ),
                              teacherLeavesData.modelTeacherLeave.data![index]
                                              .status
                                              .toString() ==
                                          "Pending" &&
                                      DateTime.parse(teacherLeavesData
                                              .modelTeacherLeave
                                              .data![index]
                                              .end!)
                                          .isAfter((DateTime.now()))
                                  ? Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        InkWell(
                                          onTap: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) =>
                                                  ScreenTeacherUpdateLeaves(
                                                leaveId: teacherLeavesData
                                                    .modelTeacherLeave
                                                    .data![index]
                                                    .id!
                                                    .toInt(),
                                                principalId: teacherLeavesData
                                                    .modelTeacherLeave
                                                    .data![index]
                                                    .principalId!,
                                                days: teacherLeavesData
                                                    .modelTeacherLeave
                                                    .data![index]
                                                    .days!
                                                    .toInt(),
                                                endDate: teacherLeavesData
                                                    .modelTeacherLeave
                                                    .data![index]
                                                    .end
                                                    .toString(),
                                                startDate: teacherLeavesData
                                                    .modelTeacherLeave
                                                    .data![index]
                                                    .start
                                                    .toString(),
                                                reason: teacherLeavesData
                                                    .modelTeacherLeave
                                                    .data![index]
                                                    .reason
                                                    .toString(),
                                              ),
                                            ),
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.all(2),
                                            color: YellowDash,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: Icon(Icons.edit,
                                                  color: White),
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () => showDialog(
                                              barrierDismissible: false,
                                              context: context,
                                              builder: (_) => AlertDialog(
                                                    actionsPadding:
                                                        EdgeInsets.zero,
                                                    title: Text(
                                                        Strings.confirmation),
                                                    content: Text(
                                                      Strings.are_you_sure,
                                                      textScaleFactor: 0.8,
                                                    ),
                                                    actions: [
                                                      TextButton(
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Text(
                                                          "No",
                                                          style: TextStyle(
                                                              color:
                                                                  PrimaryColor),
                                                        ),
                                                      ),
                                                      TextButton(
                                                        onPressed: () {
                                                          teacherLeavesData
                                                              .deleteTeacherLeave(
                                                                  leaveId: teacherLeavesData
                                                                      .modelTeacherLeave
                                                                      .data![
                                                                          index]
                                                                      .id!,
                                                                  teacherId:
                                                                      userData
                                                                          .id!,
                                                                  context:
                                                                      context)
                                                              .then((value) =>
                                                                  setState(
                                                                      () {}));
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: Text(
                                                          "Yes",
                                                          style: TextStyle(
                                                              color:
                                                                  PrimaryColor),
                                                        ),
                                                      )
                                                    ],
                                                  )).then((value) {
                                            setState(() {});
                                          }),
                                          child: Container(
                                            margin: EdgeInsets.all(2),
                                            color: Red,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: Icon(Icons.delete,
                                                  color: White),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : SizedBox()
                            ],
                          ),
                          Divider(thickness: 1, color: GreyLight),
                        ],
                      ),
                    );
                  },
                ),
              ),
              floatingActionButton: FloatingActionButton(
                backgroundColor: PrimaryColor,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => ScreenTeacherAddLeaves(),
                    ),
                  );
                },
                child: Icon(
                  Icons.add,
                  color: White,
                ),
              ),
            );
          } else {
            return Scaffold(
              floatingActionButton: FloatingActionButton(
                backgroundColor: PrimaryColor,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => ScreenTeacherAddLeaves(),
                    ),
                  );
                },
                child: Icon(
                  Icons.add,
                  color: White,
                ),
              ),
              body: Center(
                child: Container(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 14),
                      child: Text("No Leaves"),
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
