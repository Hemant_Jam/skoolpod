import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_login.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Resources/strings.dart';

class AuthNotifier extends ChangeNotifier{
  ModelLogin? modelLogin;
  Future<ModelLogin?> makeLogin(BuildContext context,GlobalKey<FormState> formKey,TextEditingController emailCon,TextEditingController passwordCon) async {
    final isValid = formKey.currentState?.validate();
    if (!isValid!) {
      return null;
    } else {
      formKey.currentState?.save();
      Map<String, String> param = new Map();
      param["email"] = emailCon.text.trim();
      param["password"] = passwordCon.text.trim();
      var response = await WebCall.requestPostApiCall(context,url:ApiServices.LOGIN,body: param );
        if(response!=null && response["status"]==1){
          return ModelLogin.fromJson(response);
        }
    }
  }
  String? validateEmail(String? value) {
    if (value == null || value.length<1) {
      return Strings.email_required;
    }
    else if(RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
      return null;
    }
    else {
      return Strings.email_not_valid;
    }
  }
  String? validatePassword(String? value) {
    if (value==null || value.isEmpty) {
      return Strings.password_required;
    }
    else if(value.length<6) {
      return Strings.password_not_valid;
    }
    return null;
  }
}