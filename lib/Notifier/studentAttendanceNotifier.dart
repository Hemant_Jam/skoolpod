import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_class_detail_for_admin.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class StudentAttendanceNotifier extends ChangeNotifier {
  StudentAttendanceNotifier();
  ModelClassDetailForAdmin? modelClassDetailForAdmin;

  DateTime currentTime = DateTime.now();
  int monthCount = 1;

  String startDate = '2021-06-01';
  String endDate = '2021-06-30';
  DateTime titleDate = DateTime.now();
  //
  final List<String> monthList = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  Future<void> webCallGetStudentAttendance({
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) async {
    Map<String, String> param = new Map();
    param["principal_id"] = userId.toString();
    param["standard_id"] = standardId.toString();
    param["division_id"] = divisionId.toString();
    param["lecture_id"] = lectureId.toString();
    param["from"] = startDate;
    param["to"] = endDate;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_CLASS_ATTENDANCE, body: param);
    if (response != null && response["status"] == 1) {
      modelClassDetailForAdmin = ModelClassDetailForAdmin.fromJson(response);

      notifyListeners();
    }
  }
}
