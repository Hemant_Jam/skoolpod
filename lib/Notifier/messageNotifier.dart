import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_messages.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:socket_io_client/socket_io_client.dart';

class MessageNotifier extends ChangeNotifier{
  ModelMessages? modelMessages;
  Socket? socket;
  int messageId = 0;

  MessageNotifier(String userId,BuildContext context){
    getMessages(userId, context);
    connectSocket(userId);
  }
  Future<void> getMessages(String userId,BuildContext context) async {
    Map<String,dynamic> param={"user_id":userId};
    var response = await WebCall.requestPostApiCall(context,url:ApiServices.GET_CHAT,body: param );
    if(response!=null && response["status"]==1){
      modelMessages =ModelMessages.fromJson(response);
      notifyListeners();
    }
  }

  void connectSocket(String userId){
    socket = io("http://15.206.197.138:8111",
        OptionBuilder()
            .setTransports(['websocket'])
        //.disableAutoConnect()  // disable auto-connection
            .build()
    );
    socket!.onConnect((data) => print("on Connect: "+data.toString()));
    socket!.onError((data) => print("on Error: "+data.toString()));
    if(socket!.disconnected)
      socket!.connect();
    socket!.on(ApiServices.EVENT_GET_NEW_MESSAGE + userId, onGetNewMessage);
  }

  onGetNewMessage(data) {
    print("onGetNewMessage  data: "+data.toString());
    if(data!=null){
      Data messageData = Data.fromJson(jsonDecode(data));
      if(messageId!= messageData.id) {
        modelMessages!.results!.data!.insert(0, messageData);
        messageId = messageData.id!;
        notifyListeners();
      }
    }
  }

  @override
  void dispose() {
    if(socket!=null)
      socket!.disconnect();
    super.dispose();
  }
}