import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:skoolpod/Model/model_student_attendance_yearly.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class StudentAttendanceYearlyNotifier extends ChangeNotifier {
  StudentAttendanceYearlyNotifier();
  ModelStudentAttendanceYearly? modelStudentAttendanceYearly;

  DateTime currentTime = DateTime.now();

  String startDate = '2021-01-01';
  String endDate = '2021-12-31';
  DateTime titleDate = DateTime.now();

  void getCurrentYear() {
    DateTime _startDate = DateTime(currentTime.year, 6, 1);
    DateTime _endDate = DateTime(currentTime.year + 1, 6, 0);
    titleDate = _startDate;

    String formattedStartDate = DateFormat('yyyy-MM-dd').format(_startDate);
    String formattedEndDate = DateFormat('yyyy-MM-dd').format(_endDate);

    startDate = formattedStartDate;
    endDate = formattedEndDate;

    notifyListeners();
  }

  Future<void> webCallGetStudentAttendanceYearly({
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) async {
    Map<String, String> param = new Map();
    param["principal_id"] = userId.toString();
    param["standard_id"] = standardId.toString();
    param["division_id"] = divisionId.toString();
    param["lecture_id"] = lectureId.toString();
    param["from"] = startDate;
    param["to"] = endDate;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_CLASS_ATTENDANCE, body: param);
    if (response != null && response["status"] == 1) {
      modelStudentAttendanceYearly =
          ModelStudentAttendanceYearly.fromJson(response);

      notifyListeners();
    }
  }
}
