import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_teacher_leave.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class TeacherLeavesNotifier extends ChangeNotifier {
  static const String other = 'Other';
  ModelTeacherLeave modelTeacherLeave = ModelTeacherLeave();

  Future<void> webCallGetTeacherLeaves(
      {required BuildContext context, required int userId}) async {
    Map<String, String> param = Map();
    param["teacher_id"] = userId.toString();
    param["type"] = other;
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.BASE_URL + ApiServices.GET_TEACHER_LEAVE, body: param);
    if (response != null && response["status"] == 1) {
      modelTeacherLeave = ModelTeacherLeave.fromJson(response);
      notifyListeners();
    }
  }

  Future<bool> deleteTeacherLeave(
      {required int leaveId,
      required int teacherId,
      required BuildContext context}) async {
    Map<String, String> param = Map();
    param["leave_id"] = leaveId.toString();
    param["teacher_id"] = teacherId.toString();
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.REMOVE_TEACHER_LEAVE, body: param);
    if (response != null && response["status"] == 1) {
      Ui.showSuccessToastMessage("Leave removed successfully");
      notifyListeners();
      return true;
    } else
      return false;
  }
}
