import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:skoolpod/Model/model_student_attendance_daily.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class StudentAttendanceDailyNotifier extends ChangeNotifier {
  StudentAttendanceDailyNotifier();
  ModelStudentAttendanceDaily? modelStudentAttendanceDaily;
  DateTime titleDate = DateTime.now();
  String apiDate = '2021-06-1';
  int day = 1;
  final List<String> monthList = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  pickDate({
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        contentPadding: EdgeInsets.all(4),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
                Future.delayed(Duration(milliseconds: 400));
              },
              child: Text('OK')),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('CANCEL')),
        ],
        content: SizedBox(
          height: MediaQuery.of(context).size.height / 4,
          child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            initialDateTime: DateTime.parse(apiDate),
            onDateTimeChanged: (DateTime newDateTime) {
              titleDate = newDateTime;
              String formattedStartDate =
                  DateFormat('yyyy-MM-dd').format(newDateTime);
              apiDate = formattedStartDate;
            },
          ),
        ),
      ),
    ).then((value) => webCallGetStudentAttendanceDaily(
        context: context,
        userId: userId,
        standardId: standardId,
        divisionId: divisionId,
        lectureId: lectureId));
    notifyListeners();
  }

  void nextDate({
    required int day,
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) {
    DateTime currentTime = DateTime.now();
    DateTime currentDate = DateTime(currentTime.year, currentTime.month, day);
    titleDate = currentDate;
    String formattedStartDate = DateFormat('yyyy-MM-dd').format(currentDate);
    apiDate = formattedStartDate;
    webCallGetStudentAttendanceDaily(
        context: context,
        userId: userId,
        standardId: standardId,
        divisionId: divisionId,
        lectureId: lectureId);
    notifyListeners();
  }

  void previousDate({
    required int day,
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) {
    DateTime currentTime = DateTime.now();
    DateTime currentDate = DateTime(currentTime.year, currentTime.month, day);
    titleDate = currentDate;
    String formattedStartDate = DateFormat('yyyy-MM-dd').format(currentDate);
    apiDate = formattedStartDate;
    webCallGetStudentAttendanceDaily(
        context: context,
        userId: userId,
        standardId: standardId,
        divisionId: divisionId,
        lectureId: lectureId);
    notifyListeners();
  }

  void getCurrentDate() {
    DateTime currentDate = DateTime.now();
    day = currentDate.day;
    String formattedStartDate = DateFormat('yyyy-MM-dd').format(currentDate);
    titleDate = currentDate;
    apiDate = formattedStartDate;
    notifyListeners();
  }

  Future<void> webCallGetStudentAttendanceDaily({
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) async {
    Map<String, String> param = new Map();
    param["principal_id"] = userId.toString();
    param["standard_id"] = standardId.toString();
    param["division_id"] = divisionId.toString();
    param["lecture_id"] = lectureId.toString();
    param["from"] = apiDate;
    param["to"] = apiDate;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_CLASS_ATTENDANCE, body: param);
    if (response != null && response["status"] == 1) {
      modelStudentAttendanceDaily =
          ModelStudentAttendanceDaily.fromJson(response);

      notifyListeners();
    }
  }
}
