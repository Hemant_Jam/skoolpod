import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_student_leave.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class StudentLeavesNotifier extends ChangeNotifier {
  static const String getAll = 'GetAll';
  ModelStudentLeave modelStudentLeave = ModelStudentLeave();

  Future<void> webCallGetStudentLeaves(
      {required BuildContext context, required int userId}) async {
    Map<String, String> param = Map();

    param["teacher_id"] = userId.toString();
    param["type"] = getAll;
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.STUDENT_LEAVE_MANAGE, body: param);
    if (response != null && response["status"] == 1) {
      modelStudentLeave = ModelStudentLeave.fromJson(response);
      notifyListeners();
    }
  }

  Future<void> studentLeaveStatusChange(
      {required BuildContext context,
      required int leaveId,
      required String statusOfLeave,
      required int teacherId}) async {
    Map<String, dynamic> parameter = Map();
    parameter["leave_id"] = leaveId;
    parameter["type"] = "Update";
    parameter["status"] = statusOfLeave; //"Approved"/"Rejected"
    parameter["teacher_id"] = teacherId;
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.STUDENT_LEAVE_MANAGE, body: parameter);
    try {
      if (response != null && response["status"] == 1) {
        notifyListeners();
      } else {
        Ui.showSuccessToastMessage("something wrong");
      }
    } catch (e) {
      Ui.showErrorToastMessage(e.toString());
    }
  }
}
