import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_student_list_activity.dart';
import 'package:skoolpod/Service/webCall.dart';

class StudentListActivityNotifier extends ChangeNotifier {
  ModelStudentListActivity? modelStudentListActivity;
  Future<void> webCallGetStudentListActivity({
    required BuildContext context,
    required int divisionId,
    required int standardId,
    required int teacherId,
  }) async {
    Map<String, dynamic> param = {};
    param["division_id"] = divisionId;
    param["standard_id"] = standardId;
    param["teacher_id"] = teacherId;
    var res = await WebCall.requestPostApiCall(context,
        body: param, url: "get-student-list", isLoading: false);
    if (res["status"] == 1) {
      modelStudentListActivity = ModelStudentListActivity.fromJson(res);
      notifyListeners();
    }
  }
}
