import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:skoolpod/Model/model_class_detail.dart';
import 'package:skoolpod/Model/model_class_detail_for_admin.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class ClassDetailNotifier extends ChangeNotifier {
  ModelClassDetail? modelClassDetail;
  ModelClassDetailForAdmin? modelClassDetailForAdmin;

  ClassDetailNotifier() {
    //webCallGetStudentAttendance(context, userId,scheduleId);
  }

  Future<void> webCallGetStudentAttendance(
      BuildContext context, int userId, int scheduleId) async {
    final df = new DateFormat(Strings.FORMAT_SIMPLE);
    Map<String, String> param = new Map();
    param["date"] = df.format(DateTime.now());
    param["teacher_id"] = userId.toString();
    param["schedule_id"] = scheduleId.toString();
    param["type"] = "lecture";
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_CLASS_STUDENT_ATTENDANCE,
        body: param,
        isLoading: false);
    if (response != null && response["status"] == 1) {
      modelClassDetail = ModelClassDetail.fromJson(response);
      notifyListeners();
    }
  }

  Future<void> webCallGetStudentAttendanceForAdmin(BuildContext context,
      int userId, int standardId, int divisionId, int lectureId) async {
    final df = new DateFormat(Strings.FORMAT_SIMPLE);
    Map<String, String> param = new Map();
    param["principal_id"] = userId.toString();
    param["standard_id"] = standardId.toString();
    param["division_id"] = divisionId.toString();
    param["lecture_id"] = lectureId.toString();
    param["from"] = df.format(DateTime.now());
    param["to"] = df.format(DateTime.now());

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_CLASS_ATTENDANCE, body: param);
    if (response != null && response["status"] == 1) {
      modelClassDetailForAdmin = ModelClassDetailForAdmin.fromJson(response);
      notifyListeners();
    }
  }
}
