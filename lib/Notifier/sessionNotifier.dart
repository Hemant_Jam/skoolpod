import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:skoolpod/Helper/sharedPreference.dart';
import 'package:skoolpod/Model/model_login.dart';
import 'package:skoolpod/Resources/strings.dart';

class SessionNotifier extends ChangeNotifier{
  Data? userData;
  String? token;
  bool? isLogin;

  SessionNotifier() {
    getPreferenceData();
  }

  Future<Data?> getPreferenceData() async {
    isLogin = await SharedPref.checkString(Strings.APP_TOKEN);
    if(isLogin!) {
      var data = await SharedPref.getItem(Strings.USER_DATA, String);
      userData = Data.fromJson(json.decode(data));
      token = await SharedPref.getItem(Strings.APP_TOKEN, String);
      return userData;
    }
  }
  setPreferenceData(ModelLogin modelLogin) async {
    userData=modelLogin.data;
    token= modelLogin.data?.appToken;
    isLogin=true;

    await SharedPref.setItem(Strings.USER_DATA, json.encode(modelLogin.data?.toJson()));
    await SharedPref.setItem(Strings.APP_TOKEN, modelLogin.data?.appToken);
    await SharedPref.setItem(Strings.IS_LOGGED_IN, true);
  }
  clearSession(){
    userData=null;
    token= null;
    isLogin=false;
    SharedPref.clear();
  }
}