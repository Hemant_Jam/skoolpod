import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_standerd_devision.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class AdminClassDivisionNotifier extends ChangeNotifier{
  List<DivData> divList =[];
  ModelStandardDivision? modelStandardDivision;
  final BuildContext context;

  AdminClassDivisionNotifier(this.context, int userId, int schoolId) {
    webCallStandardDivision(userId,schoolId);
  }

  Future<void> webCallStandardDivision(int userId, int schoolId) async {
    Map<String, String> param = new Map();
    param["principal_id"] = userId.toString();
    param["school_id"] = schoolId.toString();
    var response = await WebCall.requestPostApiCall(context,url:ApiServices.GET_STANDARD_DIVISION,body: param ,isLoading: false);
    if(response!=null && response["status"]==1){
      modelStandardDivision = ModelStandardDivision.fromJson(response);
      for (var i=0; i< modelStandardDivision!.data!.length;i++) {
        for (var j=0;  j< modelStandardDivision!.data![i].data!.length ; j++) {
          modelStandardDivision!.data![i].data![j].name!.stdName = modelStandardDivision!.data![i].name?.name??"";
          modelStandardDivision!.data![i].data![j].name!.stdID = modelStandardDivision!.data![i].name?.id??0;
          divList.add(modelStandardDivision!.data![i].data![j]);
        }
      }
      notifyListeners();
    }
  }
}