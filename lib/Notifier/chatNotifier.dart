import 'dart:convert';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:skoolpod/Model/model_chat.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Widgets/ui_component.dart';
import 'package:socket_io_client/socket_io_client.dart';

class ChatNotifier extends ChangeNotifier{
  Data? data;
  BuildContext context;
  Socket? socket;
  int messageId = 0;
  Map<String,String>? userParameter;

  ChatNotifier(this.context,String url,Map<String,dynamic> param){
    connectSocket(param["conversation_id"].toString());
    getConversation(url, param);
  }

  Future<void>? getConversation(String url,Map<String,dynamic> param) async {

    var response = await WebCall.requestPostApiCall(context,url:url,body: param);
    if(response!=null && response["status"]==1){
      data =ModelChat.fromJson(response).data;
      notifyListeners();
      if(socket!=null){
        userParameter = {
          "user_id":param["user_id"].toString(),
          "conversation_id":param["conversation_id"].toString(),
        };
        readMessages(userParameter!);
      }
    }
  }

  void connectSocket(String conversationId){
    socket = io("http://15.206.197.138:8111",
        OptionBuilder()
            .setTransports(['websocket'])
            //.disableAutoConnect()  // disable auto-connection
            .build()
    );
    socket!.onConnect((data) => print("on Connect: "+data.toString()));
    socket!.onError((data) => print("on Error: "+data.toString()));
    if(socket!.disconnected)
      socket!.connect();
    socket!.on(ApiServices.EVENT_GET_MESSAGE + conversationId, onGetMessage);
  }

  Future<bool> sendMessage(Map<String,String> messageData)async{
    if(!await checkConnectivity())
    return false;
    if(socket!.connected){
      print("sendMessage  data: "+JsonEncoder().convert(messageData));
      socket!.on(ApiServices.EVENT_SEND_MESSAGE, (data) => print("onSend "+data.toString()));
      socket!.emit(ApiServices.EVENT_SEND_MESSAGE,messageData);
      return true;
    }
    return false;
  }
  void readMessages(Map<String,String> userParameter){
    if(socket!.connected){
      print("readMessage data: "+JsonEncoder().convert(userParameter));
      socket!.emit(ApiServices.EVENT_READ_MESSAGE,userParameter);
      socket!.on(ApiServices.EVENT_READ_MESSAGE, (data) => print("onReadMessage "+data.toString()));
    }
  }

    @override
    void dispose() {
      if(socket!=null)
      socket!.disconnect();
      super.dispose();
    }

  onGetMessage(data) {
    print("onGetMessage  data: "+data.toString());
    if(data!=null){
      ChatData chatData = ChatData.fromJson(jsonDecode(data)["data"]);
      if(messageId!= chatData.id) {
        this.data!.data!.insert(0, chatData);
        messageId = chatData.id!;
        notifyListeners();
        if(userParameter!=null)
          readMessages(userParameter!);
      }
    }
  }

  Future<bool> checkConnectivity() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult != ConnectivityResult.mobile &&
        connectivityResult != ConnectivityResult.wifi) {
      Ui.showErrorToastMessage("No internet connection");
      return false;
    }
    else
      return true;
  }
}