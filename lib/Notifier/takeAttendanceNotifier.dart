import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Helper/utils.dart';
import 'package:skoolpod/Model/model_common_res.dart';
import 'package:skoolpod/Model/model_student_list.dart';
import 'package:skoolpod/Model/model_teacher_attendance.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class TakeAttendanceNotifier extends ChangeNotifier {
  ModelTeacherAttendance? modelTeacherAttendance;
  ModelStudentList? modelStudentList;
  bool isTeacherCheckedIn = false;
  int lateMinutes = 0;
  /*TakeAttendanceNotifier(this.context, this.userId) {
    webCallGetTeacherAttendance();
  }*/

  Future<void> webCallGetTeacherAttendance(
      BuildContext context, int userId) async {
    Map<String, String> param = new Map();
    param["date"] = Utils.getSimpleDateFormatOfToday();
    param["teacher_id"] = userId.toString();
    param["type"] = "Day";
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_TEACHER_SCHOOL_ATTENDANCE, body: param);
    if (response != null) {
      modelTeacherAttendance = ModelTeacherAttendance.fromJson(response);
      if (modelTeacherAttendance!.status! == 1) {
        String outTime = modelTeacherAttendance!.data!.out!.trim();
        String inTime = modelTeacherAttendance!.data!.ins!.trim();

        if (outTime == "" && inTime != "")
          isTeacherCheckedIn = true;
        else if (outTime != "" && inTime != "")
          isTeacherCheckedIn = false;
        else
          Ui.showToastMessage(Strings.you_can_submit_attendance_after_check_in);
      } else
        Ui.showToastMessage(Strings.you_can_submit_attendance_after_check_in);

      notifyListeners();
    }
  }

  Future<void> webCallGetStudentList(
      BuildContext context, int userId, int scheduleId) async {
    Map<String, String> param = new Map();
    param["schedule_id"] = scheduleId.toString();
    param["teacher_id"] = userId.toString();
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_STUDENT_LIST, body: param, isLoading: false);
    if (response != null && response["status"] == 1) {
      modelStudentList = ModelStudentList.fromJson(response);
      notifyListeners();
    }
  }

  Future<void> webCallGetStudentAttendance(
      BuildContext context, int userId, int scheduleId) async {
    Map<String, String> param = new Map();
    param["schedule_id"] = scheduleId.toString();
    param["teacher_id"] = userId.toString();
    param["from"] = Utils.getSimpleDateFormatOfToday();
    param["to"] = Utils.getSimpleDateFormatOfToday();
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_ALL_ATTENDANCE, body: param, isLoading: false);
    if (response != null && response["status"] == 1) {
      modelStudentList = ModelStudentList.fromJson(response);
      notifyListeners();
    }
  }

  Future<ModelCommonRes?> webCallSubmitAttendance(BuildContext context,
      int userId, int scheduleId, List<String> absentList) async {
    Map<String, String> param = new Map();
    String absentIdString = '';
    if (absentList.isNotEmpty)
      absentIdString =
          absentList.toString().substring(1, absentList.toString().length - 1);
    print("absentId : " + absentIdString);

    param["date"] = Utils.getSimpleDateFormatOfToday();
    param["absent"] = absentIdString;
    param["schedule_id"] = scheduleId.toString();
    param["teacher_id"] = userId.toString();
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.FILL_STUDENT_ATTENDANCE, body: param);
    if (response != null) {
      return ModelCommonRes.fromJson(response);
    }
    return null;
  }
}
