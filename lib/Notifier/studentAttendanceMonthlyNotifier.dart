import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:skoolpod/Model/model_student_attendance_monthly.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class StudentAttendanceMonthlyNotifier extends ChangeNotifier {
  StudentAttendanceMonthlyNotifier();
  ModelClassStudentAttendanceMonthly? modelClassStudentAttendanceMonthly;

  DateTime currentTime = DateTime.now();
  int monthCount = 1;
  //
  String startDate = '2021-06-01';
  String endDate = '2021-06-30';
  DateTime titleDate = DateTime.now();
  //
  final List<String> monthList = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  void getCurrentMonth() {
    DateTime _startDate = DateTime(currentTime.year, currentTime.month, 1);
    DateTime _endDate = DateTime(currentTime.year, currentTime.month + 1, 0);
    titleDate = _startDate;
    monthCount = currentTime.month;

    String formattedStartDate = DateFormat('yyyy-MM-dd').format(_startDate);
    String formattedEndDate = DateFormat('yyyy-MM-dd').format(_endDate);

    startDate = formattedStartDate;
    endDate = formattedEndDate;

    notifyListeners();
  }

  void gotoNextMonth({
    required int month,
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) {
    DateTime _startDate = DateTime(currentTime.year, month, 1);
    titleDate = _startDate;
    DateTime _endDate = DateTime(currentTime.year, month + 1, 0);

    String formattedStartDate = DateFormat('yyyy-MM-dd').format(_startDate);
    String formattedEndDate = DateFormat('yyyy-MM-dd').format(_endDate);
    startDate = formattedStartDate;
    endDate = formattedEndDate;
    webCallGetStudentAttendanceMonthly(
        context: context,
        userId: userId,
        standardId: standardId,
        divisionId: divisionId,
        lectureId: lectureId);
    notifyListeners();
  }

  void gotoPreviousMonth({
    required int month,
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) {
    DateTime _startDate = DateTime(currentTime.year, month, 1);
    titleDate = _startDate;
    DateTime _endDate = DateTime(currentTime.year, month + 1, 0);

    String formattedStartDate = DateFormat('yyyy-MM-dd').format(_startDate);
    String formattedEndDate = DateFormat('yyyy-MM-dd').format(_endDate);

    startDate = formattedStartDate;
    endDate = formattedEndDate;
    webCallGetStudentAttendanceMonthly(
        context: context,
        userId: userId,
        standardId: standardId,
        divisionId: divisionId,
        lectureId: lectureId);
    notifyListeners();
  }

  Future<void> webCallGetStudentAttendanceMonthly({
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) async {
    Map<String, String> param = new Map();
    param["principal_id"] = userId.toString();
    param["standard_id"] = standardId.toString();
    param["division_id"] = divisionId.toString();
    param["lecture_id"] = lectureId.toString();
    param["from"] = startDate;
    param["to"] = endDate;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_CLASS_ATTENDANCE, body: param);
    if (response != null && response["status"] == 1) {
      modelClassStudentAttendanceMonthly =
          ModelClassStudentAttendanceMonthly.fromJson(response);

      notifyListeners();
    }
  }
}
