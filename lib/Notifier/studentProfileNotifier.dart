import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:skoolpod/Helper/utils.dart';
import 'package:skoolpod/Model/model_common_res.dart';
import 'package:skoolpod/Model/model_student_profile.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:http_parser/http_parser.dart';

class StudentProfileNotifier extends ChangeNotifier {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final parentFirstNameController = TextEditingController();
  final parentLastNameController = TextEditingController();
  var standardDivisionController = TextEditingController();
  var birthdayController = TextEditingController();
  var genderController = TextEditingController();
  final phoneController = TextEditingController();
  final parentEmailController = TextEditingController();
  ModelStudentProfile? modelStudentProfile;

  StudentProfileNotifier(BuildContext context, int studentId) {
    getProfile(context, studentId);
  }

  Future<void> getProfile(BuildContext context, int studentId) async {
    Map<String, String> param = {"student_id": studentId.toString()};

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_STUDENT_PROFILE, body: param, isLoading: false);
    if (response != null && response["status"] == 1) {
      modelStudentProfile = ModelStudentProfile?.fromJson(response);
      firstNameController.text = modelStudentProfile?.data?.firstname ?? "";
      lastNameController.text = modelStudentProfile?.data?.lastname ?? "";
      standardDivisionController.text =
          "${modelStudentProfile?.data?.standard?.name ?? ""} ${modelStudentProfile?.data?.division?.name ?? ""} ";
      birthdayController.text = modelStudentProfile?.data!.dob != null
          ? Utils.formateDate(modelStudentProfile!.data!.dob!,
              Strings.FORMAT_SIMPLE, Strings.FORMAT_BIRTHDAY)
          : "";
      genderController.text = modelStudentProfile?.data?.gender ?? "";

      parentFirstNameController.text =
          modelStudentProfile?.data?.parent?.firstname ?? "";
      parentLastNameController.text =
          modelStudentProfile?.data?.parent?.lastname ?? "";
      parentEmailController.text =
          modelStudentProfile?.data?.parent?.email ?? "";
      phoneController.text = modelStudentProfile?.data?.parent?.phone ?? "";
      notifyListeners();
    }
  }

  Future<ModelCommonRes?> updateStudentProfile(BuildContext context,
      GlobalKey<FormState> formKey, int userId, int studentId) async {
    final isValid = formKey.currentState!.validate();
    if (isValid) {
      formKey.currentState!.save();
      Map<String, String> param = {
        "student_id": studentId.toString(),
        "parent_id": userId.toString(),
        "firstname": firstNameController.text.trim(),
        "lastname": lastNameController.text.trim(),
        "gender": genderController.text.trim(),
        "dob": Utils.formateDate(birthdayController.text.trim(),
            Strings.FORMAT_BIRTHDAY, Strings.FORMAT_SIMPLE),
      };
      var response = await WebCall.requestPostApiCall(context,
          url: ApiServices.UPDATE_STUDENT_PROFILE, body: param);
      return ModelCommonRes.fromJson(response);
    }
  }

  Future<ModelCommonRes>? uploadImageOfChild(
      int userId, File file, BuildContext context) async {
    Map<String, String> param = new Map();
    param['parent_id'] = userId.toString();
    param['student_id'] = modelStudentProfile!.data!.id.toString();
    String fileName = file.path.split('/').last;
    var image = MapEntry(
        "image",
        await MultipartFile.fromFile(
          file.path,
          filename: fileName,
          contentType: new MediaType('image', '*'),
        ));
    var response = await WebCall.uploadImage(
      context,
      param,
      file: image,
      url: ApiServices.UPLOAD_STUDENT_PROFILE_IMAGE,
    );

    var data = ModelCommonRes.fromJson(response);
    return data;
  }

  String? validateFirstName(value) {
    if (firstNameController.text.isEmpty)
      return "First Name is Required";
    else
      return null;
  }

  String? validateLastName(value) {
    if (lastNameController.text.isEmpty)
      return "Last Name is Required";
    else
      return null;
  }

  String? validateStandardDivision(String? value) {
    if (value != null && value.isEmpty)
      return "Standard is Required";
    else
      return null;
  }

  String? validateGender(String? value) {
    if (value != null && value.isEmpty)
      return "Gender Required";
    else
      return null;
  }
}
