import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Model/model_student_activity_list.dart';

class ActivityListNotifier extends ChangeNotifier {
  ModelStudentActivityList? modelStudentActivityList;
  Future<void> webCallGetActivityList(
      {required BuildContext context,
      required int studentId,
      required teacherId}) async {
    Map<String, dynamic> param = {};
    param["student_id"] = studentId;
    param["teacher_id"] = teacherId;
    param["type"] = "All";
    var res = await WebCall.requestPostApiCall(context,
        url: "get-student-activity", body: param, isLoading: false);
    if (res["status"] == 1) {
      modelStudentActivityList = ModelStudentActivityList.fromJson(res);
      notifyListeners();
    }
  }
}
//
// baseUrl+
// {
// "student_id": 55,   ===> from previous screen
// "teacher_id":427,  ==> userId
// "type": "All"   ==> static
// }
