import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:skoolpod/Model/model_teacher_leave_list.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class TeacherLeaveListNotifier extends ChangeNotifier {
  ModelTeacherLeaveList modelTeacherLeaveList = ModelTeacherLeaveList();
  Future<void> teacherLeaveList(
      {required BuildContext context, required int userId}) async {
    Map<String, String> param = Map();

    param["principal_id"] = userId.toString();
    param["type"] = "GetAll";
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.TEACHER_LEAVE_MANAGE, body: param);
    if (response != null && response["status"] == 1) {
      modelTeacherLeaveList = ModelTeacherLeaveList.fromJson(response);
      notifyListeners();
    }
  }

  Future<void> leaveStatus(
      {required BuildContext context,
      required int leaveId,
      required String statusOfLeave,
      required int principalId}) async {
    Map<String, dynamic> parameter = Map();
    parameter["leave_id"] = leaveId;
    parameter["type"] = "Update";
    parameter["status"] = statusOfLeave; //"Approved"/"Rejected"
    parameter["principal_id"] = principalId;
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.TEACHER_LEAVE_MANAGE, body: parameter);
    try {
      if (response != null && response["status"] == 1) {
        notifyListeners();
      } else {
        Ui.showSuccessToastMessage("something wrong");
      }
    } catch (e) {
      Ui.showErrorToastMessage(e.toString());
    }
  }
}
