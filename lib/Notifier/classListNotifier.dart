import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Helper/utils.dart';
import 'package:skoolpod/Model/model_classes.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class ClassesNotifier extends ChangeNotifier {
  ModelClasses? modelClasses;
  final BuildContext context;
  final int userId;

  ClassesNotifier(this.context, this.userId) {
    webCallGetTeacherClasses();
  }

  Future<void> webCallGetTeacherClasses() async {
    Map<String, String> param = new Map();
    param["date"] = Utils.getSimpleDateFormatOfToday();
    param["teacher_id"] = userId.toString();
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_TEACHER_CLASS, body: param, isLoading: false);
    if (response != null && response["status"] == 1) {
      modelClasses = ModelClasses.fromJson(response);
      notifyListeners();
    }
  }
}
