import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:skoolpod/Model/model_student_attendance_quarterly.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class StudentAttendanceQuarterlyNotifier extends ChangeNotifier {
  StudentAttendanceQuarterlyNotifier();
  ModelStudentAttendanceQuarterly? modelStudentAttendanceQuarterly;

  DateTime currentTime = DateTime.now();
  int monthCount = 6;
  int year = 1;
  int day = 1;
  String startDate = '2021-06-01';
  String endDate = '2021-08-31';
  DateTime titleStartDate = DateTime.now();

  void getCurrentQuarter() {
    year = currentTime.year;
    titleStartDate = DateTime(currentTime.year, 6, 1);
    notifyListeners();
  }

  void gotoNextQuarter({
    required int month,
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) {
    DateTime _startDate = DateTime(year, month, 1);
    DateTime _endDate = DateTime(year, month + 3, 0);
    titleStartDate = _startDate;
    if (_endDate.month == 5) {
      year = currentTime.year;
      monthCount = 3;
      titleStartDate = DateTime(year, monthCount, 1);
    }
    String formattedStartDate = DateFormat('yyyy-MM-dd').format(_startDate);
    String formattedEndDate = DateFormat('yyyy-MM-dd').format(_endDate);

    startDate = formattedStartDate;
    endDate = formattedEndDate;

    webCallGetStudentAttendanceQuarterly(
        context: context,
        userId: userId,
        standardId: standardId,
        divisionId: divisionId,
        lectureId: lectureId);

    notifyListeners();
  }

  void gotoPreviousQuarter({
    required int month,
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) {
    DateTime _startDate = DateTime(year, month, 1);
    DateTime _endDate = DateTime(year, month + 3, 0);
    titleStartDate = _startDate;
    if (_endDate.month == 8 && _endDate.year == currentTime.year) {
      year = currentTime.year + 1;
      monthCount = 6;
      titleStartDate = DateTime(year, month, 1);
    }
    String formattedStartDate = DateFormat('yyyy-MM-dd').format(_startDate);
    String formattedEndDate = DateFormat('yyyy-MM-dd').format(_endDate);

    startDate = formattedStartDate;
    endDate = formattedEndDate;

    webCallGetStudentAttendanceQuarterly(
        context: context,
        userId: userId,
        standardId: standardId,
        divisionId: divisionId,
        lectureId: lectureId);
    notifyListeners();
  }

  Future<void> webCallGetStudentAttendanceQuarterly({
    required BuildContext context,
    required int userId,
    required int standardId,
    required int divisionId,
    required int lectureId,
  }) async {
    Map<String, String> param = new Map();
    param["principal_id"] = userId.toString();
    param["standard_id"] = standardId.toString();
    param["division_id"] = divisionId.toString();
    param["lecture_id"] = lectureId.toString();
    param["from"] = startDate;
    param["to"] = endDate;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_CLASS_ATTENDANCE, body: param);
    if (response != null && response["status"] == 1) {
      modelStudentAttendanceQuarterly =
          ModelStudentAttendanceQuarterly.fromJson(response);
      notifyListeners();
    }
  }
}
