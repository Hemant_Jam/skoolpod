import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_child_leave_list.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class ChildLeaveListNotifier extends ChangeNotifier {
  ModelChildLeaveList modelChildLeaveList = ModelChildLeaveList();

  /// get list of child leave
  Future getChildLeaveList(
      {required BuildContext context,
      required int parentId,
      required int studentId}) async {
    Map<String, dynamic> parameter = {};
    parameter["parent_id"] = parentId;
    parameter["student_id"] = studentId;
    parameter["type"] = "Get";
    var response = await WebCall.requestPostApiCall(context,
        body: parameter, url: ApiServices.CHILDREN_LEAVE);
    if (response != null && response["status"] == 1) {
      modelChildLeaveList = ModelChildLeaveList.fromJson(response);
      notifyListeners();
    }
  }

  ///for child leave delete
  Future<bool> deleteChildLeave(
      {required int leaveId,
      required int studentId,
      required int parentId,
      required BuildContext context}) async {
    Map<String, dynamic> param = Map();

    param["parent_id"] = parentId;
    param["student_id"] = studentId;
    param["type"] = "Delete";
    param["leaveid"] = leaveId;
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.CHILDREN_LEAVE, body: param);
    if (response != null && response["status"] == 1) {
      Ui.showSuccessToastMessage("Leave removed successfully");
      notifyListeners();
      return true;
    } else
      return false;
  }
}
