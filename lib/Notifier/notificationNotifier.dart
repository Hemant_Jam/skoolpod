import 'package:flutter/material.dart';
import 'package:skoolpod/Model/model_common_res.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Resources/strings.dart';

class NotificationNotifier extends ChangeNotifier {
  Future<ModelCommonRes>? sendNotification(
      Map<String, String> param, BuildContext context) async {
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.SEND_PUSH_NOTIFICATION, body: param);
    return ModelCommonRes.fromJson(response);
  }

  String? validateNotificationTitle(String? value) {
    if (value == null || value.isEmpty) {
      return Strings.enter_notification_title;
    }
    return null;
  }

  String? validateNotificationMessage(String? value) {
    if (value == null || value.isEmpty) {
      return Strings.enter_notification_message;
    }
    return null;
  }
}
