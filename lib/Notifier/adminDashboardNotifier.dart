import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:skoolpod/Model/model_admin_dashboard.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class AdminDashBoardNotifier extends ChangeNotifier{
  ModelAdminDashboard? modelAdminDashboard;

  AdminDashBoardNotifier(BuildContext context, int userId) {
    webCallAdminDashboard(context, userId);
  }

  Future<void> webCallAdminDashboard(BuildContext context, int userId) async {
    final df = new DateFormat(Strings.FORMAT_SIMPLE);
    Map<String, String> param = new Map();
    param["date"] = df.format(DateTime.now());
    param["principal_id"] = userId.toString();
    var response = await WebCall.requestPostApiCall(context,url:ApiServices.GET_PRINCIPAL_DASHBOARD,body: param ,isLoading: false);
    if(response!=null && response["status"]==1){
       modelAdminDashboard = ModelAdminDashboard.fromJson(response);
       notifyListeners();
    }
  }
}