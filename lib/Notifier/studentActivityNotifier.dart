import 'package:flutter/material.dart';
import 'package:skoolpod/Model/model_student_activity.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class StudentActivityNotifier extends ChangeNotifier {
  ActivityModel activityModel = ActivityModel();

  Future<void> webCallGetTeacherLecture(
      {required BuildContext context, required int teacherId}) async {
    Map<String, dynamic> param = {};
    param["teacher_id"] = teacherId;
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_TEACHER_LECTURE, body: param);
    activityModel = ActivityModel.fromJson(response);
    notifyListeners();
  }
}
