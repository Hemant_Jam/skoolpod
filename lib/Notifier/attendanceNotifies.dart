import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_attendance_page.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class AttendanceNotifier extends ChangeNotifier {
  List<DivData> divList = [];
  ModelAttendance? modelAttendance;
  final BuildContext context;

  AttendanceNotifier(this.context, int userId, int schoolId) {
    webCallStandardDivision(userId, schoolId);
  }

  Future<void> webCallStandardDivision(int userId, int schoolId) async {
    Map<String, String> param = new Map();
    param["principal_id"] = userId.toString();
    param["school_id"] = schoolId.toString();
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_STANDARD_DIVISION, body: param, isLoading: false);
    if (response != null && response["status"] == 1) {
      modelAttendance = ModelAttendance.fromJson(response);
      for (var i = 0; i < modelAttendance!.data!.length; i++) {
        for (var j = 0; j < modelAttendance!.data![i].data!.length; j++) {
          modelAttendance!.data![i].data![j].name!.stdName =
              modelAttendance!.data![i].name?.name ?? "";
          modelAttendance!.data![i].data![j].name!.stdID =
              modelAttendance!.data![i].name?.id ?? 0;
          divList.add(modelAttendance!.data![i].data![j]);
        }
      }
      notifyListeners();
    }
  }
}
