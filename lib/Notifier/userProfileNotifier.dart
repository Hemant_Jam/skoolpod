import 'package:flutter/material.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:skoolpod/Screen/Parent/screen_child_list.dart';
import 'package:skoolpod/Screen/Teacher/screen_leave_manage.dart';

class UserProfileNotifier extends ChangeNotifier {
  final String userRole;
  Map<String, String> listMap = {};
  Map<String, String> routeMap = {};
  UserProfileNotifier(this.userRole) {
    setData(userRole);
  }

  setData(String role) {
    if (role == Strings.TEACHER)
      listMap = {
        Strings.leaves: "assets/images/ic_dash_leave_small.png",
        Strings.query_board: "assets/images/ic_dash_query.png",
        Strings.video_library: "assets/images/ic_video_library.png",
        Strings.staff_directory: "assets/images/ic_dash_staff.png",
        Strings.add_care_activity: "assets/images/ic_dash_staff.png",
      };
    else
      listMap = {
        Strings.child_profile: "assets/images/ic_child_avatar.png",
        Strings.leaves: "assets/images/ic_dash_leave_small.png",
        Strings.query_board: "assets/images/ic_dash_query.png",
      };
    routeMap = {
      Strings.child_profile: ScreenChildList.routeName,
      Strings.leaves: role == Strings.TEACHER
          ? ScreenLeaveManage.routeName
          : ScreenChildList.routeName,
      Strings.query_board: "",
      Strings.video_library: "",
      Strings.staff_directory: "",
      Strings.add_care_activity: "",
    };
  }
}
