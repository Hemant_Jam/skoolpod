import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_child_list.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';

class ChildListNotifier extends ChangeNotifier {
  List<Data>? data;

  Future<void> webCallGetChildList(BuildContext context, int userId) async {
    Map<String, String> param = new Map();
    param["parent_id"] = userId.toString();

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_CHILD_LIST, body: param, isLoading: false);
    if (response != null && response["status"] == 1) {
      ModelChildList modelChildList = ModelChildList.fromJson(response);
      data = modelChildList.data;
      notifyListeners();
    }
  }
}
