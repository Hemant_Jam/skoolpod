import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_add_teacher_leave.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class AddTeacherLeaveNotifier extends ChangeNotifier {
  ModelAddTeacherLeave modelAddTeacherLeave = ModelAddTeacherLeave();
  String principalName = "";
  int? principalId;
  TextEditingController selectPrincipalController = TextEditingController();
  List pList = [];
  void changePrincipalName({required String pName, required int pId}) {
    principalName = pName;
    principalId = pId;
    selectPrincipalController.value = TextEditingValue(text: principalName);
    notifyListeners();
  }

  Future getPrincipalList(
      {required BuildContext context, required int schoolId}) async {
    Map<String, String> param = Map();
    param["school_id"] = schoolId.toString();
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_PRINCIPAL, body: param);
    if (response != null && response["status"] == 1) {
      pList.add(response);
      modelAddTeacherLeave = ModelAddTeacherLeave.fromJson(response);
      notifyListeners();
    }
    return response;
  }

  Future<bool> addTeacherLeave(
      {required BuildContext context,
      required int days,
      required String startDate,
      required String endDate,
      required int teacherId,
      required String reason}) async {
    Map<String, dynamic> param = Map();

    param["days"] = days;
    param["start"] = startDate;
    param["end"] = endDate;
    param["principal_id"] = principalId;
    param["teacher_id"] = teacherId;
    param["reason"] = reason;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.ADD_TEACHER_LEAVE, body: param);

    if (response != null && response["status"] == 1) {
      Ui.showSuccessToastMessage("Leave added successfully");
      return true;
    } else
      Ui.showErrorToastMessage(response['msg']);
    return false;
  }

  Future<bool> updateTeacherLeave(
      {required BuildContext context,
      required int leaveId,
      required int days,
      required String startDate,
      required String endDate,
      required int teacherId,
      required String reason,
      required int principalID}) async {
    Map<String, dynamic> param = Map();
    param["leaveid"] = leaveId;
    param["days"] = days;
    param["start"] = startDate;
    param["end"] = endDate;
    param["principal_id"] = principalId;
    param["teacher_id"] = teacherId;
    param["reason"] = reason;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.ADD_TEACHER_LEAVE, body: param);

    if (response != null && response["status"] == 1) {
      Ui.showSuccessToastMessage("Leave updated successfully");
      return true;
    } else
      Ui.showErrorToastMessage(response['msg']);
    return false;
  }
}
