import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:skoolpod/Model/model_common_res.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Resources/strings.dart';
import 'package:http_parser/http_parser.dart';

class AssignmentNotifier extends ChangeNotifier{

  Future<ModelCommonRes>? sendAssignment(Map<String,String> param,File file,BuildContext context) async {

    String fileName = file.path.split('/').last;
    var image = MapEntry("body", await MultipartFile.fromFile(file.path,filename: fileName,contentType: new MediaType('image', '*'),));
    var response = await WebCall.uploadImage(context,param,file :image,url:ApiServices.SEND_MESSAGE,);

    var data = ModelCommonRes.fromJson(response);
      return data;

  }
  String? validateClass(String? value) {
    if (value==null || value.isEmpty) {
      return Strings.select_class;
    }
    return null;
  }

  String? validateAssignmentTitle(String? value) {
    if (value==null || value.isEmpty) {
      return Strings.enter_assignment_title;
    }
    return null;
  }
  String? validateAssignmentDescription(String? value) {
    if (value==null || value.isEmpty) {
      return Strings.enter_assignment_description;
    }
    return null;
  }
}