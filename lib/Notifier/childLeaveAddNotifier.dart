import 'package:flutter/cupertino.dart';
import 'package:skoolpod/Model/model_add_child_leave.dart';
import 'package:skoolpod/Service/apiServices.dart';
import 'package:skoolpod/Service/webCall.dart';
import 'package:skoolpod/Widgets/ui_component.dart';

class ChildLeaveAddNotifier extends ChangeNotifier {
  ModelAddChildLeave modelAddChildLeave = ModelAddChildLeave();
  int? teacherId;
  String leaveType = "";
  TextEditingController selectTeacherController = TextEditingController();
  TextEditingController selectLeaveTypeController = TextEditingController();
  void changeLeaveType({required String leaveType}) {
    this.leaveType = leaveType;
    selectLeaveTypeController.value = TextEditingValue(text: leaveType);
    notifyListeners();
  }

  void changeTeacherName(
      {required String teacherName, required int teacherID}) {
    teacherId = teacherID;
    selectTeacherController.value = TextEditingValue(text: teacherName);
    notifyListeners();
  }

  Future getTeacherList(
      {required BuildContext context, required int studentId}) async {
    Map<String, dynamic> param = Map();
    param["student_id"] = studentId;
    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.GET_STUDENT_LEAVE_TEACHER, body: param);
    if (response != null && response["status"] == 1) {
      modelAddChildLeave = ModelAddChildLeave.fromJson(response);
      notifyListeners();
    }
    return response;
  }

  /// apply for new leave of child
  Future<bool> addChildLeave(
      {required BuildContext context,
      required int days,
      required String startDate,
      required String endDate,
      required String reason,
      required int parentId,
      required int studentId}) async {
    Map<String, dynamic> param = Map();
    param["days"] = days;
    param["start"] = startDate;
    param["end"] = endDate;
    param["leave_type"] = leaveType;
    param["parent_id"] = parentId;
    param["student_id"] = studentId;
    param["reason"] = reason;
    param["type"] = "Add";
    param["teacher_id"] = teacherId;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.CHILDREN_LEAVE, body: param);

    if (response != null && response["status"] == 1) {
      Ui.showSuccessToastMessage("Leave added successfully");
      return true;
    } else
      Ui.showErrorToastMessage(response['msg']);
    return false;
  }

  /// change/edit current leave
  Future<bool> updateChildLeave({
    required BuildContext context,
    required int days,
    required String startDate,
    required String endDate,
    required int parentId,
    required int studentId,
    required String reason,
    required int leaveId,
  }) async {
    Map<String, dynamic> param = Map();
    param["days"] = days;
    param["start"] = startDate;
    param["end"] = endDate;
    param["leave_type"] = leaveType;
    param["parent_id"] = parentId;
    param["student_id"] = studentId;
    param["reason"] = reason;
    param["type"] = "Update";
    param["teacher_id"] = teacherId;
    param["leaveid"] = leaveId;

    var response = await WebCall.requestPostApiCall(context,
        url: ApiServices.CHILDREN_LEAVE, body: param);

    if (response != null && response["status"] == 1) {
      Ui.showSuccessToastMessage("Leave updated successfully");
      return true;
    } else
      Ui.showErrorToastMessage(response['msg']);
    return false;
  }
}
