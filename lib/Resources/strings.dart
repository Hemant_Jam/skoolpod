class Strings {
  static const String you = "YOU";
  static const String student = "STUDENT";
  static const String are_you_sure = "Are you sure you want to remove leave?";
  static const String confirmation = "Confirmation";
  static const String select_date = "Select Date";
  static const String from_date = 'From date';
  static const String to_date = 'To date';
  static const String enter_reason_for_leave = "Enter Reason For Leave";

  static const String select_your_principal = 'Select Your Principal';
  static const String type_your_reason_here = 'Type your reason here';
  static const String add_leaves = "Add Leave";
  static const String dashboard = "Dashboard";
  static const String select_principal = "Select Principal";
  static const String welcome_back = "Welcome Back";
  static const String enter_email = "Please enter your Email Id to log in";
  static const String email_address = "Email Address";
  static const String email_required = "Email is required";
  static const String email_not_valid =
      "Email is required Email address is not valid!";
  static const String password = "Password";
  static const String password_required = "Password is required!";
  static const String password_not_valid =
      "Password must be between 6 to 25 characters";
  static const String login = "LOGIN";
  static const String logout = "Logout";
  static const String sure_to_logout = "Are you sure you want to logout?";
  static const String check_in = "Check in";
  static const String check_out = "Check out";
  static const String message = "Message";
  static const String calender = "Calender";
  static const String student_attendance = "Student Attendance";
  static const String attendance = "Attendance";
  static const String child_attendance = "Child Attendance";
  static const String classes = "Classes";
  static const String leaves = "Leave";
  static const String student_activity = "Student Activity";
  static const String staff_directory = "Staff Directory";
  static const String query = "Query";
  static const String more = "More..";
  static const String more_ = "More";
  static const String child_diaries = "Child Diaries";
  static const String video_library = "Video Library";
  static const String add_assignment = "Add Assignment";
  static const String push_message = "Push Message";
  static const String events = "Events";
  static const String add = "ADD";
  static const String send = "SEND";
  static const String select_class = "Select your class";
  static const String class_list = "Class List";
  static const String take_attendance = "TAKE\nATTENDANCE";
  static const String edit_attendance = "EDIT\nATTENDANCE";
  static const String enter_assignment_title = "Enter Assignment Title";
  static const String enter_assignment_description =
      "Enter Assignment description";
  static const String enter_notification_title = "Enter Notification Title";
  static const String enter_notification_message = "Enter notification message";
  static const String txt_upload_assignment =
      "Upload assignment file\n(PDF, Doc or any audio file)";
  static const String present = "PRESENT";
  static const String absent = "ABSENT";
  static const String you_can_submit_attendance_after_check_in =
      "You can submit attendance after Check in";
  static const String submit_attendance = "SUBMIT ATTENDANCE";
  static const String attendance_submit_msg =
      "Attendance submitted successfully.";
  static const String child_profile = "Child Profile";
  static const String profile_details = "Profile Details";
  static const String parent_profile_details = "Parent Profile Details";
  static const String student_profile_update_msg =
      "Student profile updated successfully";
  static const String first_name = "First Name";
  static const String last_name = "Last Name";
  static const String standard_division = "Standard-Division";
  static const String birthday = "Birthday";
  static const String gender = "Gender";
  static const String email = "Email";
  static const String phone_number = "Phone Number";
  static const String query_board = "Query Board";
  static const String add_care_activity = "Add Care Activity";
  static const String select_child = "Select child";

  static const urlPattern =
      r"(https?|http)://([-A-Z0-9.]+)(/[-A-Z0-9+&@#/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#/%=~_|!:‌​,.;]*)?";
  //Roles of user
  static const DEVICE_TYPE = "Android";
  static const TEACHER = "Teacher";
  static const PRINCIPAL = "Principal";
  static const PARENT = "Parent";

  // SharedPreference
  static const String IS_LOGGED_IN = "isLoggedIn";
  static const String APP_TOKEN = "appToken";
  static const String USER_DATA = "userData";

  //Message type
  static const MESSAGE_TEXT = "Text";
  static const MESSAGE_VIDEO = "Video";
  static const MESSAGE_IMAGE = "Image";
  static const MESSAGE_DOCUMENT = "Document";
  static const MESSAGE_AUDIO = "Audio";

  //Fonts name
  static const String VARELA = "Varela";
  static const String BALOO = "Baloo";

  //datetime format
  static const String FORMAT_SERVER = "yyyy-MM-dd HH:mm:ss";
  static const String FORMAT_TODAY_TIME = "hh:mm a";
  static const String FORMAT_SIMPLE = "yyyy-MM-dd";
  static const String FORMAT_DATE_TIME = "dd-MMM hh:mm a";
  static const String FORMAT_BIRTHDAY = "MMM dd, yyyy";
}
