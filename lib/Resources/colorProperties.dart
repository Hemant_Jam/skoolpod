import 'dart:ui';

const PrimaryColor = const Color(0xff00baaf);
const PrimaryDark = const Color(0xff0184a2);
const Green = const Color(0xff43A047);
const Red = const Color(0xffE53935);
const White = const Color(0xffFFFFFF);
const ColorUsername = const Color(0xff4E4E4E);
const Dash6 = const Color(0xff666666);
const Dash1 = const Color(0xff333333);
const ColorYellow = const Color(0xffF5A623);
const YellowDash = const Color(0xffFFCA4A);
const GreyLight = const Color(0xff999999);
const PrimaryLight = const Color(0xff00E0D3);
const CardTitleBg = const Color(0xffD6D5D5);

const Map<int, Color> colorCodes = {
  50: Color.fromRGBO(33, 46, 99, .1),
  100: Color.fromRGBO(33, 46, 99, .2),
  200: Color.fromRGBO(33, 46, 99, .3),
  300: Color.fromRGBO(33, 46, 99, .4),
  400: Color.fromRGBO(33, 46, 99, .5),
  500: Color.fromRGBO(33, 46, 99, .6),
  600: Color.fromRGBO(33, 46, 99, .7),
  700: Color.fromRGBO(33, 46, 99, .8),
  800: Color.fromRGBO(33, 46, 99, .9),
  900: Color.fromRGBO(33, 46, 99, 1),
};
